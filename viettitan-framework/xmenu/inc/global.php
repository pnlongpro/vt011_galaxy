<?php
function xmenu_get_transition() {
	return array(
		'none' => esc_html__('None','viettitan'),
		'x-animate-slide-up' => esc_html__('Slide Up','viettitan'),
		'x-animate-slide-down' => esc_html__('Slide Down','viettitan'),
		'x-animate-slide-left' => esc_html__('Slide Left','viettitan'),
		'x-animate-slide-right' => esc_html__('Slide Right','viettitan'),
		'x-animate-sign-flip' => esc_html__('Sign Flip','viettitan'),
	);
}

function xmenu_get_grid () {
	return array(
		'basic' => array(
			'text' => esc_html__('Basic','viettitan'),
			'options' => array(
				'auto' => esc_html__('Automatic','viettitan'),
				'x-col x-col-12-12' => esc_html__('Full Width','viettitan'),
			)
		),
		'halves' => array(
			'text' => esc_html__('Halves','viettitan'),
			'options' => array(
				'x-col x-col-6-12' => esc_html__('1/2','viettitan'),
			)
		),
		'thirds' => array(
			'text' => esc_html__('Thirds','viettitan'),
			'options' => array(
				'x-col x-col-4-12' => esc_html__('1/3','viettitan'),
				'x-col x-col-8-12' => esc_html__('2/3','viettitan'),
			)
		),
		'quarters' => array(
			'text' => esc_html__('Quarters','viettitan'),
			'options' => array(
				'x-col x-col-3-12' => esc_html__('1/4','viettitan'),
				'x-col x-col-9-12' => esc_html__('3/4','viettitan'),
			)
		),
		'fifths' => array(
			'text' => esc_html__('Fifths','viettitan'),
			'options' => array(
				'x-col x-col-2-10' => esc_html__('1/5','viettitan'),
				'x-col x-col-4-10' => esc_html__('2/5','viettitan'),
				'x-col x-col-6-10' => esc_html__('3/5','viettitan'),
				'x-col x-col-8-10' => esc_html__('4/5','viettitan'),
			)
		),
		'sixths' => array(
			'text' => esc_html__('Sixths','viettitan'),
			'options' => array(
				'x-col x-col-2-12' => esc_html__('1/6','viettitan'),
				'x-col x-col-10-12' => esc_html__('5/6','viettitan'),
			)
		),
		'sevenths' => array(
			'text' => esc_html__('Sevenths','viettitan'),
			'options' => array(
				'x-col x-col-1-7' => esc_html__('1/7','viettitan'),
				'x-col x-col-2-7' => esc_html__('2/7','viettitan'),
				'x-col x-col-3-7' => esc_html__('3/7','viettitan'),
				'x-col x-col-4-7' => esc_html__('4/7','viettitan'),
				'x-col x-col-5-7' => esc_html__('5/7','viettitan'),
				'x-col x-col-6-7' => esc_html__('6/7','viettitan'),
			)
		),
		'eighths' => array(
			'text' => esc_html__('Eighths','viettitan'),
			'options' => array(
				'x-col x-col-1-8' => esc_html__('1/8','viettitan'),
				'x-col x-col-3-8' => esc_html__('3/8','viettitan'),
				'x-col x-col-5-8' => esc_html__('5/8','viettitan'),
				'x-col x-col-7-8' => esc_html__('7/8','viettitan'),
			)
		),
		'ninths' => array(
			'text' => esc_html__('Ninths','viettitan'),
			'options' => array(
				'x-col x-col-1-9' => esc_html__('1/9','viettitan'),
				'x-col x-col-2-9' => esc_html__('2/9','viettitan'),
				'x-col x-col-4-9' => esc_html__('4/9','viettitan'),
				'x-col x-col-5-9' => esc_html__('5/9','viettitan'),
				'x-col x-col-7-9' => esc_html__('7/9','viettitan'),
				'x-col x-col-8-9' => esc_html__('8/9','viettitan'),
			)
		),
		'tenths' => array(
			'text' => esc_html__('Tenths','viettitan'),
			'options' => array(
				'x-col x-col-1-10' => esc_html__('1/10','viettitan'),
				'x-col x-col-3-10' => esc_html__('3/10','viettitan'),
				'x-col x-col-7-10' => esc_html__('7/10','viettitan'),
				'x-col x-col-9-10' => esc_html__('9/10','viettitan'),
			)
		),
		'elevenths' => array(
			'text' => esc_html__('Elevenths','viettitan'),
			'options' => array(
				'x-col x-col-1-11' => esc_html__('1/11','viettitan'),
				'x-col x-col-2-11' => esc_html__('2/11','viettitan'),
				'x-col x-col-3-11' => esc_html__('3/11','viettitan'),
				'x-col x-col-4-11' => esc_html__('4/11','viettitan'),
				'x-col x-col-5-11' => esc_html__('5/11','viettitan'),
				'x-col x-col-6-11' => esc_html__('6/11','viettitan'),
				'x-col x-col-7-11' => esc_html__('7/11','viettitan'),
				'x-col x-col-8-11' => esc_html__('8/11','viettitan'),
				'x-col x-col-9-11' => esc_html__('9/11','viettitan'),
				'x-col x-col-10-11' => esc_html__('10/11','viettitan'),
			)
		),
		'twelfths' => array(
			'text' => esc_html__('Twelfths','viettitan'),
			'options' => array(
				'x-col x-col-1-12' => esc_html__('1/12','viettitan'),
				'x-col x-col-5-12' => esc_html__('5/12','viettitan'),
				'x-col x-col-7-12' => esc_html__('7/12','viettitan'),
				'x-col x-col-11-12' => esc_html__('11/12','viettitan'),
			)
		),
	);
}

function xmenu_get_item_settings() {
	if (isset($GLOBALS['xmenu_item_settings'])) {
		return $GLOBALS['xmenu_item_settings'];
	}
	$GLOBALS['xmenu_item_settings'] = array(
		'general' => array(
			'text' => esc_html__('General','viettitan'),
			'icon' => 'fa fa-cogs',
			'config' => array(
				'general-heading' => array(
					'text' => esc_html__('General','viettitan'),
					'type' => 'heading'
				),
				'general-url' => array(
					'text' => esc_html__('URL','viettitan'),
					'type' => 'text',
					'std'  => '',
				),
				'general-title' => array(
					'text' => esc_html__('Navigation Label','viettitan'),
					'type' => 'text',
					'std'  => '',
				),
				'general-attr-title' => array(
					'text' => esc_html__('Title Attribute','viettitan'),
					'type' => 'text',
					'std'  => '',
				),
				'general-target' => array(
					'text' => esc_html__('Open link in a new window/tab','viettitan'),
					'type' => 'checkbox',
					'std'  => '',
					'value' => '_blank',
				),
				'general-classes' => array(
					'text' => esc_html__('CSS Classes (optional)','viettitan'),
					'type' => 'array',
					'std'  => '',
				),
				'general-xfn' => array(
					'text' => esc_html__('Link Relationship (XFN)','viettitan'),
					'type' => 'text',
					'std'  => '',
				),
				'general-description' => array(
					'text' => esc_html__('Description','viettitan'),
					'type' => 'textarea',
					'std'  => '',
				),
				'general-other-heading' => array(
					'text' => esc_html__('Other','viettitan'),
					'type' => 'heading'
				),
				'other-disable-text' => array(
					'text' => esc_html__('Disable Text','viettitan'),
					'type' => 'checkbox',
					'std' => ''
				),
				'other-disable-menu-item' => array(
					'text' => esc_html__('Disable Menu Item','viettitan'),
					'type' => 'checkbox',
					'std' => ''
				),
				'other-disable-link' => array(
					'text' => esc_html__('Disable Link','viettitan'),
					'type' => 'checkbox',
					'std' => ''
				),
				'other-display-header-column' => array(
					'text' => esc_html__('Display as a Sub Menu column header','viettitan'),
					'type' => 'checkbox',
					'std' => ''
				),
				'other-feature-text' => array(
					'text' => esc_html__('Menu Feature Text','viettitan'),
					'type' => 'text',
					'std' => ''
				),
			)
		),
		'icon' => array(
			'text' => esc_html__('Icon','viettitan'),
			'icon' => 'fa fa-qrcode',
			'config' => array(
				'icon-heading' => array(
					'text' => esc_html__('Icon','viettitan'),
					'type' => 'heading'
				),
				'icon-value' => array(
					'text' => esc_html__('Set Icon','viettitan'),
					'type' => 'icon',
					'std'  => '',
				),
				'icon-position' => array(
					'text' => esc_html__('Icon Position','viettitan'),
					'type' => 'select',
					'std'  => 'left',
					'options' => array(
						'left' => esc_html__('Left of Menu Text','viettitan'),
						'right' => esc_html__('Right of Menu Text','viettitan'),
					)
				),
				'icon-padding' => array(
					'text' => esc_html__('Padding Icon and Text Menu','viettitan'),
					'type' => 'text',
					'std'  => '',
					'des' => esc_html__('Padding between Icon and Text Menu (px). Do not include units','viettitan')
				)
			)
		),
		'image' => array(
			'text' => esc_html__('Image','viettitan'),
			'icon' => 'fa fa-picture-o',
			'config' => array(
				'image-heading' => array(
					'text' => esc_html__('Image','viettitan'),
					'type' => 'heading'
				),
				'image-url' => array(
					'text' => esc_html__('Image Url','viettitan'),
					'type' => 'image',
					'std'  => '',
				),
				'image-size' => array(
					'text' => esc_html__('Image Size','viettitan'),
					'type' => 'select',
					'std'  => 'full',
					'options' => xmenu_get_image_size()
				),
				'image-dimensions' => array(
					'text' => esc_html__('Image Dimensions','viettitan'),
					'type' => 'select',
					'std'  => 'inherit',
					'options' => array(
						'inherit' => 'Inherit from Menu Settings',
						'custom' => 'Custom',
					)
				),
				'image-width' => array(
					'text' => esc_html__('Image Width','viettitan'),
					'type' => 'text',
					'std'  => '',
					'des' => esc_html__('Image width attribute (px). Do not include units. Only valid if "Image Dimension" is set to "Custom" above','viettitan')
				),
				'image-height' => array(
					'text' => esc_html__('Image Height','viettitan'),
					'type' => 'text',
					'std'  => '',
					'des' => esc_html__('Image width attribute (px). Do not include units. Only valid if "Image Dimension" is set to "Custom" above','viettitan')
				),
				'image-layout' => array(
					'text' => esc_html__('Image Layout','viettitan'),
					'type' => 'select',
					'std'  => 'image-only',
					'options' => array(
						'image-only' => esc_html__('Image Only','viettitan'),
						'left' => esc_html__('Image Left','viettitan'),
						'right' => esc_html__('Image Right','viettitan'),
						'above' => esc_html__('Image Above','viettitan'),
						'below' => esc_html__('Image Below','viettitan'),
					)
				),
				'image-feature' => array(
					'text' => esc_html__('Use Feature Image','viettitan'),
					'type' => 'checkbox',
					'std'  => '',
					'des' => 'Use Feature Image from Post/Page Menu Item',
				),
			)
		),

		'layout' => array(
			'text' => esc_html__('Layout','viettitan'),
			'icon' => 'fa fa-columns',
			'config' => array(
				'layout-heading' => array(
					'text' => esc_html__('Layout','viettitan'),
					'type' => 'heading'
				),
				'layout-width' => array(
					'text' => esc_html__('Menu Item Width','viettitan'),
					'type' => 'select-group',
					'std'  => 'auto',
					'options' => xmenu_get_grid()
				),
				'layout-text-align' => array(
					'text' => esc_html__('Item Content Alignment','viettitan'),
					'type' => 'select',
					'std'  => 'none',
					'options' => array(
						'none' => esc_html__('Default','viettitan'),
						'left' => esc_html__('Text Left','viettitan'),
						'center' => esc_html__('Text Center','viettitan'),
						'right' => esc_html__('Text Right','viettitan'),
					)
				),
				'layout-padding' => array(
					'text' => esc_html__('Padding','viettitan'),
					'type' => 'text',
					'std'  => '',
					'des' => esc_html__('Set padding for menu item. Include the units.','viettitan'),
				),
				'layout-margin' => array(
					'text' => esc_html__('Margin','viettitan'),
					'type' => 'text',
					'std'  => '',
					'des' => esc_html__('Set margin for menu item. Include the units.','viettitan'),
				),
				'layout-new-row' => array(
					'text' => esc_html__('New Row','viettitan'),
					'type' => 'checkbox',
					'std'  => ''
				),
			)
		),
		'submenu' => array(
			'text' => esc_html__('Sub Menu','viettitan'),
			'icon' => 'fa fa-list-alt',
			'config' => array(
				'submenu-heading' => array(
					'text' => esc_html__('Sub Menu','viettitan'),
					'type' => 'heading'
				),
				'submenu-type' => array(
					'text' => esc_html__('Sub Menu Type','viettitan'),
					'type' => 'select',
					'std'  => 'standard',
					'options' => array(
						'standard' => esc_html__('Standard','viettitan'),
						'multi-column' => esc_html__('Multi Column','viettitan'),
						/*'stack' => esc_html__('Stack','viettitan'),*/
						'tab' => esc_html__('Tab','viettitan'),
					)
				),
				'submenu-position' => array(
					'text' => esc_html__('Sub Menu Position','viettitan'),
					'type' => 'select',
					'std'  => '',
					'options' => array(
						'' => esc_html__('Automatic','viettitan'),
						'pos-left-menu-parent' => esc_html__('Left of Menu Parent','viettitan'),
						'pos-right-menu-parent' => esc_html__('Right of Menu Parent','viettitan'),
						'pos-center-menu-parent' => esc_html__('Center of Menu Parent','viettitan'),
						'pos-left-menu-bar' => esc_html__('Left of Menu Bar','viettitan'),
						'pos-right-menu-bar' => esc_html__('Right of Menu Bar','viettitan'),
						'pos-full' => esc_html__('Full Size','viettitan'),
					)
				),
				'submenu-width-custom' => array(
					'text' => esc_html__('Sub Menu Width Custom','viettitan'),
					'type' => 'text',
					'std'  => '',
					'des' => esc_html__('Set custom Sub Menu Width. Include the units (px/em/%).','viettitan'),
				),
				'submenu-col-width-default' => array(
					'text' => esc_html__('Sub Menu Column Width Default','viettitan'),
					'type' => 'select-group',
					'std'  => 'auto',
					'options' => xmenu_get_grid()
				),
				'submenu-col-spacing-default' => array(
					'text' => esc_html__('Sub Menu Column Spacing Default','viettitan'),
					'type' => 'text',
					'std'  => '',
					'des' => esc_html__('Set sub menu column spacing default. Do not include unit.','viettitan'),
				),
				'submenu-list-style' => array(
					'text' => esc_html__('Sub Menu List Style','viettitan'),
					'type' => 'select',
					'std'  => 'none',
					'options' => array(
						'none' => esc_html__('None','viettitan'),
						'disc' => esc_html__('Disc','viettitan'),
						'square' => esc_html__('Square','viettitan'),
						'circle' => esc_html__('Circle','viettitan'),
					)
				),
				'submenu-tab-position' => array(
					'text' => esc_html__('Tab Position','viettitan'),
					'type' => 'select',
					'std'  => 'left',
					'des' => esc_html__('Tab Position set to "Sub Menu Type" is "TAB".','viettitan'),
					'options' => array(
						'left' => esc_html__('Left','viettitan'),
						'right' => esc_html__('Right','viettitan'),
					)
				),
				'submenu-animation' => array(
					'text' => esc_html__('Sub Menu Animation','viettitan'),
					'type' => 'select',
					'std'  => 'none',
					'options' => xmenu_get_transition()
				),
			)
		),
		'custom-content' => array(
			'text' => esc_html__('Custom Content','viettitan'),
			'icon' => 'fa fa-code',
			'config' => array(
				'custom-content-heading' => array(
					'text' => esc_html__('Custom Content','viettitan'),
					'type' => 'heading'
				),
				'custom-content-value' => array(
					'text' => esc_html__('Custom Content','viettitan'),
					'type' => 'textarea',
					'std'  => '',
					'des' => esc_html__('Can contain HTML and shortcodes','viettitan'),
					'height' => '300px'
				),
			)
		),
		'widget' => array(
			'text' => esc_html__('Widget Area','viettitan'),
			'icon' => 'fa-puzzle-piece',
			'config' => array(
				'widget-heading' => array(
					'text' => esc_html__('Select Widget Area','viettitan'),
					'type' => 'heading'
				),
				'widget-area' => array(
					'text' => esc_html__('Widget Area','viettitan'),
					'type' => 'sidebar',
					'std' => '-1',
					'hide-label' => 'true'
				),
			)
		),
		'customize-style' => array(
			'text' => esc_html__('Customize Style','viettitan'),
			'icon' => 'fa-paint-brush',
			'config' => array(
				'custom-style-menu-heading' => array(
					'text' => esc_html__('Menu Item','viettitan'),
					'type' => 'heading'
				),
				'custom-style-menu-bg-color' => array(
					'text' => esc_html__('Background Color','viettitan'),
					'type' => 'color',
					'std'  => '',
				),
				'custom-style-menu-text-color' => array(
					'text' => esc_html__('Text Color','viettitan'),
					'type' => 'color',
					'std'  => '',
				),
				'custom-style-menu-bg-color-active' => array(
					'text' => esc_html__('Background Color [Active]','viettitan'),
					'type' => 'color',
					'std'  => '',
				),
				'custom-style-menu-text-color-active' => array(
					'text' => esc_html__('Text Color [Active]','viettitan'),
					'type' => 'color',
					'std'  => '',
				),
				'custom-style-menu-bg-image' => array(
					'text' => esc_html__('Background Image','viettitan'),
					'type' => 'image',
					'std'  => '',
				),
				'custom-style-menu-bg-image-repeat' => array(
					'text' => esc_html__('Background Image Repeat','viettitan'),
					'type' => 'select',
					'std' => 'no-repeat',
					'hide-label' => 'true',
					'options' => array(
						'no-repeat' => 'no-repeat',
						'repeat' => 'repeat',
						'repeat-x' => 'repeat-x',
						'repeat-y' => 'repeat-y'
					)
				),
				'custom-style-menu-bg-image-attachment' => array(
					'text' => esc_html__('Background Image Attachment','viettitan'),
					'type' => 'select',
					'std' => 'scroll',
					'hide-label' => 'true',
					'options' => array(
						'scroll' => 'scroll',
						'fixed' => 'fixed'
					)
				),
				'custom-style-menu-bg-image-position' => array(
					'text' => esc_html__('Background Image Position','viettitan'),
					'type' => 'select',
					'std' => 'center',
					'hide-label' => 'true',
					'options' => array(
						'center' => 'center',
						'center left' => 'center left',
						'center right' => 'center right',
						'top left' => 'top left',
						'top center' => 'top center',
						'top right' => 'top right',
						'bottom left' => 'bottom left',
						'bottom center' => 'bottom center',
						'bottom right' => 'bottom right'
					)
				),
				'custom-style-menu-bg-image-size' => array(
					'text' => esc_html__('Background Image Size','viettitan'),
					'type' => 'select',
					'std' => 'auto',
					'hide-label' => 'true',
					'options' => array(
						'auto' => 'Keep original',
						'100% auto' => 'Stretch to width',
						'auto 100%' => 'Stretch to height',
						'cover' => 'Cover',
						'contain' => 'Contain'
					)
				),
				'custom-style-sub-menu-heading' => array(
					'text' => esc_html__('Sub Menu','viettitan'),
					'type' => 'heading'
				),
				'custom-style-sub-menu-bg-color' => array(
					'text' => esc_html__('Background Color','viettitan'),
					'type' => 'color',
					'std'  => '',
				),
				'custom-style-sub-menu-text-color' => array(
					'text' => esc_html__('Text Color','viettitan'),
					'type' => 'color',
					'std'  => '',
				),
				'custom-style-sub-menu-bg-image' => array(
					'text' => esc_html__('Background Image','viettitan'),
					'type' => 'image',
					'std'  => '',
				),
				'custom-style-sub-menu-bg-image-repeat' => array(
					'text' => esc_html__('Background Image Repeat','viettitan'),
					'type' => 'select',
					'std' => 'no-repeat',
					'hide-label' => 'true',
					'options' => array(
						'no-repeat' => 'no-repeat',
						'repeat' => 'repeat',
						'repeat-x' => 'repeat-x',
						'repeat-y' => 'repeat-y'
					)
				),
				'custom-style-sub-menu-bg-image-attachment' => array(
					'text' => esc_html__('Background Image Attachment','viettitan'),
					'type' => 'select',
					'std' => 'scroll',
					'hide-label' => 'true',
					'options' => array(
						'scroll' => 'scroll',
						'fixed' => 'fixed'
					)
				),
				'custom-style-sub-menu-bg-image-position' => array(
					'text' => esc_html__('Background Image Position','viettitan'),
					'type' => 'select',
					'std' => 'center',
					'hide-label' => 'true',
					'options' => array(
						'center' => 'center',
						'center left' => 'center left',
						'center right' => 'center right',
						'top left' => 'top left',
						'top center' => 'top center',
						'top right' => 'top right',
						'bottom left' => 'bottom left',
						'bottom center' => 'bottom center',
						'bottom right' => 'bottom right'
					)
				),
				'custom-style-sub-menu-bg-image-size' => array(
					'text' => esc_html__('Background Image Size','viettitan'),
					'type' => 'select',
					'std' => 'auto',
					'hide-label' => 'true',
					'options' => array(
						'auto' => 'Keep original',
						'100% auto' => 'Stretch to width',
						'auto 100%' => 'Stretch to height',
						'cover' => 'Cover',
						'contain' => 'Contain'
					)
				),
				'custom-style-col-min-width' => array(
					'text' => esc_html__('Column Min Width','viettitan'),
					'type' => 'text',
					'std'  => '',
					'des' => esc_html__('Set min-width for Sub Menu Column (px). Not include the units.','viettitan'),
				),
				'custom-style-padding' => array(
					'text' => esc_html__('Padding','viettitan'),
					'type' => 'text',
					'std'  => '',
					'des' => esc_html__('Set padding for Sub Menu. Include the units.','viettitan'),
				),

				'custom-style-feature-menu-text-heading' => array(
					'text' => esc_html__('Menu Feature Text','viettitan'),
					'type' => 'heading'
				),
				'custom-style-feature-menu-text-type' => array(
					'text' => esc_html__('Feature Menu Type','viettitan'),
					'type' => 'select',
					'std'  => '',
					'options' => array(
						'' => esc_html__('Standard','viettitan'),
						'x-feature-menu-not-float' => esc_html__('Not Float','viettitan')
					)
				),
				'custom-style-feature-menu-text-bg-color' => array(
					'text' => esc_html__('Background Color','viettitan'),
					'type' => 'color',
					'std'  => '',
				),
				'custom-style-feature-menu-text-color' => array(
					'text' => esc_html__('Text Color','viettitan'),
					'type' => 'color',
					'std'  => '',
				),
				'custom-style-feature-menu-text-top' => array(
					'text' => esc_html__('Position Top','viettitan'),
					'type' => 'text',
					'std'  => '',
					'des'  => 'Position Top (px) Feature Menu Text. Do not include units.',
				),
				'custom-style-feature-menu-text-left' => array(
					'text' => esc_html__('Position Left','viettitan'),
					'type' => 'text',
					'std'  => '',
					'des'  => 'Position Left (px) Feature Menu Text. Do not include units.',
				),
			)
		),
		'responsive' => array(
			'text' => esc_html__('Responsive','viettitan'),
			'icon' => 'fa-desktop',
			'config' => array(
				'responsive-heading' => array(
					'text' => esc_html__('Responsive','viettitan'),
					'type' => 'heading'
				),
				'responsive-hide-mobile-css' => array(
					'text' => esc_html__('Hide item on mobile via CSS','viettitan'),
					'type' => 'checkbox',
					'std' => ''
				),
				'responsive-hide-desktop-css' => array(
					'text' => esc_html__('Hide item on desktop via CSS','viettitan'),
					'type' => 'checkbox',
					'std' => ''
				),
				'responsive-hide-mobile-css-submenu' => array(
					'text' => esc_html__('Hide sub menu on mobile via CSS','viettitan'),
					'type' => 'checkbox',
					'std' => ''
				),
				'responsive-remove-mobile' => array(
					'text' => esc_html__('Remove this item when mobile device is detected via wp_is_mobile()','viettitan'),
					'type' => 'checkbox',
					'std' => ''
				),
				'responsive-remove-desktop' => array(
					'text' => esc_html__('Remove this item when desktop device is NOT detected via wp_is_mobile()','viettitan'),
					'type' => 'checkbox',
					'std' => ''
				),
				'responsive-remove-mobile-submenu' => array(
					'text' => esc_html__('Remove sub menu when desktop device is NOT detected via wp_is_mobile()','viettitan'),
					'type' => 'checkbox',
					'std' => ''
				),
			),
		),
		'responsive' => array(
			'text' => esc_html__('Responsive','viettitan'),
			'icon' => 'fa-desktop',
			'config' => array(
				'responsive-heading' => array(
					'text' => esc_html__('Responsive','viettitan'),
					'type' => 'heading'
				),
				'responsive-hide-mobile-css' => array(
					'text' => esc_html__('Hide item on mobile via CSS','viettitan'),
					'type' => 'checkbox',
					'std' => ''
				),
				'responsive-hide-desktop-css' => array(
					'text' => esc_html__('Hide item on desktop via CSS','viettitan'),
					'type' => 'checkbox',
					'std' => ''
				),
				'responsive-hide-mobile-css-submenu' => array(
					'text' => esc_html__('Hide sub menu on mobile via CSS','viettitan'),
					'type' => 'checkbox',
					'std' => ''
				),
				'responsive-hide-desktop-css-submenu' => array(
					'text' => esc_html__('Hide sub menu on desktop via CSS','viettitan'),
					'type' => 'checkbox',
					'std' => ''
				),
			),
		)
	);
	return $GLOBALS['xmenu_item_settings'];
}

function xmenu_get_item_defaults() {
	$defaults = array(
		'nosave-type_label' => '',
		'nosave-type' => '',
		'nosave-change' => 0
	);
	$items_setting = xmenu_get_item_settings();
	foreach ($items_setting as $seting_key => $setting) {
		foreach ($setting['config'] as $key => $value) {
			if (isset($value['config']) && $value['config']) {

			}
			else {
				if ($value['type'] != 'heading') {
					$defaults[$key] = $value['std'];
				}
			}

		}
	}
	return $defaults;
}
function xmenu_get_image_size() {
	global $_wp_additional_image_sizes;

	$sizes = array();
	$get_intermediate_image_sizes = get_intermediate_image_sizes();

	// Create the full array with sizes and crop info
	foreach( $get_intermediate_image_sizes as $_size ) {

		if ( in_array( $_size, array( 'thumbnail', 'medium', 'large' ) ) ) {

			$sizes[ $_size ]['width'] = get_option( $_size . '_size_w' );
			$sizes[ $_size ]['height'] = get_option( $_size . '_size_h' );
			$sizes[ $_size ]['crop'] = (bool) get_option( $_size . '_crop' );

		} elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {

			$sizes[ $_size ] = array(
				'width' => $_wp_additional_image_sizes[ $_size ]['width'],
				'height' => $_wp_additional_image_sizes[ $_size ]['height'],
				'crop' =>  $_wp_additional_image_sizes[ $_size ]['crop']
			);

		}

	}
	$image_size = array();
	$image_size ['full'] = esc_html__('Full Size','viettitan');
	foreach ($sizes as $key => $value) {
		$image_size[$key] = ucfirst($key) . ' (' . $value['width'] . ' x ' . $value['height'] .')' . ($value['crop'] ? '[cropped]' : '') ;
	}
	return $image_size;
}