<?php
$demo_site = array(
	'main' => array(
		'name' => esc_html__('Main','viettitan'),
		'path'  => 'viettitan/main',
	),
	'construction' => array(
		'name' => esc_html__('Construction','viettitan'),
		'path'  => 'viettitan/construction',
	),
	'hospital' => array(
		'name' => esc_html__('Hospital','viettitan'),
		'path'  => 'viettitan/hospital',
	),
	'handyman' => array(
		'name' => esc_html__('Handyman','viettitan'),
		'path'  => 'viettitan/handyman',
	),
	'cleaning' => array(
		'name' => esc_html__('Cleaning','viettitan'),
		'path'  => 'viettitan/cleaning',
	),
	'fitness' => array(
		'name' => esc_html__('Fitness','viettitan'),
		'path'  => 'viettitan/fitness',
	),
	'interior' => array(
		'name' => esc_html__('Interior','viettitan'),
		'path'  => 'viettitan/interior',
	),
	'lawyer' => array(
		'name' => esc_html__('Lawyer','viettitan'),
		'path'  => 'viettitan/lawyer',
	),
	'logistics' => array(
		'name' => esc_html__('Logistics','viettitan'),
		'path'  => 'viettitan/logistics',
	),
);
foreach ($demo_site as $key => $value) {
	$demo_site[$key]['image'] = VIETTITAN_THEME_URL . 'assets/data-demo/' . $key . '/preview.jpg';
}

$hide_fix_class = 'hide';
if (isset($_REQUEST['fix-demo-data']) && ($_REQUEST['fix-demo-data'] == '1')) {
$hide_fix_class = '';
}
?>
<div class="viettitan-demo-data-wrapper">
	<h1><?php esc_html_e('VIETTITAN - Install Demo Data','viettitan') ?></h1>
	<p><?php esc_html_e('Please choose demo to install (take about 3-35 mins)','viettitan') ?></p>
	<div class="install-message" data-success="<?php esc_html_e('Install Done','viettitan') ?>"></div>

	<div class="viettitan-demo-site-wrapper">
		<?php foreach ($demo_site as $key => $value): ?>
			<div class="viettitan-demo-site">
				<div class="viettitan-demo-site-inner">
					<div class="demo-site-thumbnail">
						<div class="centered">
							<img src="<?php echo esc_url($value['image'])?>" alt="<?php echo esc_attr($value['name'])?>"/>
						</div>
					</div>
				</div>
				<h3><span><?php echo esc_html($value['name'])?></span><span class="install-button" data-demo="<?php echo esc_attr($key) ?>" data-path="<?php echo esc_attr($value['path']) ?>"><?php esc_html_e('Install','viettitan'); ?></span></h3>
				<button class="fix_install_demo_error <?php echo esc_attr($hide_fix_class) ?>" data-demo="<?php echo esc_attr($key) ?>" data-path="<?php echo esc_attr($value['path']) ?>"><?php esc_html_e('Fix Setting','viettitan') ?></button>
			</div>
		<?php endforeach; ?>
		<div class="clear"></div>
	</div>
	<div class="install-progress-wrapper">
		<div class="title"><?php esc_html_e('Reset theme options','viettitan') ?></div>
		<div id="viettitan_reset_option" class="meter"><span style="width: 0%"></span></div>

		<div class="title"><?php esc_html_e('Install Demo Data','viettitan') ?></div>
		<div id="viettitan_install_demo" class="meter orange"><span style="width: 0%"></span></div>

		<div class="title"><?php esc_html_e('Import slider','viettitan') ?></div>
		<div id="viettitan_import_slider" class="meter red"><span style="width: 0%"></span></div>
	</div>
</div>