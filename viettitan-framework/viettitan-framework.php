<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 6/1/2015
 * Time: 8:51 AM
 */

/*================================================
CORE FILES
================================================== */
require_once( VIETTITAN_THEME_DIR . '/viettitan-framework/core/global.class.php' );
require_once( VIETTITAN_THEME_DIR . '/viettitan-framework/xmenu/xmenu.php' );
require_once( VIETTITAN_THEME_DIR . '/viettitan-framework/tax-meta-class/Tax-meta-class.php' );
require_once( VIETTITAN_THEME_DIR . '/viettitan-framework/meta-box/meta-box.php' );
require_once( VIETTITAN_THEME_DIR . '/viettitan-framework/core/resize.php' );
require_once( VIETTITAN_THEME_DIR . '/viettitan-framework/core/action.php' );
require_once( VIETTITAN_THEME_DIR . '/viettitan-framework/core/wp-core.php' );
require_once( VIETTITAN_THEME_DIR . '/viettitan-framework/core/filter.php' );
require_once( VIETTITAN_THEME_DIR . '/viettitan-framework/core/base.php' );
require_once( VIETTITAN_THEME_DIR . '/viettitan-framework/core/breadcrumb.php' );
require_once( VIETTITAN_THEME_DIR . '/viettitan-framework/core/head.php' );
require_once( VIETTITAN_THEME_DIR . '/viettitan-framework/core/header.php' );
require_once( VIETTITAN_THEME_DIR . '/viettitan-framework/core/footer.php' );
require_once( VIETTITAN_THEME_DIR . '/viettitan-framework/core/blog.php' );
require_once( VIETTITAN_THEME_DIR . '/viettitan-framework/core/woocommerce.php' );
require_once( VIETTITAN_THEME_DIR . '/viettitan-framework/core/widget-custom-class.php' );
