<?php
/**
 * Created by PhpStorm.
 * User: duonglh
 * Date: 8/23/14
 * Time: 3:01 PM
 */

function viettitan_generate_less()
{
    try{
	    $viettitan_options = get_option('viettitan_viettitan_options');
	    if ( ! defined( 'FS_METHOD' ) ) {
		    define('FS_METHOD', 'direct');
	    }


        $loading_animation = isset($viettitan_options['loading_animation']) ? $viettitan_options['loading_animation'] : 'none';
        $css_variable = viettitan_custom_css_variable();
        $custom_css = viettitan_custom_css();
        $fileout = get_template_directory() . "/assets/css/less/config.less";
        if ( !file_put_contents( $fileout, $css_variable, LOCK_EX ) ) {
            @chmod( $fileout, 0777 );
            file_put_contents( $fileout, $css_variable, LOCK_EX );
        }
        if (!class_exists('Less_Parser')) {
            require_once VIETTITAN_THEME_DIR . 'viettitan-framework/less/Less.php';
        }
        $parser = new Less_Parser(array( 'compress'=>true ));

        $parser->parse($css_variable);
        $parser->parseFile( VIETTITAN_THEME_DIR . 'assets/css/less/style.less' );

        if ($loading_animation != 'none' && !empty($loading_animation)) {
            $parser->parseFile( VIETTITAN_THEME_DIR . 'assets/css/less/loading/'.$loading_animation.'.less' );
        }

        if ( isset($viettitan_options['panel_selector']) && ($viettitan_options['panel_selector'] == 1)) {
            $parser->parseFile( VIETTITAN_THEME_DIR . 'assets/css/less/panel-style-selector.less' );
        }

        $parser->parse($custom_css);
        $css = $parser->getCss();

        require_once(ABSPATH . 'wp-admin/includes/file.php');
        WP_Filesystem();
        global $wp_filesystem;

        if (!$wp_filesystem->put_contents( VIETTITAN_THEME_DIR.   "style.min.css", $css, FS_CHMOD_FILE)) {
            return array(
                'status' => 'error',
                'message' => esc_html__('Could not save file','viettitan')
            );
        }

        $theme_info = $wp_filesystem->get_contents( VIETTITAN_THEME_DIR . "theme-info.txt" );

        $parser = new Less_Parser();
        $parser->parse($css_variable);
        $parser->parseFile(VIETTITAN_THEME_DIR . 'assets/css/less/style.less');
        if ($loading_animation != 'none' && !empty($loading_animation)) {
            $parser->parseFile( VIETTITAN_THEME_DIR . 'assets/css/less/loading/'.$loading_animation.'.less' );
        }

        if ( isset($viettitan_options['panel_selector']) && ($viettitan_options['panel_selector'] == 1)) {
            $parser->parseFile( VIETTITAN_THEME_DIR . 'assets/css/less/panel-style-selector.less' );
        }


        $parser->parse($custom_css);
        $css = $parser->getCss();

        $css = $theme_info . "\n" . $css;
	    $css = str_replace("\r\n","\n", $css);

        if (!$wp_filesystem->put_contents( VIETTITAN_THEME_DIR.   "style.css", $css, FS_CHMOD_FILE)) {
            return array(
                'status' => 'error',
                'message' => esc_html__('Could not save file','viettitan')
            );
        }

        return array(
            'status' => 'success',
            'message' => ''
        );

    }catch(Exception $e){
        $error_message = $e->getMessage();
        return array(
            'status' => 'error',
            'message' => $error_message
        );
    }
}