<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 6/1/2015
 * Time: 10:27 AM
 */
/*================================================
HEAD META
================================================== */
if (!function_exists('viettitan_head_meta')) {
	function viettitan_head_meta() {
		viettitan_get_template('head-meta');
	}
	add_action('wp_head','viettitan_head_meta',0);
}

/*================================================
SOCIAL META
================================================== */
if (!function_exists('viettitan_add_opengraph_doctype')) {
	function viettitan_add_opengraph_doctype($output) {
		$viettitan_options = &Viettitan_Global::get_options();
		$enable_social_meta = false;
		if (isset($viettitan_options['enable_social_meta'])) {
			$enable_social_meta = $viettitan_options['enable_social_meta'];
		}
		if (!$enable_social_meta) {
			return$output;
		}
		return $output . ' xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml"';

	}
	add_filter( 'language_attributes', 'viettitan_add_opengraph_doctype' );
}

if (!function_exists('viettitan_social_meta')) {
	function viettitan_social_meta() {
		viettitan_get_template('social-meta');
	}
	add_action( 'wp_head', 'viettitan_social_meta', 5 );
}

