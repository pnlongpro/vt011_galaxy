<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 6/19/2015
 * Time: 3:59 PM
 */
/*---------------------------------------------------
/* SEARCH AJAX
/*---------------------------------------------------*/
if (!function_exists('viettitan_result_search_callback')) {
    function viettitan_result_search_callback() {
        ob_start();

        $viettitan_options = &Viettitan_Global::get_options();
        $posts_per_page = 8;

        if (isset($viettitan_options['search_box_result_amount']) && !empty($viettitan_options['search_box_result_amount'])) {
            $posts_per_page = $viettitan_options['search_box_result_amount'];
        }

        $post_type = array();
        if (isset($viettitan_options['search_box_post_type']) && is_array($viettitan_options['search_box_post_type'])) {
            foreach($viettitan_options['search_box_post_type'] as $key => $value) {
                if ($value == 1) {
                    $post_type[] = $key;
                }
            }
        }


        $keyword = $_REQUEST['keyword'];

        if ( $keyword ) {
            $search_query = array(
                's' => $keyword,
                'order'     	=> 'DESC',
                'orderby'   	=> 'date',
                'post_status'	=> 'publish',
                'post_type' 	=> $post_type,
                'posts_per_page'         => $posts_per_page + 1,
            );
            $search = new WP_Query( $search_query );

            $newdata = array();
            if ($search && count($search->post) > 0) {
                $count = 0;
                foreach ( $search->posts as $post ) {
                    if ($count == $posts_per_page) {
                        $newdata[] = array(
                            'id'        => -2,
                            'title'     => '<a href="' . site_url() .'?s=' . $keyword . '"><i class="wicon icon-outline-vector-icons-pack-94"></i> ' . esc_html__('View More','viettitan') . '</a>',
                            'guid'      => '',
                            'date'      => null,
                        );

                        break;
                    }
                    $newdata[] = array(
                        'id'        => $post->ID,
                        'title'     => $post->post_title,
                        'guid'      => get_permalink( $post->ID ),
                        'date'      => mysql2date( 'M d Y', $post->post_date ),
                    );
                    $count++;

                }
            }
            else {
                $newdata[] = array(
                    'id'        => -1,
                    'title'     => esc_html__('Sorry, but nothing matched your search terms. Please try again with different keywords.','viettitan'),
                    'guid'      => '',
                    'date'      => null,
                );
            }

            ob_end_clean();
            echo json_encode( $newdata );
        }
        die(); // this is required to return a proper result
    }
    add_action( 'wp_ajax_nopriv_result_search', 'viettitan_result_search_callback' );
    add_action( 'wp_ajax_result_search', 'viettitan_result_search_callback' );

}

if (!function_exists('viettitan_result_search_product_callback')) {
	function viettitan_result_search_product_callback() {
		ob_start();

		$viettitan_options = &Viettitan_Global::get_options();
		$posts_per_page = 8;

		if (isset($viettitan_options['search_box_result_amount']) && !empty($viettitan_options['search_box_result_amount'])) {
			$posts_per_page = $viettitan_options['search_box_result_amount'];
		}

		$keyword = $_REQUEST['keyword'];
		$cate_id = isset($_REQUEST['cate_id']) ? $_REQUEST['cate_id'] : '-1';

		if ( $keyword ) {
			$search_query = array(
				's' => $keyword,
				'order'     	=> 'DESC',
				'orderby'   	=> 'date',
				'post_status'	=> 'publish',
				'post_type' 	=> array('product'),
				'posts_per_page'         => $posts_per_page + 1,
			);
			if (isset($cate_id) && ($cate_id != -1)) {
				$search_query ['tax_query'] = array(array(
					'taxonomy' => 'product_cat',
					'terms' => array($cate_id),
					'include_children' => true,
				));
			}

			$search = new WP_Query( $search_query );

			$newdata = array();
			if ($search && count($search->post) > 0) {
				$count = 0;
				foreach ( $search->posts as $post ) {
					if ($count >= $posts_per_page) {

						$category = get_term_by('id', $cate_id, 'product_cat', 'ARRAY_A');
						$cate_slug = isset($category['slug']) ? '&amp;product_cate=' . $category['slug'] : '';
						$newdata[] = array(
							'id'        => -2,
							'title'     => '<a href="' . site_url() .'?s=' . $keyword . '&amp;post_type=product' . $cate_slug . '"><i class="wicon icon-outline-vector-icons-pack-94"></i> ' . esc_html__('View More','viettitan') . '</a>',
						);
						break;
					}
					$product = new WC_Product( $post->ID );
					$price = $product->get_price_html();

					$newdata[] = array(
						'id'        => $post->ID,
						'title'     => $post->post_title,
						'guid'      => get_permalink( $post->ID ),
						'thumb'		=> get_the_post_thumbnail( $post->ID, 'thumbnail' ),
						'price'		=> $price
					);
					$count++;

				}
			}
			else {
				$newdata[] = array(
					'id'        => -1,
					'title'     => esc_html__('Sorry, but nothing matched your search terms. Please try again with different keywords.','viettitan'),
				);
			}

			ob_end_clean();
			echo json_encode( $newdata );
		}
		die(); // this is required to return a proper result
	}
	add_action( 'wp_ajax_nopriv_result_search_product', 'viettitan_result_search_product_callback' );
	add_action( 'wp_ajax_result_search_product', 'viettitan_result_search_product_callback' );
}

/*---------------------------------------------------
/* CUSTOM PAGE - LESS JS
/*---------------------------------------------------*/
if (!function_exists('viettitan_custom_page_less_js')) {
    function viettitan_custom_page_less_js() {
        echo viettitan_custom_css_variable();
        echo '@import "' . VIETTITAN_THEME_URL .'assets/css/less/style.less";', PHP_EOL;
        $viettitan_options = &Viettitan_Global::get_options();

        $loading_animation = isset($viettitan_options['loading_animation']) ? $viettitan_options['loading_animation'] : 'none';
        if ($loading_animation != 'none' && !empty($loading_animation)) {
            echo '@import "' . VIETTITAN_THEME_URL .'assets/css/less/loading/'.$loading_animation.'.less";', PHP_EOL;
        }

        if ( isset($viettitan_options['panel_selector']) && ($viettitan_options['panel_selector'] == 1)) {
            echo '@import "' . VIETTITAN_THEME_URL .'assets/css/less/panel-style-selector.less";', PHP_EOL;
        }

	    $enable_rtl_mode = '0';
	    if (isset($viettitan_options['enable_rtl_mode'])) {
		    $enable_rtl_mode =  $viettitan_options['enable_rtl_mode'];
	    }

		if (is_rtl() || $enable_rtl_mode == '1' || isset($_GET['RTL'])) {
			echo '@import "' . VIETTITAN_THEME_URL .'assets/css/less/rtl.less";', PHP_EOL;
		}
    }
    add_action('custom-page/less-js', 'viettitan_custom_page_less_js');
}


/*---------------------------------------------------
/* Add less script for developer
/*---------------------------------------------------*/
if (!function_exists('viettitan_add_less_for_dev')) {
    function viettitan_add_less_for_dev () {
        if (defined( 'VIETTITAN_SCRIPT_DEBUG' ) && VIETTITAN_SCRIPT_DEBUG) {
	        echo sprintf('<link rel="stylesheet/less" type="text/css" href="%s%s"/>',
		        VIETTITAN_THEME_URL . 'viettitan-less-css?custom-page=less-js',
		        isset($_GET['RTL']) ? '&RTL=1' : ''
		        );

            echo '<script src="'. VIETTITAN_THEME_URL. 'assets/js/less-1.7.3.min.js"></script>';

            $css = viettitan_custom_css();
            echo '<style>' . $css . '</style>';
        }
    }
    add_action('wp_head','viettitan_add_less_for_dev', 100);
}

/*---------------------------------------------------
/* Panel Selector
/*---------------------------------------------------*/
if (!function_exists('viettitan_panel_selector_callback')) {
    function viettitan_panel_selector_callback() {
        viettitan_get_template('panel-selector');
        die();
    }
    add_action( 'wp_ajax_nopriv_panel_selector', 'viettitan_panel_selector_callback' );
    add_action( 'wp_ajax_panel_selector', 'viettitan_panel_selector_callback' );
}

if (!function_exists('viettitan_panel_selector_change_color_callback')) {
    function viettitan_panel_selector_change_color_callback() {
        if (!class_exists('Less_Parser')) {
            require_once VIETTITAN_THEME_DIR . 'viettitan-framework/less/Less.php';
        }
        $content_file = viettitan_custom_css_variable();
        $primary_color = $_REQUEST['primary_color'];
        $content_file  .= '@primary_color:' . $primary_color . ';';
        $content_file  .= '@link_color:' . $primary_color . ';';
        $content_file  .= '@link_color_hover:' . $primary_color . ';';
        $content_file  .= '@link_color_active:' . $primary_color . ';';

        $file_full_variable = VIETTITAN_THEME_DIR . 'assets/css/less/variable.less';
        $file_color = VIETTITAN_THEME_DIR . 'assets/css/less/color.less';

        $parser = new Less_Parser(array( 'compress'=>true ));
        $parser->parse($content_file);
        $parser->parseFile($file_full_variable);
        $parser->parseFile($file_color);
        $css = $parser->getCss();
        echo sprintf('%s', $css);
        die();

    }
    add_action( 'wp_ajax_nopriv_custom_css_selector', 'viettitan_panel_selector_change_color_callback' );
    add_action( 'wp_ajax_custom_css_selector', 'viettitan_panel_selector_change_color_callback' );
}

/*---------------------------------------------------
/* Product Quick View
/*---------------------------------------------------*/
if (!function_exists('viettitan_product_quick_view_callback')) {
	function viettitan_product_quick_view_callback() {
		$product_id = $_REQUEST['id'];
		global $post, $product, $woocommerce;
		$post = get_post($product_id);
		setup_postdata($post);
		$product = wc_get_product( $product_id );
		wc_get_template_part('content-product-quick-view');
		wp_reset_postdata();
		die();
	}
	add_action( 'wp_ajax_nopriv_product_quick_view', 'viettitan_product_quick_view_callback' );
	add_action( 'wp_ajax_product_quick_view', 'viettitan_product_quick_view_callback' );
}

/*---------------------------------------------------
/* Blog Comment Like
/*---------------------------------------------------*/
if (!function_exists('viettitan_blog_comment_like_callback')) {
    function viettitan_blog_comment_like_callback() {
        $id = $_REQUEST['id'];
        $like_count = get_comment_meta($id,'viettitan-like',true) == '' ? 0 : get_comment_meta($id,'viettitan-like',true);
        $like_count+=1;
        update_comment_meta($id,'viettitan-like',$like_count);
        echo json_encode($like_count);
        die();
    }
    add_action( 'wp_ajax_nopriv_blog_comment_like', 'viettitan_blog_comment_like_callback' );
    add_action( 'wp_ajax_blog_comment_like', 'viettitan_blog_comment_like_callback' );
}



