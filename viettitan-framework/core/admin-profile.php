<?php
/**
 * Add extra profile fields for users in admin.
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
if (!class_exists('Viettitan_Admin_Profile')) {
	class Viettitan_Admin_Profile {
		/**
		 * Hook in tabs.
		 */
		public function __construct() {
			add_action( 'show_user_profile', array( $this, 'add_customer_meta_fields' ) );
			add_action( 'edit_user_profile', array( $this, 'add_customer_meta_fields' ) );

			add_action( 'personal_options_update', array( $this, 'save_customer_meta_fields' ) );
			add_action( 'edit_user_profile_update', array( $this, 'save_customer_meta_fields' ) );
		}


		public function get_customer_meta_fields() {
			$show_fields = apply_filters('viettitan_customer_meta_fields', array(
				'social-profiles' => array(
					'title' => esc_html__('Social Profiles','viettitan'),
					'fields' => array(
						'twitter_url' => array(
							'label' => esc_html__('Twitter','viettitan'),
							'description' => esc_html__('Your Twitter','viettitan'),
							'type' => 'text',
							'icon' => 'fa fa-twitter'
						),
						'facebook_url' => array(
							'label' => esc_html__('Facebook','viettitan'),
							'description' => esc_html__('Your facebook page/profile url','viettitan'),
							'type' => 'text',
							'icon' => 'fa fa-facebook'
						),
						'dribbble_url' => array(
							'label' => esc_html__('Dribbble','viettitan'),
							'description' => esc_html__('Your Dribbble','viettitan'),
							'type' => 'text',
							'icon' => 'fa fa-dribbble'
						),
						'vimeo_url' => array(
							'label' => esc_html__('Vimeo','viettitan'),
							'description' => esc_html__('Your Vimeo','viettitan'),
							'type' => 'text',
							'icon' => 'fa fa-vimeo-square'
						),
						'tumblr_url' => array(
							'label' => esc_html__('Tumblr','viettitan'),
							'description' => esc_html__('Your Tumblr','viettitan'),
							'type' => 'text',
							'icon' => 'fa fa-tumblr'
						),
						'skype_username' => array(
							'label' => esc_html__('Skype','viettitan'),
							'description' => esc_html__('Your Skype username','viettitan'),
							'type' => 'text',
							'icon' => 'fa fa-skype'
						),
						'linkedin_url' => array(
							'label' => esc_html__('LinkedIn','viettitan'),
							'description' => esc_html__('Your LinkedIn page/profile url','viettitan'),
							'type' => 'text',
							'icon' => 'fa fa-linkedin'
						),
						'googleplus_url' => array(
							'label' => esc_html__('Google+','viettitan'),
							'description' => esc_html__('Your Google+ page/profile URL','viettitan'),
							'type' => 'text',
							'icon' => 'fa fa-google-plus'
						),
						'flickr_url' => array(
							'label' => esc_html__('Flickr','viettitan'),
							'description' => esc_html__('Your Flickr page url','viettitan'),
							'type' => 'text',
							'icon' => 'fa fa-flickr'
						),
						'youtube_url' => array(
							'label' => esc_html__('YouTube','viettitan'),
							'description' => esc_html__('Your YouTube URL','viettitan'),
							'type' => 'text',
							'icon' => 'fa fa-youtube'
						),
						'pinterest_url' => array(
							'label' => esc_html__('Pinterest','viettitan'),
							'description' => esc_html__('Your Pinterest','viettitan'),
							'type' => 'text',
							'icon' => 'fa fa-pinterest'
						),
						'foursquare_url' => array(
							'label' => esc_html__('Foursquare','viettitan'),
							'description' => esc_html__('Your Foursqaure URL','viettitan'),
							'type' => 'text',
							'icon' => 'fa fa-foursquare'
						),
						'instagram_url' => array(
							'label' => esc_html__('Instagram','viettitan'),
							'description' => esc_html__('Your Instagram','viettitan'),
							'type' => 'text',
							'icon' => 'fa fa-instagram'
						),
						'github_url' => array(
							'label' => esc_html__('GitHub','viettitan'),
							'description' => esc_html__('Your GitHub URL','viettitan'),
							'type' => 'text',
							'icon' => 'fa fa-github'
						),
						'xing_url' => array(
							'label' => esc_html__('Xing','viettitan'),
							'description' => esc_html__('Your Xing URL','viettitan'),
							'type' => 'text',
							'icon' => 'fa fa-xing'
						),
						'behance_url' => array(
							'label' => esc_html__('Behance','viettitan'),
							'description' => esc_html__('Your Behance URL','viettitan'),
							'type' => 'text',
							'icon' => 'fa fa-behance'
						),
						'deviantart_url' => array(
							'label' => esc_html__('Deviantart','viettitan'),
							'description' => esc_html__('Your Deviantart URL','viettitan'),
							'type' => 'text',
							'icon' => 'fa fa-deviantart'
						),
						'soundcloud_url' => array(
							'label' => esc_html__('SoundCloud','viettitan'),
							'description' => esc_html__('Your SoundCloud URL','viettitan'),
							'type' => 'text',
							'icon' => 'fa fa-soundcloud'
						),
						'yelp_url' => array(
							'label' => esc_html__('Yelp','viettitan'),
							'description' => esc_html__('Your Yelp URL','viettitan'),
							'type' => 'text',
							'icon' => 'fa fa-yelp'
						),
						'rss_url' => array(
							'label' => esc_html__('RSS Feed','viettitan'),
							'description' => esc_html__('Your RSS Feed URL','viettitan'),
							'type' => 'text',
							'icon' => 'fa fa-rss'
						)
					)
				),
			) );
			return $show_fields;
		}



		public function add_customer_meta_fields( $user ) {

			$show_fields = $this->get_customer_meta_fields();

			foreach ( $show_fields as $fieldset ) :
				?>
				<h3><?php echo esc_html($fieldset['title']); ?></h3>
				<table class="form-table">
					<?php
					foreach ( $fieldset['fields'] as $key => $field ) :
						?>
						<tr>
							<th><label for="<?php echo esc_attr( $key ); ?>"><?php echo esc_html( $field['label'] ); ?></label></th>
							<td>
								<?php if ( ! empty( $field['type'] ) && 'select' == $field['type'] ) : ?>
									<select name="<?php echo esc_attr( $key ); ?>" id="<?php echo esc_attr( $key ); ?>" class="<?php echo ( ! empty( $field['class'] ) ? $field['class'] : '' ); ?>" style="width: 25em;">
										<?php
										$selected = esc_attr( get_user_meta( $user->ID, $key, true ) );
										foreach ( $field['options'] as $option_key => $option_value ) : ?>
											<option value="<?php echo esc_attr( $option_key ); ?>" <?php selected( $selected, $option_key, true ); ?>><?php echo esc_attr( $option_value ); ?></option>
										<?php endforeach; ?>
									</select>
								<?php else : ?>
									<input type="text" name="<?php echo esc_attr( $key ); ?>" id="<?php echo esc_attr( $key ); ?>" value="<?php echo esc_attr( get_user_meta( $user->ID, $key, true ) ); ?>" class="<?php echo ( ! empty( $field['class'] ) ? $field['class'] : 'regular-text' ); ?>" />
								<?php endif; ?>
								<br/>
								<span class="description"><?php echo wp_kses_post( $field['description'] ); ?></span>
							</td>
						</tr>
						<?php
					endforeach;
					?>
				</table>
				<?php
			endforeach;
		}


		public function save_customer_meta_fields( $user_id ) {
			$save_fields = $this->get_customer_meta_fields();

			foreach ( $save_fields as $fieldset ) {

				foreach ( $fieldset['fields'] as $key => $field ) {

					if ( isset( $_POST[ $key ] ) ) {
						update_user_meta( $user_id, $key, sanitize_text_field( $_POST[ $key ] ) );
					}
				}
			}
		}

	}
}
return new Viettitan_Admin_Profile();