<?php
/**
 * Created by PhpStorm.
 * User: phuongth
 * Date: 6/10/15
 * Time: 11:47 AM
 */

/*================================================
FOOTER
================================================== */
if (!function_exists('viettitan_footer_template')) {
    function viettitan_footer_template() {
        viettitan_get_template('footer/footer-template');
    }
    add_action('viettitan_main_wrapper_footer','viettitan_footer_template',10);
}

/*================================================
ADD BACK TO TOP BUTTON
================================================== */
if (!function_exists('viettitan_back_to_top')) {
    function viettitan_back_to_top() {
        $viettitan_options = &Viettitan_Global::get_options();
        $back_to_top = $viettitan_options['back_to_top'];
        if ($back_to_top == 1) {
            viettitan_get_template('back-to-top');
        }
    }
    add_action('viettitan_after_page_wrapper','viettitan_back_to_top',5);
}