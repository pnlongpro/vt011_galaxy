<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content clearfix">
		<?php the_content(); ?>
	</div>
	<?php wp_link_pages(array(
		'before' => '<div class="viettitan-page-links"><span class="viettitan-page-links-title">' . esc_html__('Pages:', 'viettitan') . '</span>',
		'after' => '</div>',
		'link_before' => '<span class="viettitan-page-link">',
		'link_after' => '</span>',
	)); ?>

</div>