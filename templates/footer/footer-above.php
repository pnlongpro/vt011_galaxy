<?php
/**
 * Created by PhpStorm.
 * User: phuongth
 * Date: 9/26/15
 * Time: 3:52 PM
 */
$viettitan_options = &Viettitan_Global::get_options();
$prefix = 'viettitan_';

$footer_above_enable = rwmb_meta($prefix . 'footer_above_enable');
if (!isset($footer_above_enable) || $footer_above_enable == '-1' || $footer_above_enable=='') {
    $footer_above_enable = $viettitan_options['footer_above'];
}
if ($footer_above_enable != '1') {
	return;
}

$footer_above_layout_custom = rwmb_meta($prefix . 'footer_above_layout');
$footer_above_layout = $footer_above_layout_custom;
if (!isset($footer_above_layout) || $footer_above_layout == '-1' || $footer_above_layout == '') {
    $footer_above_layout = isset($viettitan_options['footer_above_layout']) ? $viettitan_options['footer_above_layout'] : 'footer-above-1';
}

if (!isset($footer_above_layout_custom) || $footer_above_layout_custom == '-1' || $footer_above_layout_custom == '') {
    $footer_above_left_sidebar = isset($viettitan_options['footer_above_left_sidebar']) ? $viettitan_options['footer_above_left_sidebar'] : '';
} else {
    $footer_above_left_sidebar = rwmb_meta($prefix . 'footer_above_left_sidebar');
}

if (!isset($footer_above_layout_custom) || $footer_above_layout_custom == '-1' || $footer_above_layout_custom == '') {
    $footer_above_right_sidebar = isset($viettitan_options['footer_above_right_sidebar']) ? $viettitan_options['footer_above_right_sidebar'] : '';
} else {
    $footer_above_right_sidebar = rwmb_meta($prefix . 'footer_above_right_sidebar');
}

$col_left_class = $col_right_class = '';
switch ($footer_above_layout) {
    case 'footer-above-2':
        $col_left_class = $col_right_class = 'col-md-6';
        break;
}

$sidebar_bottom_right_class = array($col_right_class, 'sidebar', 'text-right');
$sidebar_bottom_left_class = array($col_left_class, 'sidebar');

$viettitan_footer_container_layout = &Viettitan_Global::get_footer_container_layout();

if ((($footer_above_left_sidebar != '' && is_active_sidebar($footer_above_left_sidebar)) ||
        ($footer_above_right_sidebar != '' && is_active_sidebar($footer_above_right_sidebar)))):
?>
    <div class="footer-above-wrapper">
        <div class="footer-above-inner">
            <div class="<?php echo esc_attr($viettitan_footer_container_layout) ?>">
	            <div class="row">
		            <?php if ($footer_above_left_sidebar != '' && is_active_sidebar($footer_above_left_sidebar)): ?>
			            <div class="<?php echo join(' ', $sidebar_bottom_left_class) ?>">
				            <?php dynamic_sidebar($footer_above_left_sidebar); ?>
			            </div>
		            <?php endif; ?>
		            <?php if ($footer_above_right_sidebar != '' && is_active_sidebar($footer_above_right_sidebar)): ?>
			            <div class="<?php echo join(' ', $sidebar_bottom_right_class) ?>">
				            <?php dynamic_sidebar($footer_above_right_sidebar); ?>
			            </div>
		            <?php endif; ?>
	            </div>
            </div>
        </div>
    </div>
<?php endif; ?>