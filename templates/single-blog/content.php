<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 */
$viettitan_archive_loop = &Viettitan_Global::get_archive_loop();

if (isset($viettitan_archive_loop['image-size'])) {
    $size = $viettitan_archive_loop['image-size'];
} else {
    $size = 'full';
}

$class = array();
$class[]= "clearfix";

?>
<article id="post-<?php the_ID(); ?>" <?php post_class($class); ?>>
    <?php
    $thumbnail = viettitan_post_thumbnail($size);
    if (!empty($thumbnail)) : ?>
        <div class="entry-thumbnail-wrap">
            <?php echo wp_kses_post($thumbnail); ?>
        </div>
    <?php endif; ?>
    <div class="entry-content-wrap">
        <h3 class="entry-post-title p-font"><?php the_title(); ?></h3>
        <div class="entry-post-meta-wrap">
            <?php viettitan_post_meta(); ?>
        </div>
        <div class="entry-content clearfix">
            <?php
                the_content();
                viettitan_link_pages();
            ?>
        </div>
        <?php
        /**
         * @hooked - viettitan_post_tags - 10
         * @hooked - viettitan_share - 15
         *
         **/
        do_action('viettitan_after_single_post_content');
        ?>
    </div>
    <?php
    /**
     * @hooked - viettitan_post_nav - 20
     * @hooked - viettitan_post_author_info - 25
     * @hooked - viettitan_post_related - 30
     *
     **/
    do_action('viettitan_after_single_post');
    ?>
</article>
