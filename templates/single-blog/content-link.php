<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 11/2/2015
 * Time: 3:05 PM
 */
$prefix = 'viettitan_';
$url = rwmb_meta($prefix.'post_format_link_url',array(),get_the_ID());
$text = rwmb_meta($prefix.'post_format_link_text',array(),get_the_ID());

$class = array();
$class[]= "clearfix";
?>
<article id="post-<?php the_ID(); ?>" <?php post_class($class); ?>>
	<?php if (!empty($url) && !empty($text)) : ?>
	<div class="entry-link-wrap">
		<i class="fa fa-link"></i>
		<p>
			<a href="<?php echo esc_url($url); ?>" rel="bookmark">
				<?php echo esc_html($text); ?>
			</a>
		</p>
	</div>
	<?php endif; ?>

	<div class="entry-content-wrap">
		<h3 class="entry-post-title p-font"><?php the_title(); ?></h3>
		<div class="entry-post-meta-wrap">
			<?php viettitan_post_meta(); ?>
		</div>
		<div class="entry-content clearfix">
			<?php
			the_content();
			viettitan_link_pages();
			?>
		</div>
		<?php
		/**
		 * @hooked - viettitan_post_tags - 10
		 * @hooked - viettitan_share - 15
		 *
		 **/
		do_action('viettitan_after_single_post_content');
		?>
	</div>
	<?php
	/**
	 * @hooked - viettitan_post_nav - 20
	 * @hooked - viettitan_post_author_info - 25
	 * @hooked - viettitan_post_related - 30
	 *
	 **/
	do_action('viettitan_after_single_post');
	?>
</article>

