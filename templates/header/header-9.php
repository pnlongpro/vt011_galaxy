<?php
$viettitan_options = &Viettitan_Global::get_options();
$prefix = 'viettitan_';
$header_class = array('header-nav-wrapper', 'header-9');

// HEADER NAV CUSTOM STYLE
$header_custom_style = '';

// GET HEADER NAVIGATION SCHEME
$header_nav_scheme = rwmb_meta($prefix . 'header_nav_scheme');
if (is_404()) {
    $header_nav_scheme = 'header-light';
}

if ($header_nav_scheme == 'header-overlay') {
    $header_nav_scheme_color = rwmb_meta($prefix . 'header_nav_scheme_color');
    $header_nav_scheme_opacity = rwmb_meta($prefix . 'header_nav_scheme_opacity');
    if (($header_nav_scheme_color !== '') && ($header_nav_scheme_opacity != '')) {
        $header_custom_style = sprintf(' style="background-color:%s"', viettitan_hex2rgba($header_nav_scheme_color, $header_nav_scheme_opacity * 1.0 / 100));
    }
}
if (($header_nav_scheme === '') || ($header_nav_scheme == '-1')) {
    $header_nav_scheme = $viettitan_options['header_nav_scheme'];
    if ($header_nav_scheme == 'header-overlay') {
        $header_nav_scheme_color = $viettitan_options['header_nav_scheme_color'];
        $header_nav_scheme_opacity = $viettitan_options['header_nav_scheme_opacity'];
        if (($header_nav_scheme_color !== '') && ($header_nav_scheme_opacity != '')) {
            $header_custom_style = sprintf(' style="background-color:%s"', viettitan_hex2rgba($header_nav_scheme_color, $header_nav_scheme_opacity * 1.0 / 100));
        }
    }
}
$header_class[] = $header_nav_scheme;

// GET HEADER NAVIGATION BORDER TOP
$header_nav_border_top = rwmb_meta($prefix . 'header_nav_border_top');
if (($header_nav_border_top === '') || ($header_nav_border_top == '-1')) {
    $header_nav_border_top = $viettitan_options['header_nav_border_top'];
}
if (is_404()) {
    $header_nav_border_top = $viettitan_options['header_404_nav_border_top'];
}
if ($header_nav_border_top != 'none') {
    $header_class[] = $header_nav_border_top;
}

// GET HEADER NAVIGATION BORDER BOTTOM
$header_nav_border_bottom = rwmb_meta($prefix . 'header_nav_border_bottom');
if (($header_nav_border_bottom === '') || ($header_nav_border_bottom == '-1')) {
    $header_nav_border_bottom = $viettitan_options['header_nav_border_bottom'];
}
if (is_404() && $viettitan_options['header_default_404'] == '0') {
    $header_nav_border_bottom = $viettitan_options['header_404_nav_border_bottom'];
}
if ($header_nav_border_bottom != 'none') {
    $header_class[] = $header_nav_border_bottom;
}

// GET HEADER STICKY
$header_sticky = rwmb_meta($prefix . 'header_sticky');
if (($header_sticky === '') || ($header_sticky == '-1')) {
    $header_sticky = $viettitan_options['header_sticky'];
}
if ($header_sticky == '1') {
    $header_class[] = 'header-sticky';

    $header_sticky_scheme = rwmb_meta($prefix . 'header_sticky_scheme');
    if (($header_sticky_scheme == '') || ($header_sticky_scheme == '-1')) {
        $header_sticky_scheme = isset($viettitan_options['header_sticky_scheme']) ? $viettitan_options['header_sticky_scheme'] : 'sticky-inherit';
    }
    $header_class[] = $header_sticky_scheme;
}

// GET HEADER CONTAINER LAYOUT
$header_container_layout = rwmb_meta($prefix . 'header_container_layout');
if (($header_container_layout == '') || ($header_container_layout == '-1')) {
    $header_container_layout = $viettitan_options['header_container_layout'];
}

// GET PAGE MENU
$page_menu = rwmb_meta($prefix . 'page_menu');
?>
<div class="<?php echo join(' ', $header_class) ?>"<?php echo sprintf('%s', $header_custom_style) ?>>
    <div class="<?php echo esc_attr($header_container_layout) ?>">
        <div class="header-container clearfix">
            <?php viettitan_get_template('header/header-logo'); ?>
            <div class="header-search-box">
                <?php
                $args = array(
                    'show_option_none' => __('DANH MỤC SẢN PHẨM', 'viettitan'),
                    'taxonomy' => 'product_cat',
                    'class' => 'select-category',
                    'hide_empty' => 0,
                    'orderby' => 'name',
                    'order' => 'asc',
                    'tab_index' => true,
                    'hierarchical' => true
                );

                ?>
                <form class="form-inline woo-search" method="get" action="<?php echo esc_url(home_url('/')) ?>">
                    <div class="form-group form-category">
                        <?php wp_dropdown_categories($args); ?>
                    </div>
                    <div class="form-group input-serach">
                        <input type="hidden" name="post_type" value="product"/>
                        <input value="<?php echo esc_attr(get_search_query()); ?>" type="text" name="s"
                               placeholder="<?php esc_attr_e('Tìm kiếm ...', 'viettitan'); ?>"/>
                        <i class="hide close-form fa fa-times"></i>
                        <button type="submit" class="btn-search"><i class="fa fa-search"></i></button>
                    </div>
                </form>
            </div><!--end search-header-->
        </div>
    </div>
</div>

<div class="main-nav">
    <div class="container">
        <div class="top-main-menu style1">
            <div id="box-vertical-megamenus" class="vertical-wapper">
                <div class="box-vertical-megamenus <?php echo is_front_page() ? 'open' : ''; ?>" id="brand-category">
                    <h4 class="title <?php echo is_front_page() ? 'cat-home' : ''; ?>">
                        <span class="title-menu"><?php _e('DANH MỤC SẢN PHẨM', 'viettitan'); ?></span>
                        <span class="btn-open-mobile home-page"><i class="fa fa-bars"></i></span>
                    </h4>
                    <div class="vertical-menu-content">
                        <?php if (has_nav_menu('Category')) : ?>
                            <div id="vertical-menu" class="menu-vertical-wrapper">
                                <?php
                                $arg_menu = array(
                                    'menu_id' => 'category-menu',
                                    'container' => '',
                                    'theme_location' => 'Category',
                                    'menu_class' => 'category-menu',
                                    //'walker' => new XMenuWalker()
                                );
                                if (!empty($page_menu)) {
                                    $arg_menu['menu'] = $page_menu;
                                }
                                wp_nav_menu($arg_menu);
                                ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="header-nav-right">
                <?php if (has_nav_menu('primary')) : ?>
                    <div id="primary-menu" class="menu-wrapper">
                        <?php
                        $arg_menu = array(
                            'menu_id' => 'main-menu',
                            'container' => '',
                            'theme_location' => 'primary',
                            'menu_class' => 'main-menu',
                            'walker' => new XMenuWalker()
                        );
                        if (!empty($page_menu)) {
                            $arg_menu['menu'] = $page_menu;
                        }
                        wp_nav_menu($arg_menu);
                        ?>
                    </div>
                <?php endif; ?>
                <?php viettitan_get_template('header/header-customize-nav'); ?>
            </div>
        </div>
    </div>
</div><!--end main-nav-->


<?php

?>



