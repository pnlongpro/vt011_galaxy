<?php
$viettitan_options = &Viettitan_Global::get_options();
$viettitan_header_layout = &Viettitan_Global::get_header_layout();
$prefix = 'viettitan_';

// Get search & mini-cart for header mobile
$mobile_header_shopping_cart = rwmb_meta($prefix . 'mobile_header_shopping_cart');
if (($mobile_header_shopping_cart === '') || ($mobile_header_shopping_cart == '-1')) {
	$mobile_header_shopping_cart = $viettitan_options['mobile_header_shopping_cart'];
}

$mobile_header_search_box = rwmb_meta($prefix . 'mobile_header_search_box');
if (($mobile_header_search_box === '') || ($mobile_header_search_box == '-1')) {
	$mobile_header_search_box = $viettitan_options['mobile_header_search_box'];
}

$mobile_header_menu_drop = rwmb_meta($prefix . 'mobile_header_menu_drop');
if (($mobile_header_menu_drop === '') || ($mobile_header_menu_drop == '-1')) {
	$mobile_header_menu_drop = 'dropdown';
	if (isset($viettitan_options['mobile_header_menu_drop']) && !empty($viettitan_options['mobile_header_menu_drop'])) {
		$mobile_header_menu_drop = $viettitan_options['mobile_header_menu_drop'];
	}
}

$header_container_wrapper_class = array('header-container-wrapper');

$mobile_header_stick = rwmb_meta($prefix . 'mobile_header_stick');
if (($mobile_header_stick === '') || ($mobile_header_stick == '-1')) {
	$mobile_header_stick = isset($viettitan_options['mobile_header_stick']) ? $viettitan_options['mobile_header_stick'] : '0';
}
if ($mobile_header_stick == '1') {
	$header_container_wrapper_class[] = 'header-mobile-sticky';
}

$header_mobile_nav = array('header-mobile-nav' , 'menu-drop-' . $mobile_header_menu_drop);
?>
<div class="<?php echo join(' ', $header_container_wrapper_class); ?>">
	<div class="container header-mobile-container">
		<div class="header-mobile-inner">
			<div class="toggle-icon-wrapper toggle-mobile-menu" data-ref="nav-menu-mobile" data-drop-type="<?php echo esc_attr($mobile_header_menu_drop); ?>">
				<div class="toggle-icon"> <span></span></div>
			</div>
			<div class="header-customize">
				<?php if (($mobile_header_shopping_cart == '1') && class_exists( 'WooCommerce' )): ?>
					<?php viettitan_get_template('header/mini-cart'); ?>
				<?php endif; ?>
			</div>
			<?php viettitan_get_template('header/header-mobile-logo'); ?>
		</div>
		<?php viettitan_get_template('header/header-mobile-nav'); ?>
	</div>
</div>
<div class="container">
	<div class="search-mobile-wrapper">
		<form method="get" action="<?php echo esc_url(site_url()); ?>">
			<input type="text" name="s" placeholder="Search...">
			<i class="micon icon-magnifying-glass34"></i>
		</form>
	</div>
</div>
