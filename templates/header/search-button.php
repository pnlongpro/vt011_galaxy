<?php
	$viettitan_options = &Viettitan_Global::get_options();

	$prefix = 'viettitan_';

	$data_search_type = 'standard';
	if (isset($viettitan_options['search_box_type']) && ($viettitan_options['search_box_type'] == 'ajax')) {
		$data_search_type = 'ajax';
	}
	$search_box_type = 'standard';
	$search_box_submit = 'submit';
	if (isset($viettitan_options['search_box_type'])) {
		$search_box_type = $viettitan_options['search_box_type'];
	}
	if ($search_box_type == 'ajax') {
		$search_box_submit = 'button';
	}

	$header_customize_nav_search_button_style = 'default';
	switch (Viettitan_Global::get_header_customize_current()) {
		case 'nav':
			$enable_header_customize_nav = rwmb_meta($prefix . 'enable_header_customize_nav');
			if ($enable_header_customize_nav == '1') {
				$header_customize_nav_search_button_style = rwmb_meta($prefix . 'header_customize_nav_search_button_style');
			}
			else {
				$header_customize_nav_search_button_style = isset($viettitan_options['header_customize_nav_search_button_style']) && !empty($viettitan_options['header_customize_nav_search_button_style'])
					? $viettitan_options['header_customize_nav_search_button_style'] : 'default';
			}

			break;
		case 'left':
			$enable_header_customize_left = rwmb_meta($prefix . 'enable_header_customize_left');
			if ($enable_header_customize_left == '1') {
				$header_customize_nav_search_button_style = rwmb_meta($prefix . 'header_customize_left_search_button_style');
			}
			else {
				$header_customize_nav_search_button_style = isset($viettitan_options['header_customize_left_search_button_style']) && !empty($viettitan_options['header_customize_left_search_button_style'])
					? $viettitan_options['header_customize_left_search_button_style'] : 'default';
			}
			break;
		case 'right':
			$enable_header_customize_right = rwmb_meta($prefix . 'enable_header_customize_right');
			if ($enable_header_customize_right == '1') {
				$header_customize_nav_search_button_style = rwmb_meta($prefix . 'header_customize_right_search_button_style');
			}
			else {
				$header_customize_nav_search_button_style = isset($viettitan_options['header_customize_right_search_button_style']) && !empty($viettitan_options['header_customize_right_search_button_style'])
					? $viettitan_options['header_customize_right_search_button_style'] : 'default';
			}
			break;
	}

	$search_button_wrapper_class = array(
		'search-button-wrapper',
		'header-customize-item',
		'style-' . esc_attr($header_customize_nav_search_button_style)
	);
?>
<div class="<?php echo join(' ', $search_button_wrapper_class) ?>">
	<a class="icon-search-menu" href="#" data-search-type="<?php echo esc_attr($data_search_type) ?>"><i class="micon icon-search-icon"></i></a>
</div>