<?php
$viettitan_options = &Viettitan_Global::get_options();
?>
<?php if (isset($viettitan_options['search_box_type']) && ($viettitan_options['search_box_type'] == 'ajax')): ?>
	<div id="viettitan-modal-search" tabindex="-1" role="dialog" aria-hidden="false" class="modal fade">
		<div class="modal-backdrop fade in"></div>
		<div class="viettitan-modal-dialog viettitan-modal-search fade in">
			<div data-dismiss="modal" class="viettitan-dismiss-modal"><i class="micon icon-wrong6"></i></div>
			<div class="viettitan-search-wrapper">
				<input id="search-ajax" type="search" placeholder="<?php echo esc_html__('Type at least 3 characters to search','viettitan') ?>">
				<button><i class="ajax-search-icon micon icon-search-icon"></i></button>
			</div>
			<div class="ajax-search-result"></div>
		</div>
	</div>
<?php else: ?>
	<div id="search_popup_wrapper" class="dialog">
		<div class="dialog__overlay"></div>
		<div class="dialog__content">
			<div class="morph-shape">
				<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 520 280"
				     preserveAspectRatio="none">
					<rect x="3" y="3" fill="none" width="516" height="276"/>
				</svg>
			</div>
			<div class="dialog-inner">
				<h2><?php esc_html_e('Enter your keyword','viettitan'); ?></h2>
				<form  method="get" action="<?php echo esc_url(home_url('/')); ?>" class="search-popup-inner">
					<input type="text" name="s" placeholder="<?php esc_html_e('Search...','viettitan'); ?>">
					<button type="submit"><?php esc_html_e('Search','viettitan'); ?></button>
				</form>
				<div><button class="action" data-dialog-close="close" type="button"><i class="fa fa-close"></i></button></div>
			</div>
		</div>
	</div>
<?php endif;?>