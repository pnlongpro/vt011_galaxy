<?php
$viettitan_options = &Viettitan_Global::get_options();
$viettitan_header_layout = &Viettitan_Global::get_header_layout();
$prefix = 'viettitan_';
Viettitan_Global::set_header_customize_current('left');

$header_customize_class = array('header-customize header-customize-left');

$header_customize = array();
$enable_header_customize = rwmb_meta($prefix . 'enable_header_customize_left');
if ($enable_header_customize == '1') {
	$page_header_customize = rwmb_meta($prefix . 'header_customize_left');
	if (isset($page_header_customize['enable']) && !empty($page_header_customize['enable'])) {
		$header_customize = explode('||', $page_header_customize['enable']);
	}

	$header_customize_left_separate = rwmb_meta($prefix . 'header_customize_left_separate');
	if ($header_customize_left_separate == '1') {
		$header_customize_class[] = 'header-customize-separate';
	}
}
else {
	if (isset($viettitan_options['header_customize_left']) && isset($viettitan_options['header_customize_left']['enabled']) && is_array($viettitan_options['header_customize_left']['enabled'])) {
		foreach ($viettitan_options['header_customize_left']['enabled'] as $key => $value) {
			$header_customize[] = $key;
		}
	}
	if (isset($viettitan_options['header_customize_nav_separate']) && ($viettitan_options['header_customize_left_separate'] == '1')){
		$header_customize_class[] = 'header-customize-separate';
	}
}

?>
<?php if (count($header_customize) > 0): ?>
	<div class="<?php echo join(' ', $header_customize_class) ?>">
		<?php foreach ($header_customize as $key){
			switch ($key) {
				case 'search-button':
					if ($viettitan_header_layout == 'header-7') {
						viettitan_get_template('header/search-box');
					}
					else {
						viettitan_get_template('header/search-button');
					}

					break;
				case 'shopping-cart':
					if (class_exists( 'WooCommerce' )) {
						viettitan_get_template('header/mini-cart');
					}
					break;
				case 'social-profile':
					viettitan_get_template('header/social-profile');
					break;
				case 'canvas-menu':
					viettitan_get_template('header/canvas-menu');
					break;
				case 'custom-text':
					viettitan_get_template('header/custom-text');
					break;
			}
		} ?>
	</div>
<?php endif;?>