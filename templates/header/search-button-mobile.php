<?php
	$viettitan_options = &Viettitan_Global::get_options();
	$viettitan_header_layout = &Viettitan_Global::get_header_layout();
	$prefix = 'viettitan_';

	$data_search_type = 'standard';
	if (isset($viettitan_options['search_box_type']) && ($viettitan_options['search_box_type'] == 'ajax')) {
		$data_search_type = 'ajax';
	}
?>
<div class="search-button-wrapper header-customize-item">
	<a class="icon-search-menu" href="#" data-search-type="<?php echo esc_attr($data_search_type) ?>"><i class="micon icon-magnifying-glass34"></i></a>
</div>