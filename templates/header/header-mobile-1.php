<?php
$viettitan_options = &viettitan_Global::get_options();
$viettitan_header_layout = &viettitan_Global::get_header_layout();
$prefix = 'viettitan_';

// Get search & mini-cart for header mobile
$mobile_header_shopping_cart = rwmb_meta($prefix . 'mobile_header_shopping_cart');
if (($mobile_header_shopping_cart === '') || ($mobile_header_shopping_cart == '-1')) {
	$mobile_header_shopping_cart = $viettitan_options['mobile_header_shopping_cart'];
}

$mobile_header_search_box = rwmb_meta($prefix . 'mobile_header_search_box');
if (($mobile_header_search_box === '') || ($mobile_header_search_box == '-1')) {
	$mobile_header_search_box = $viettitan_options['mobile_header_search_box'];
}

$mobile_header_menu_drop = rwmb_meta($prefix . 'mobile_header_menu_drop');
if (($mobile_header_menu_drop === '') || ($mobile_header_menu_drop == '-1')) {
	$mobile_header_menu_drop = 'dropdown';
	if (isset($viettitan_options['mobile_header_menu_drop']) && !empty($viettitan_options['mobile_header_menu_drop'])) {
		$mobile_header_menu_drop = $viettitan_options['mobile_header_menu_drop'];
	}
}

$header_container_wrapper_class = array('header-container-wrapper');

$mobile_header_stick = rwmb_meta($prefix . 'mobile_header_stick');
if (($mobile_header_stick === '') || ($mobile_header_stick == '-1')) {
	$mobile_header_stick = isset($viettitan_options['mobile_header_stick']) ? $viettitan_options['mobile_header_stick'] : '0';
}
if ($mobile_header_stick == '1') {
	$header_container_wrapper_class[] = 'header-mobile-sticky';
}

$header_mobile_nav = array('header-mobile-nav' , 'menu-drop-' . $mobile_header_menu_drop);
?>
<div class="<?php echo join(' ', $header_container_wrapper_class); ?>">
		<div class="container header-mobile-container">
		<?php viettitan_get_template('header/header-mobile-logo'); ?>
		        <div class="header-search-box clearfix">
	        	<?php
	        		$args = array(
					  'show_option_none' => __( 'Danh mục', 'viettitan' ),
					  'taxonomy'    => 'product_cat',
					  'class'      => 'select-category',
					  'hide_empty'  => 0,
					  'orderby'     => 'name',
					  'order'       => 'asc',
					  'tab_index'   => true,
					  'hierarchical' => true
					);

	        	 ?>
	        	 <form class="form-inline woo-search" method="get" action="<?php echo esc_url( home_url( '/' ) ) ?>">
				  <div class="form-group form-category">
				    <?php wp_dropdown_categories( $args ); ?>
				  </div>
				  <div class="form-group input-serach">
				    <input type="hidden" name="post_type" value="product" />
				    <input value="<?php echo esc_attr( get_search_query() );?>" type="text" name="s"  placeholder="<?php esc_attr_e( 'Tìm Kiếm..', 'viettitan' ); ?>" />
				    <i class="hide close-form fa fa-times"></i>
				  </div>
				  <button type="submit" class="btn-search"><i class="fa fa-search"></i></button>
				</form>
	        </div><!--end search-header-->
		<div class="header-mobile-inner">
			<div class="toggle-icon-wrapper toggle-mobile-menu" data-ref="nav-menu-mobile" data-drop-type="<?php echo esc_attr($mobile_header_menu_drop); ?>">
				<div class="toggle-icon"> <span></span></div>
			</div>
			            <div  class="box-vertical-megamenus <?php echo is_front_page() ? 'open' : ''; ?>" id="brand-category">
                <h3 class="title">
                    <span class="title-menu"><?php _e( 'Categories', 'viettitan' ); ?></span>
                    <span class="btn-open-mobile home-page"><i class="fa fa-bars"></i></span>
                </h3>
                <div class="vertical-menu-content">
                    	<?php if (has_nav_menu('Category')) : ?>
					<div id="vertical-menu" class="menu-vertical-wrapper">
						<?php
						$arg_menu = array(
							'menu_id' => 'category-menu',
							'container' => '',
							'theme_location' => 'Category',
							'menu_class' => 'category-menu',
							'walker' => new XMenuWalker()
						);
						if (!empty($page_menu)) {
							$arg_menu['menu'] = $page_menu;
						}
						wp_nav_menu( $arg_menu );
						?>
					</div>
				<?php endif; ?>
                </div>
            </div>
		<?php viettitan_get_template('header/header-mobile-nav'); ?>
	</div>
</div>