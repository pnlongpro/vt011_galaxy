<?php
	if (!Viettitan_Global::is_bind_canvas_menu()) {
		add_action('viettitan_after_page_wrapper','viettitan_add_canvas_menu_region');
		function viettitan_add_canvas_menu_region() {
			?>
				<nav class="canvas-menu-wrapper dark">
					<a href="#" class="canvas-menu-close"><i class="micon icon-wrong6"></i></a>
					<div class="canvas-menu-inner sidebar">
						<?php if (is_active_sidebar('canvas-menu')) { dynamic_sidebar('canvas-menu'); } ?>
					</div>
				</nav>
			<?php
		}
	}
?>
<div class="header-customize-item canvas-menu-toggle-wrapper">
	<a class="canvas-menu-toggle" href="#"><i class="micon icon-menu27"></i></a>
</div>