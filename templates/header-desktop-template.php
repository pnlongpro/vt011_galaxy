<?php
$viettitan_options = &Viettitan_Global::get_options();
$viettitan_header_layout = &Viettitan_Global::get_header_layout();
$prefix = 'viettitan_';

$header_class = array('main-header');

// GET HEADER BOXED
$header_boxed = rwmb_meta($prefix . 'header_boxed');
if (($header_boxed == '') || ($header_boxed == '-1')) {
	$header_boxed = $viettitan_options['header_boxed'];
}
if ($header_boxed == '1') {
	$header_class[] = 'header-boxed';
}

// GET HEADER FLOAT
$header_float = rwmb_meta($prefix . 'header_float');
if (($header_float === '') || ($header_float == '-1')) {
	$header_float = $viettitan_options['header_float'];
}
if(is_404()  && $viettitan_options['header_default_404']=='0'){
    $header_float = '0';
}
if ($header_float == '1') {
	$header_class[] = 'header-float';
}

// HEADER CUSTOM SYLE
$header_custom_style = '';

// GET HEADER SCHEME
$header_scheme = rwmb_meta($prefix . 'header_scheme');
if( is_404() && $viettitan_options['header_default_404']=='0'){
    $header_scheme = 'header-light';
}
if ($header_scheme == 'header-overlay') {
	$header_scheme_color = rwmb_meta($prefix . 'header_scheme_color');
	$header_scheme_opacity = rwmb_meta($prefix . 'header_scheme_opacity');
	if (($header_scheme_color !== '') && ($header_scheme_opacity != '')) {
		$header_custom_style = sprintf(' style="background-color:%s"', viettitan_hex2rgba($header_scheme_color, $header_scheme_opacity * 1.0 / 100));
	}
}
if (($header_scheme === '') || ($header_scheme == '-1')) {
	$header_scheme = $viettitan_options['header_scheme'];
	if ($header_scheme == 'header-overlay') {
		$header_scheme_color = $viettitan_options['header_scheme_color'];
		$header_scheme_opacity = $viettitan_options['header_scheme_opacity'];
		if (($header_scheme_color !== '') && ($header_scheme_opacity != '')) {
			$header_custom_style = sprintf(' style="background-color:%s"', viettitan_hex2rgba($header_scheme_color, $header_scheme_opacity * 1.0 / 100));
		}
	}
}
$header_class[] = $header_scheme;

// HEADER LAYOUT LEFT
if (in_array($viettitan_header_layout, array('header-7'))) {
	$header_class[] = 'header-left';
}
else {
	// GET SUB MENU SCHEME
	$menu_sub_scheme = rwmb_meta($prefix . 'menu_sub_scheme');
	if (($menu_sub_scheme === '') || ($menu_sub_scheme == '-1')) {
		$menu_sub_scheme = isset($viettitan_options['menu_sub_scheme']) ? $viettitan_options['menu_sub_scheme'] : 'sub-menu-dark';
	}
	if (!empty($menu_sub_scheme)) {
		$header_class[] = $menu_sub_scheme;
	}
}
if(is_404() && $viettitan_options['header_default_404']=='0'){
    $viettitan_header_layout= $viettitan_options['header_404_layout'];
}
?>
<header id="main-header-wrapper" class="<?php echo join(' ', $header_class); ?>" <?php echo sprintf('%s', $header_custom_style) ?>>
	<?php viettitan_get_template('header/top-bar' ); ?>
	<?php viettitan_get_template('header/' . $viettitan_header_layout ); ?>
</header>