<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 6/1/2015
 * Time: 6:16 PM
 */
/*================================================
LOAD STYLESHEETS
================================================== */
if (!function_exists('viettitan_enqueue_styles')) {
	function viettitan_enqueue_styles() {
		$viettitan_options = &Viettitan_Global::get_options();
		$min_suffix = (isset($viettitan_options['enable_minifile_css']) && $viettitan_options['enable_minifile_css'] == 1) ? '.min' :  '';

		/*font-awesome*/
		$url_font_awesome = VIETTITAN_THEME_URL . 'assets/plugins/fonts-awesome/css/font-awesome.min.css';
		if (isset($viettitan_options['cdn_font_awesome']) && !empty($viettitan_options['cdn_font_awesome'])) {
			$url_font_awesome = $viettitan_options['cdn_font_awesome'];
		}
		wp_enqueue_style('viettitan_framework_font_awesome', $url_font_awesome, array());
		wp_enqueue_style('viettitan_framework_font_awesome_animation', VIETTITAN_THEME_URL . 'assets/plugins/fonts-awesome/css/font-awesome-animation.min.css', array());

		/*bootstrap*/
		$url_bootstrap = VIETTITAN_THEME_URL . 'assets/plugins/bootstrap/css/bootstrap.min.css';
		if (isset($viettitan_options['cdn_bootstrap_css']) && !empty($viettitan_options['cdn_bootstrap_css'])) {
			$url_bootstrap = $viettitan_options['cdn_bootstrap_css'];
		}
		wp_enqueue_style('viettitan_framework_bootstrap', $url_bootstrap, array());

		/*Viettitan FontIcon*/
		wp_enqueue_style('viettitan_framework_viettitan_icon', VIETTITAN_THEME_URL . 'assets/plugins/viettitan-icon/css/styles'.$min_suffix.'.css', array());

		/*owl-carousel*/
		wp_enqueue_style('viettitan_framework_owl_carousel', VIETTITAN_THEME_URL . 'assets/plugins/owl-carousel/assets/owl.carousel'.$min_suffix.'.css', array());

		/*prettyPhoto*/
		wp_enqueue_style('viettitan_framework_prettyPhoto', VIETTITAN_THEME_URL . 'assets/plugins/prettyPhoto/css/prettyPhoto'.$min_suffix.'.css', array());

		/*peffect_scrollbar*/
		wp_enqueue_style('viettitan_framework_peffect_scrollbar', VIETTITAN_THEME_URL . 'assets/plugins/perfect-scrollbar/css/perfect-scrollbar.min.css', array());

		/*slick*/
		wp_enqueue_style('viettitan_framework_slick', VIETTITAN_THEME_URL . 'assets/plugins/slick/css/slick'.$min_suffix.'.css', array());

		/*Font*/
		wp_enqueue_style('viettitan_framework_font_vn', VIETTITAN_THEME_URL . 'assets/fonts/stylesheet'.$min_suffix.'.css', array());





		if (!(defined('VIETTITAN_SCRIPT_DEBUG') && VIETTITAN_SCRIPT_DEBUG)) {
			wp_enqueue_style('viettitan_framework_style', VIETTITAN_THEME_URL . 'style'.$min_suffix.'.css');
		}

		$enable_rtl_mode = '0';
		if (isset($viettitan_options['enable_rtl_mode'])) {
			$enable_rtl_mode =  $viettitan_options['enable_rtl_mode'];
		}

		if (is_rtl() || $enable_rtl_mode == '1' || isset($_GET['RTL'])) {
			wp_enqueue_style('viettitan_framework_rtl', VIETTITAN_THEME_URL . 'assets/css/rtl'.$min_suffix.'.css');
		}
	}
	add_action('wp_enqueue_scripts', 'viettitan_enqueue_styles',11);
}

if (!function_exists('viettitan_custom_font_styles')) {
    function viettitan_custom_font_styles()
    {
        $viettitan_options = &Viettitan_Global::get_options();
        $custom_font_css = '';

        for($i=1;$i<=2;$i++){
            $src = array();
            $custom_font = $viettitan_options['custom_font_'.$i.'_name'];
            if(isset($custom_font) && $custom_font!=''){
                if(isset($viettitan_options['custom_font_'.$i.'_eot'])){
                    $eot =  $viettitan_options['custom_font_'.$i.'_eot']['url'];
                    $src[] = "url('$eot?#iefix') format('embedded-opentype')";;
                }
                if(isset($viettitan_options['custom_font_'.$i.'_ttf'])){
                    $ttf = $viettitan_options['custom_font_'.$i.'_ttf']['url'];
                    $src[] =  "url('$ttf') format('truetype')";
                }
                if(isset($viettitan_options['custom_font_'.$i.'_woff'])){
                    $woff = $viettitan_options['custom_font_'.$i.'_woff']['url'];
                    $src[] = "url('$woff') format('woff')";
                }
                if(isset($viettitan_options['custom_font_'.$i.'_svg'])){
                    $svg = $viettitan_options['custom_font_'.$i.'_svg']['url'];
                    $src[] = "url('$svg?#svgFontName') format('svg')";
                }
                if($src){
                    $custom_font_css .= "@font-face { ";
                    $custom_font_css .= "font-family: '$custom_font'; ";
                    $custom_font_css .= "src: " . implode(", ", $src) . "; }" . "\r\n";
                }

            }
        }
        if($custom_font_css!=''){
            echo sprintf('<style>%s</style>',$custom_font_css);
        }
    }
    add_action('wp_head', 'viettitan_custom_font_styles', 100);
    add_action('admin_head', 'viettitan_custom_font_styles', 100);
}

/*================================================
LOAD SCRIPTS
================================================== */
if (!function_exists('viettitan_enqueue_script')) {
	function viettitan_enqueue_script() {
		$viettitan_options = &Viettitan_Global::get_options();
		$min_suffix = (isset($viettitan_options['enable_minifile_js']) && $viettitan_options['enable_minifile_js'] == 1) ? '.min' :  '';

		/*bootstrap*/
		$url_bootstrap = VIETTITAN_THEME_URL . 'assets/plugins/bootstrap/js/bootstrap.min.js';
		if (isset($viettitan_options['cdn_bootstrap_js']) && !empty($viettitan_options['cdn_bootstrap_js'])) {
			$url_bootstrap = $viettitan_options['cdn_bootstrap_js'];
		}
		wp_enqueue_script('viettitan_framework_bootstrap', $url_bootstrap, array('jquery'), false, true);

		if (is_single()) {
			wp_enqueue_script('comment-reply');
		}

		/*plugins*/
		wp_enqueue_script('viettitan_framework_plugins', VIETTITAN_THEME_URL . 'assets/js/plugin'.$min_suffix.'.js', array(), false, true);

		/*smooth-scroll*/
		if ( isset($viettitan_options['smooth_scroll']) && ($viettitan_options['smooth_scroll'] == 1)) {
			wp_enqueue_script('viettitan_framework_smooth_scroll', VIETTITAN_THEME_URL . 'assets/plugins/smoothscroll/SmoothScroll' . $min_suffix . '.js', array(), false, true);
		}

		wp_enqueue_script( 'viettitan-jplayer-js', VIETTITAN_THEME_URL . 'assets/plugins/jquery.jPlayer/jquery.jplayer.min.js', array(), '', true );
		/*Retina*/
		wp_enqueue_script('viettitan_framework_retina', VIETTITAN_THEME_URL . 'assets/js/jquery.retina.min.js', array(), false, false);

		/*header 9*/
		wp_enqueue_script('viettitan_framework_header9', VIETTITAN_THEME_URL . 'templates/header/header9.js', array(), false, true);

		/*slick*/
		wp_enqueue_script('viettitan_framework_slick', VIETTITAN_THEME_URL . 'assets/plugins/slick/js/slick' . $min_suffix . '.js', array(), false, true);

		wp_enqueue_script('viettitan_framework_app', VIETTITAN_THEME_URL . 'assets/js/main' . $min_suffix . '.js', array(), false, true);

		// Localize the script with new data
		$translation_array = array(
			'product_compare' => esc_html__('Compare','viettitan'),
			'product_wishList' => esc_html__('WishList','viettitan')
		);
		wp_localize_script('viettitan_framework_app', 'viettitan_framework_constant', $translation_array);

		wp_localize_script('viettitan_framework_app', 'viettitan_framework_ajax_url', get_site_url() . '/wp-admin/admin-ajax.php?activate-multi=true');
		wp_localize_script('viettitan_framework_app', 'viettitan_framework_theme_url', VIETTITAN_THEME_URL);
		wp_localize_script('viettitan_framework_app', 'viettitan_framework_site_url', site_url());


	}
	add_action('wp_enqueue_scripts', 'viettitan_enqueue_script');
}

/* CUSTOM CSS OUTPUT
	================================================== */
if(!function_exists('viettitan_enqueue_custom_css')){
    function viettitan_enqueue_custom_css() {
        $viettitan_options = &Viettitan_Global::get_options();
        $custom_css = $viettitan_options['custom_css'];
        if ( $custom_css ) {
	        echo '<style id="viettitan_custom_style" type="text/css"></style>';
            echo sprintf('<style type="text/css">%s %s</style>',"\n",$custom_css);
        }
    }
    add_action( 'wp_head', 'viettitan_enqueue_custom_css' );
}

if (!function_exists('viettitan_woocompare_custom_style')) {
	function viettitan_woocompare_custom_style() {
		$action = isset($_GET['action']) ? $_GET['action'] : '';
		if ($action == 'yith-woocompare-view-table') {
			$custom_css = '
			.woocommerce-compare-page h1 {
			font-size: 24px;
			background-color: #f4f4f4;
			color: #444;
			}';
			echo '<style id="viettitan_woocompare_custom_style" type="text/css"></style>';
			echo sprintf('<style type="text/css">%s %s</style>',"\n",$custom_css);
		}

	}
	add_action('wp_print_footer_scripts','viettitan_woocompare_custom_style');
}


/* CUSTOM JS OUTPUT
	================================================== */
if(!function_exists('viettitan_enqueue_custom_script')){
    function viettitan_enqueue_custom_script() {
        $viettitan_options = &Viettitan_Global::get_options();
        $custom_js = $viettitan_options['custom_js'];
        if ( $custom_js ) {
            echo sprintf('<script type="text/javascript">%s</script>',$custom_js);
        }
    }
    add_action( 'wp_footer', 'viettitan_enqueue_custom_script' );
}
