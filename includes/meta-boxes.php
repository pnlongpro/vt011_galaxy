<?php
/*
*
*	Meta Box Functions
*	------------------------------------------------
*	Viettitan Framework
* 	Copyright Swift Ideas 2015 - http://www.viettitan.net
*
*/
/********************* META BOX REGISTERING ***********************/

/**
 * Register meta boxes
 *
 * @return void
 */
function viettitan_register_meta_boxes()
{
	$meta_boxes = &Viettitan_Global::get_meta_boxes();
	$prefix = 'viettitan_';
	/* PAGE MENU */
	$menu_list = array();
	if ( function_exists( 'viettitan_get_menu_list' ) ) {
		$menu_list = viettitan_get_menu_list();
	}

// POST FORMAT: Image
//--------------------------------------------------
	$meta_boxes[] = array(
		'title' => esc_html__('Post Format: Image', 'viettitan'),
		'id' => $prefix .'meta_box_post_format_image',
		'post_types' => array('post'),
		/*'context' => 'side',
		'priority' => 'low',*/
		'fields' => array(
			array(
				'name' => esc_html__('Image', 'viettitan'),
				'id' => $prefix . 'post_format_image',
				'type' => 'image_advanced',
				'max_file_uploads' => 1,
				'desc' => esc_html__('Select a image for post','viettitan')
			),
		),
	);

// POST FORMAT: Gallery
//--------------------------------------------------
	$meta_boxes[] = array(
		'title' => esc_html__('Post Format: Gallery', 'viettitan'),
		'id' => $prefix . 'meta_box_post_format_gallery',
		'post_types' => array('post'),
		'fields' => array(
			array(
				'name' => esc_html__('Images', 'viettitan'),
				'id' => $prefix . 'post_format_gallery',
				'type' => 'image_advanced',
				'desc' => esc_html__('Select images gallery for post','viettitan')
			),
		),
	);

// POST FORMAT: Video
//--------------------------------------------------
	$meta_boxes[] = array(
		'title' => esc_html__('Post Format: Video', 'viettitan'),
		'id' => $prefix . 'meta_box_post_format_video',
		'post_types' => array('post'),
		'fields' => array(
			array(
				'name' => esc_html__( 'Video URL or Embeded Code', 'viettitan' ),
				'id'   => $prefix . 'post_format_video',
				'type' => 'textarea',
			),
		),
	);

// POST FORMAT: Audio
//--------------------------------------------------
	$meta_boxes[] = array(
		'title' => esc_html__('Post Format: Audio', 'viettitan'),
		'id' => $prefix . 'meta_box_post_format_audio',
		'post_types' => array('post'),
		'fields' => array(
			array(
				'name' => esc_html__( 'Audio URL or Embeded Code', 'viettitan' ),
				'id'   => $prefix . 'post_format_audio',
				'type' => 'textarea',
			),
		),
	);

// POST FORMAT: QUOTE
//--------------------------------------------------
    $meta_boxes[] = array(
        'title' => esc_html__('Post Format: Quote', 'viettitan'),
        'id' => $prefix . 'meta_box_post_format_quote',
        'post_types' => array('post'),
        'fields' => array(
            array(
                'name' => esc_html__( 'Quote', 'viettitan' ),
                'id'   => $prefix . 'post_format_quote',
                'type' => 'textarea',
            ),
            array(
                'name' => esc_html__( 'Author', 'viettitan' ),
                'id'   => $prefix . 'post_format_quote_author',
                'type' => 'text',
            ),
            array(
                'name' => esc_html__( 'Author Url', 'viettitan' ),
                'id'   => $prefix . 'post_format_quote_author_url',
                'type' => 'url',
            ),
        ),
    );
    // POST FORMAT: LINK
	//--------------------------------------------------
    $meta_boxes[] = array(
        'title' => esc_html__('Post Format: Link', 'viettitan'),
        'id' => $prefix . 'meta_box_post_format_link',
        'post_types' => array('post'),
        'fields' => array(
            array(
                'name' => esc_html__( 'Url', 'viettitan' ),
                'id'   => $prefix . 'post_format_link_url',
                'type' => 'url',
            ),
            array(
                'name' => esc_html__( 'Text', 'viettitan' ),
                'id'   => $prefix . 'post_format_link_text',
                'type' => 'text',
            ),
        ),
    );

	// PAGE LAYOUT
	$meta_boxes[] = array(
		'id' => $prefix . 'page_layout_meta_box',
		'title' => esc_html__('Page Layout', 'viettitan'),
		'post_types' => array('post', 'page',  'portfolio','product'),
		'tab' => true,
		'fields' => array(
			array(
				'name'  => esc_html__( 'Layout Style', 'viettitan' ),
				'id'    => $prefix . 'layout_style',
				'type'  => 'button_set',
				'options' => array(
					'-1' => esc_html__('Default','viettitan'),
					'boxed'	  => esc_html__('Boxed','viettitan'),
					'wide'	  => esc_html__('Wide','viettitan'),
					'float'	  => esc_html__('Float','viettitan')
				),
				'std'	=> '-1',
				'multiple' => false,
			),
			array(
				'name'  => esc_html__( 'Page Layout', 'viettitan' ),
				'id'    => $prefix . 'page_layout',
				'type'  => 'button_set',
				'options' => array(
					'-1' => esc_html__('Default','viettitan'),
					'full'	  => esc_html__('Full Width','viettitan'),
					'container'	  => esc_html__('Container','viettitan'),
					'container-fluid'	  => esc_html__('Container Fluid','viettitan'),
				),
				'std'	=> '-1',
				'multiple' => false,
			),
			array(
				'name'  => esc_html__( 'Page Sidebar', 'viettitan' ),
				'id'    => $prefix . 'page_sidebar',
				'type'  => 'image_set',
				'allowClear' => true,
				'options' => array(
					'none'	  => VIETTITAN_THEME_URL.'/assets/images/theme-options/sidebar-none.png',
					'left'	  => VIETTITAN_THEME_URL.'/assets/images/theme-options/sidebar-left.png',
					'right'	  => VIETTITAN_THEME_URL.'/assets/images/theme-options/sidebar-right.png',
					'both'	  => VIETTITAN_THEME_URL.'/assets/images/theme-options/sidebar-both.png'
				),
				'std'	=> '',
				'multiple' => false,

			),
			array (
				'name' 	=> esc_html__('Left Sidebar', 'viettitan'),
				'id' 	=> $prefix . 'page_left_sidebar',
				'type' 	=> 'sidebars',
				'placeholder' => esc_html__('Select Sidebar','viettitan'),
				'std' 	=> '',
				'required-field' => array($prefix . 'page_sidebar','=',array('','left','both')),
			),

			array (
				'name' 	=> esc_html__('Right Sidebar', 'viettitan'),
				'id' 	=> $prefix . 'page_right_sidebar',
				'type' 	=> 'sidebars',
				'placeholder' => esc_html__('Select Sidebar','viettitan'),
				'std' 	=> '',
				'required-field' => array($prefix . 'page_sidebar','=',array('','right','both')),
			),

			array(
				'name'  => esc_html__( 'Sidebar Width', 'viettitan' ),
				'id'    => $prefix . 'sidebar_width',
				'type'  => 'button_set',
				'options' => array(
					'-1'		=> esc_html__('Default','viettitan'),
					'small'		=> esc_html__('Small (1/4)','viettitan'),
					'larger'	=> esc_html__('Large (1/3)','viettitan')
				),
				'std'	=> '-1',
				'multiple' => false,
				'required-field' => array($prefix . 'page_sidebar','<>','none'),
			),

			array (
				'name' 	=> esc_html__('Page Class Extra', 'viettitan'),
				'id' 	=> $prefix . 'page_class_extra',
				'type' 	=> 'text',
				'std' 	=> ''
			),
		)
	);

	// TOP DRAWER
	$meta_boxes[] = array(
		'id' => $prefix . 'top_drawer_meta_box',
		'title' => esc_html__('Top drawer', 'viettitan'),
		'post_types' => array('post', 'page',  'portfolio','product'),
		'tab' => true,
		'fields' => array(
			array (
				'name' 	=> esc_html__('Top Drawer Type', 'viettitan'),
				'id' 	=> $prefix . 'top_drawer_type',
				'type' 	=> 'button_set',
				'std' 	=> '-1',
				'options' => array(
					'-1' => esc_html__('Default','viettitan'),
					'none' => esc_html__('Disable','viettitan-framework'),
					'show' => esc_html__('Always Show','viettitan-framework'),
					'toggle' => esc_html__('Toggle','viettitan-framework')
				),
				'desc' => esc_html__('Top drawer type', 'viettitan'),
			),
			array (
				'name' 	=> esc_html__('Top Drawer Sidebar', 'viettitan'),
				'id' 	=> $prefix . 'top_drawer_sidebar',
				'type' 	=> 'sidebars',
				'placeholder' => esc_html__('Select Sidebar','viettitan'),
				'std' 	=> '',
				'required-field' => array($prefix . 'top_drawer_type','<>','none'),
			),

			array (
				'name' 	=> esc_html__('Top Drawer Wrapper Layout', 'viettitan'),
				'id' 	=> $prefix . 'top_drawer_wrapper_layout',
				'type' 	=> 'button_set',
				'std' 	=> '-1',
				'options' => array(
					'-1' => esc_html__('Default','viettitan'),
					'full' => esc_html__('Full Width','viettitan-framework'),
					'container' => esc_html__('Container','viettitan-framework'),
					'container-fluid' => esc_html__('Container Fluid','viettitan-framework')
				),
				'required-field' => array($prefix . 'top_drawer_type','<>','none'),
			),

			array (
				'name' 	=> esc_html__('Top Drawer hide on mobile', 'viettitan'),
				'id' 	=> $prefix . 'top_drawer_hide_mobile',
				'type' 	=> 'button_set',
				'std' 	=> '-1',
				'options' => array(
					'-1' => esc_html__('Default','viettitan'),
					'1' => esc_html__('Show on mobile','viettitan-framework'),
					'0' => esc_html__('Hide on mobile','viettitan-framework'),
				),
				'required-field' => array($prefix . 'top_drawer_type','<>','none'),
			),

		)
	);

	// TOP BAR
	$meta_boxes[] = array(
		'id' => $prefix . 'top_bar_meta_box',
		'title' => esc_html__('Top Bar', 'viettitan'),
		'post_types' => array('post', 'page',  'portfolio','product'),
		'tab' => true,
		'fields' => array(
			array (
				'name' 	=> esc_html__('Top Bar Desktop', 'viettitan'),
				'id' 	=> $prefix . 'top_bar_section_1',
				'type' 	=> 'section',
				'std' 	=> '',
			),
			array (
				'name' 	=> esc_html__('Show/Hide Top Bar', 'viettitan'),
				'id' 	=> $prefix . 'top_bar',
				'type' 	=> 'button_set',
				'std' 	=> '-1',
				'options' => array(
					'-1' => esc_html__('Default','viettitan'),
					'1' => esc_html__('Show Top Bar','viettitan'),
					'0' => esc_html__('Hide Top Bar','viettitan')
				),
				'desc' => esc_html__('Show Hide Top Bar.', 'viettitan'),
			),

			array (
				'name' 	=> esc_html__('Top Bar Layout', 'viettitan'),
				'id' 	=> $prefix . 'top_bar_layout',
				'type' 	=> 'image_set',
				'allowClear' => true,
				'width' => '80px',
				'std' 	=> '',
				'options' => array(
					'top-bar-1' => VIETTITAN_THEME_URL.'assets/images/theme-options/top-bar-layout-1.jpg',
					'top-bar-2' => VIETTITAN_THEME_URL.'assets/images/theme-options/top-bar-layout-2.jpg',
					'top-bar-3' => VIETTITAN_THEME_URL.'assets/images/theme-options/top-bar-layout-3.jpg',
					'top-bar-4' => VIETTITAN_THEME_URL.'assets/images/theme-options/top-bar-layout-4.jpg'
				),
				'required-field' => array($prefix . 'top_bar','<>','0'),
			),

			array (
				'name' 	=> esc_html__('Top Left Sidebar', 'viettitan'),
				'id' 	=> $prefix . 'top_bar_left_sidebar',
				'type' 	=> 'sidebars',
				'std' 	=> '',
				'placeholder' => esc_html__('Select Sidebar','viettitan'),
				'required-field' => array($prefix . 'top_bar','<>','0'),
			),

			array (
				'name' 	=> esc_html__('Top Right Sidebar', 'viettitan'),
				'id' 	=> $prefix . 'top_bar_right_sidebar',
				'type' 	=> 'sidebars',
				'std' 	=> '',
				'placeholder' => esc_html__('Select Sidebar','viettitan'),
				'required-field' => array($prefix . 'top_bar','<>','0'),
			),
			array (
				'name' 	=> esc_html__('Top Bar Scheme', 'viettitan'),
				'id' 	=> $prefix . 'top_bar_scheme',
				'type' 	=> 'select',
				'std' 	=> '-1',
				'options' => array(
					'-1'                    => esc_html__('Default','viettitan'),
					'top-bar-light'         => esc_html__('Light','viettitan'),
					'top-bar-light-gray'    => esc_html__('Light Gray','viettitan'),
					'top-bar-gray'          => esc_html__('Gray','viettitan'),
					'top-bar-dark-gray'     => esc_html__('Dark Gray','viettitan'),
					'top-bar-dark'          => esc_html__('Dark','viettitan'),
					'top-bar-overlay'       => esc_html__('Overlay','viettitan'),
					'top-bar-transparent'   => esc_html__('Transparent','viettitan'),
				),
				'desc' => esc_html__('Show Hide Top Bar.', 'viettitan'),
				'required-field' => array($prefix . 'top_bar','<>','0'),
			),
			array (
				'name' 	=> esc_html__('Top Bar Border Bottom', 'viettitan'),
				'id' 	=> $prefix . 'top_bar_border_bottom',
				'type' 	=> 'button_set',
				'std' 	=> '-1',
				'options' => array(
					'-1'            => esc_html__('Default','viettitan'),
					'none'          => esc_html__('None','viettitan'),
					'bordered'      => esc_html__('Bordered','viettitan'),
				),
				'desc' => esc_html__('Show Hide Top Bar.', 'viettitan'),
				'required-field' => array($prefix . 'top_bar','<>','0'),
			),

			array (
				'name' 	=> esc_html__('Top Bar Mobile', 'viettitan'),
				'id' 	=> $prefix . 'top_bar_section_2',
				'type' 	=> 'section',
				'std' 	=> '',
			),
			array (
				'name' 	=> esc_html__('Show/Hide Top Bar', 'viettitan'),
				'id' 	=> $prefix . 'top_bar_mobile',
				'type' 	=> 'button_set',
				'std' 	=> '-1',
				'options' => array(
					'-1' => esc_html__('Default','viettitan'),
					'1' => esc_html__('Show Top Bar','viettitan'),
					'0' => esc_html__('Hide Top Bar','viettitan')
				),
				'desc' => esc_html__('Show Hide Top Bar.', 'viettitan'),
			),
			array (
				'name' 	=> esc_html__('Top Bar Layout', 'viettitan'),
				'id' 	=> $prefix . 'top_bar_mobile_layout',
				'type' 	=> 'image_set',
				'allowClear' => true,
				'width' => '80px',
				'std' 	=> '',
				'options' => array(
					'top-bar-1' => VIETTITAN_THEME_URL.'assets/images/theme-options/top-bar-layout-1.jpg',
					'top-bar-2' => VIETTITAN_THEME_URL.'assets/images/theme-options/top-bar-layout-2.jpg',
					'top-bar-3' => VIETTITAN_THEME_URL.'assets/images/theme-options/top-bar-layout-3.jpg',
					'top-bar-4' => VIETTITAN_THEME_URL.'assets/images/theme-options/top-bar-layout-4.jpg'
				),
				'required-field' => array($prefix . 'top_bar_mobile','<>','0'),
			),

			array (
				'name' 	=> esc_html__('Top Left Sidebar', 'viettitan'),
				'id' 	=> $prefix . 'top_bar_mobile_left_sidebar',
				'type' 	=> 'sidebars',
				'std' 	=> '',
				'placeholder' => esc_html__('Select Sidebar','viettitan'),
				'required-field' => array($prefix . 'top_bar_mobile','<>','0'),
			),

			array (
				'name' 	=> esc_html__('Top Right Sidebar', 'viettitan'),
				'id' 	=> $prefix . 'top_bar_mobile_right_sidebar',
				'type' 	=> 'sidebars',
				'std' 	=> '',
				'placeholder' => esc_html__('Select Sidebar','viettitan'),
				'required-field' => array($prefix . 'top_bar_mobile','<>','0'),
			),
			array (
				'name' 	=> esc_html__('Top Bar Scheme', 'viettitan'),
				'id' 	=> $prefix . 'top_bar_mobile_scheme',
				'type' 	=> 'select',
				'std' 	=> '-1',
				'options' => array(
					'-1'                    => esc_html__('Default','viettitan'),
					'top-bar-light'         => esc_html__('Light','viettitan'),
					'top-bar-light-gray'    => esc_html__('Light Gray','viettitan'),
					'top-bar-gray'          => esc_html__('Gray','viettitan'),
					'top-bar-dark-gray'     => esc_html__('Dark Gray','viettitan'),
					'top-bar-dark'          => esc_html__('Dark','viettitan'),
					'top-bar-overlay'       => esc_html__('Overlay','viettitan'),
					'top-bar-transparent'   => esc_html__('Transparent','viettitan'),
				),
				'desc' => esc_html__('Show Hide Top Bar.', 'viettitan'),
				'required-field' => array($prefix . 'top_bar_mobile','<>','0'),
			),
			array (
				'name' 	=> esc_html__('Top Bar Border Bottom', 'viettitan'),
				'id' 	=> $prefix . 'top_bar_mobile_border_bottom',
				'type' 	=> 'button_set',
				'std' 	=> '-1',
				'options' => array(
					'-1'                    => esc_html__('Default','viettitan'),
					'none'                  => esc_html__('None','viettitan'),
					'bordered'              => esc_html__('Bordered','viettitan'),
					'container-bordered'    => esc_html__('Container Bordered','viettitan'),
				),
				'desc' => esc_html__('Show Hide Top Bar.', 'viettitan'),
				'required-field' => array($prefix . 'top_bar_mobile','<>','0'),
			),
		)
	);

	// PAGE HEADER
	//--------------------------------------------------
	$meta_boxes[] = array(
		'id' => $prefix . 'page_header_meta_box',
		'title' => esc_html__('Page Header', 'viettitan'),
		'post_types' => array('post', 'page',  'portfolio','product'),
		'tab' => true,
		'fields' => array(
			array (
				'name' 	=> esc_html__('Header On/Off?', 'viettitan'),
				'id' 	=> $prefix . 'header_show_hide',
				'type' 	=> 'checkbox',
				'desc' => esc_html__("Switch header ON or OFF?", 'viettitan'),
				'std'	=> '1',
			),
			array (
				'name' 	=> esc_html__('Page Header Desktop', 'viettitan'),
				'id' 	=> $prefix . 'page_header_section_1',
				'type' 	=> 'section',
				'std' 	=> '',
			),
			array (
				'name' 	=> esc_html__('Header Layout', 'viettitan'),
				'id' 	=> $prefix . 'header_layout',
				'type'  => 'image_set',
				'allowClear' => true,
				'std'	=> '',
				'options' => array(
					'header-1'	    => VIETTITAN_THEME_URL.'/assets/images/theme-options/header-1.png',
					'header-2'	    => VIETTITAN_THEME_URL.'/assets/images/theme-options/header-2.png',
					'header-3'	    => VIETTITAN_THEME_URL.'/assets/images/theme-options/header-3.png',
					'header-4'	    => VIETTITAN_THEME_URL.'/assets/images/theme-options/header-4.png',
					'header-5'	    => VIETTITAN_THEME_URL.'/assets/images/theme-options/header-5.png',
					'header-6'	    => VIETTITAN_THEME_URL.'/assets/images/theme-options/header-6.png',
					'header-7'	    => VIETTITAN_THEME_URL.'/assets/images/theme-options/header-7.png',
					'header-8'	    => VIETTITAN_THEME_URL.'/assets/images/theme-options/header-8.png',
				),
				'required-field' => array($prefix . 'header_show_hide','=','1'),
			),
			array(
				'id'    => $prefix . 'header_boxed',
				'name'  => esc_html__( 'Header Boxed', 'viettitan' ),
				'type'  => 'button_set',
				'std'	=> '-1',
				'options' => array(
					'-1'    => esc_html__('Default','viettitan'),
					'1'     => esc_html__('On','viettitan'),
					'0'     => esc_html__('Off','viettitan'),
				),
				'required-field' => array($prefix . 'header_show_hide','=','1'),
			),
			array(
				'id'    => $prefix . 'header_container_layout',
				'name'  => esc_html__( 'Header Container Layout', 'viettitan' ),
				'type'  => 'button_set',
				'std'	=> '-1',
				'options' => array(
					'-1'                => esc_html__('Default','viettitan'),
					'container'         => esc_html__('Container','viettitan'),
					'container-full'    => esc_html__('Container Full','viettitan'),
				),
				'required-field' => array($prefix . 'header_show_hide','=','1'),
			),
			array(
				'id'    => $prefix . 'header_float',
				'name'  => esc_html__( 'Header Float', 'viettitan' ),
				'type'  => 'button_set',
				'std'	=> '-1',
				'options' => array(
					'-1'    => esc_html__('Default','viettitan'),
					'1'     => esc_html__('On','viettitan'),
					'0'     => esc_html__('Off','viettitan'),
				),
				'required-field' => array($prefix . 'header_show_hide','=','1'),
			),
			array(
				'id'    => $prefix . 'header_scheme',
				'name'  => esc_html__( 'Header Scheme', 'viettitan' ),
				'type'  => 'select',
				'std'	=> '-1',
				'options' => array(
					'-1'                   => esc_html__('Default','viettitan'),
					'header-light'         => esc_html__('Light','viettitan'),
					'header-light-gray'    => esc_html__('Light Gray','viettitan'),
					'header-gray'          => esc_html__('Gray','viettitan'),
					'header-dark-gray'     => esc_html__('Dark Gray','viettitan'),
					'header-dark'          => esc_html__('Dark','viettitan'),
					'header-transparent'   => esc_html__('Transparent','viettitan'),
					'header-overlay'       => esc_html__('Overlay','viettitan'),
				),
				'required-field' => array($prefix . 'header_show_hide','=','1'),
			),
			array(
				'id' => $prefix . 'header_scheme_color',
				'name' => esc_html__('Header scheme background color', 'viettitan'),
				'desc' => esc_html__("Set header scheme background color overlay.", 'viettitan'),
				'type'  => 'color',
				'std' => '#000',
				'required-field' => array($prefix . 'header_scheme','=','header-overlay'),
			),
			array(
				'id'        => $prefix .'header_scheme_opacity',
				'name'      => esc_html__( 'Header scheme opacity', 'viettitan' ),
				'desc'      => esc_html__( 'Set the opacity level of the overlay', 'viettitan' ),
				'clone'     => false,
				'type'      => 'slider',
				'prefix'    => '',
				'std'       => '20',
				'js_options' => array(
					'min'  => 0,
					'max'  => 100,
					'step' => 1,
				),
				'required-field' => array($prefix . 'header_scheme','=','header-overlay'),
			),
			array(
				'id' => $prefix . 'header_scheme_text_color',
				'name' => esc_html__('Header scheme text color', 'viettitan'),
				'desc' => esc_html__("Set header scheme text color overlay.", 'viettitan'),
				'type'  => 'color',
				'std' => '#fff',
				'required-field' => array($prefix . 'header_scheme','=','header-overlay'),
			),

			array(
				'id'    => $prefix . 'header_nav_scheme',
				'name'  => esc_html__( 'Header Navigation Scheme', 'viettitan' ),
				'type'  => 'select',
				'std'	=> '-1',
				'options' => array(
					'-1'                   => esc_html__('Default','viettitan'),
					'header-light'         => esc_html__('Light','viettitan'),
					'header-light-gray'    => esc_html__('Light Gray','viettitan'),
					'header-gray'          => esc_html__('Gray','viettitan'),
					'header-dark-gray'     => esc_html__('Dark Gray','viettitan'),
					'header-dark'          => esc_html__('Dark','viettitan'),
					'header-transparent'   => esc_html__('Transparent','viettitan'),
					'header-overlay'       => esc_html__('Overlay','viettitan'),
				),
				'required-field' => array($prefix . 'header_show_hide','=','1'),
			),
			array(
				'id' => $prefix . 'header_nav_scheme_color',
				'name' => esc_html__('Header navigation scheme background color', 'viettitan'),
				'desc' => esc_html__("Set header navigation scheme background color overlay.", 'viettitan'),
				'type'  => 'color',
				'std' => '#000',
				'required-field' => array($prefix . 'header_nav_scheme','=','header-overlay'),
			),
			array(
				'id'        => $prefix .'header_nav_scheme_opacity',
				'name'      => esc_html__( 'Header navigation scheme opacity', 'viettitan' ),
				'desc'      => esc_html__( 'Set the opacity level of the overlay', 'viettitan' ),
				'clone'     => false,
				'type'      => 'slider',
				'prefix'    => '',
				'std'       => '20',
				'js_options' => array(
					'min'  => 0,
					'max'  => 100,
					'step' => 1,
				),
				'required-field' => array($prefix . 'header_nav_scheme','=','header-overlay'),
			),
			array(
				'id' => $prefix . 'header_nav_scheme_text_color',
				'name' => esc_html__('Header navigation scheme text color', 'viettitan'),
				'desc' => esc_html__("Set header navigation scheme text color overlay.", 'viettitan'),
				'type'  => 'color',
				'std' => '#fff',
				'required-field' => array($prefix . 'header_nav_scheme','=','header-overlay'),
			),
			array(
				'id'    => $prefix . 'header_nav_border_top',
				'name'  => esc_html__( 'Header navigation border top', 'viettitan' ),
				'type'  => 'button_set',
				'std'	=> '-1',
				'options' => array(
					'-1'            => esc_html__('Default','viettitan'),
					'none'          => esc_html__('None','viettitan'),
					'bottom-bordered'      => esc_html__('Solid','viettitan'),
				),
				'required-field' => array($prefix . 'header_show_hide','=','1'),
			),
			array(
				'id'    => $prefix . 'header_nav_border_bottom',
				'name'  => esc_html__( 'Header navigation border bottom', 'viettitan' ),
				'type'  => 'button_set',
				'std'	=> '-1',
				'options' => array(
					'-1'                => esc_html__('Default','viettitan'),
					'none'              => esc_html__('None','viettitan'),
					'bottom-border-solid'      => esc_html__('Solid','viettitan'),
					'bottom-border-gradient'   => esc_html__('Gradient','viettitan'),
					'bottom-border-gradient w2p3' => esc_html__('Gradient 2','viettitan'),
				),
				'required-field' => array($prefix . 'header_show_hide','=','1'),
			),
			array(
				'id'    => $prefix . 'header_sticky',
				'name'  => esc_html__( 'Show/Hide Header Sticky', 'viettitan' ),
				'type'  => 'button_set',
				'std'	=> '-1',
				'options' => array(
					'-1'    => esc_html__('Default','viettitan'),
					'1'     => esc_html__('On','viettitan'),
					'0'     => esc_html__('Off','viettitan')
				),
				'required-field' => array($prefix . 'header_show_hide','=','1'),
			),
			array(
				'id'    => $prefix . 'header_sticky_scheme',
				'name'  => esc_html__( 'Header sticky scheme', 'viettitan' ),
				'type'  => 'button_set',
				'std'	=> '-1',
				'options' => array(
					'-1'    => esc_html__('Default','viettitan'),
					'sticky-inherit'   => esc_html__('Inherit','viettitan'),
					'sticky-gray'      => esc_html__('Gray','viettitan'),
					'sticky-light'     => esc_html__('Light','viettitan'),
					'sticky-dark'      => esc_html__('Dark','viettitan')
				),
				'required-field' => array($prefix . 'header_show_hide','=','1'),
			),

			array (
				'name' 	=> esc_html__('Page Header Mobile', 'viettitan'),
				'id' 	=> $prefix . 'page_header_section_2',
				'type' 	=> 'section',
				'std' 	=> '',
			),
			array (
				'name' 	=> esc_html__('Header Mobile Layout', 'viettitan'),
				'id' 	=> $prefix . 'mobile_header_layout',
				'type'  => 'image_set',
				'allowClear' => true,
				'std'	=> '',
				'options' => array(
					'header-mobile-1'	    => VIETTITAN_THEME_URL.'assets/images/theme-options/header-mobile-layout-1.png',
					'header-mobile-2'	    => VIETTITAN_THEME_URL.'assets/images/theme-options/header-mobile-layout-2.png',
					'header-mobile-3'	    => VIETTITAN_THEME_URL.'assets/images/theme-options/header-mobile-layout-3.png',
					'header-mobile-4'	    => VIETTITAN_THEME_URL.'assets/images/theme-options/header-mobile-layout-4.png',
				)
			),
			array(
				'id'    => $prefix . 'mobile_header_menu_drop',
				'name'  => esc_html__( 'Menu Drop Type', 'viettitan' ),
				'type'  => 'button_set',
				'std'	=> '-1',
				'options' => array(
					'-1'        => esc_html__('Default','viettitan'),
					'dropdown'  => esc_html__('Dropdown Menu','viettitan'),
					'fly'       => esc_html__('Fly Menu','viettitan'),
				)
			),
			array(
				'id'    => $prefix . 'mobile_header_scheme',
				'name'  => esc_html__( 'Header Scheme', 'viettitan' ),
				'type'  => 'select',
				'std'	=> '-1',
				'options' => array(
					'-1'                   => esc_html__('Default','viettitan'),
					'header-light'         => esc_html__('Light','viettitan'),
					'header-light-gray'    => esc_html__('Light Gray','viettitan'),
					'header-gray'          => esc_html__('Gray','viettitan'),
					'header-dark-gray'     => esc_html__('Dark Gray','viettitan'),
					'header-dark'          => esc_html__('Dark','viettitan'),
					'header-overlay'       => esc_html__('Overlay','viettitan'),
					'header-transparent'   => esc_html__('Transparent','viettitan'),
				)
			),
			array(
				'id'    => $prefix . 'mobile_header_border_bottom',
				'name'  => esc_html__( 'Mobile header border bottom', 'viettitan' ),
				'type'  => 'button_set',
				'std'	=> '-1',
				'options' => array(
					'-1'                    => esc_html__('Default','viettitan'),
					'none'                  => esc_html__('None','viettitan'),
					'bordered'              => esc_html__('Bordered','viettitan'),
					'container-bordered'    => esc_html__('Container Bordered','viettitan'),
				)
			),
			array(
				'id'    => $prefix . 'mobile_header_float',
				'name'  => esc_html__( 'Mobile header float', 'viettitan' ),
				'type'  => 'button_set',
				'std'	=> '-1',
				'options' => array(
					'-1'    => esc_html__('Default','viettitan'),
					'1'     => esc_html__('On','viettitan'),
					'0'     => esc_html__('Off','viettitan')
				)
			),
			array (
				'id' 	=> $prefix . 'mobile_header_stick',
				'name' 	=> esc_html__('Header mobile sticky', 'viettitan'),
				'type' 	=> 'button_set',
				'std' 	=> '-1',
				'options' => array(
					'-1' => esc_html__('Default','viettitan'),
					'1' => esc_html__('Enable','viettitan'),
					'0' => esc_html__('Disable','viettitan'),
				),
			),
			array (
				'name' 	=> esc_html__('Mobile Header Search Box', 'viettitan'),
				'id' 	=> $prefix . 'mobile_header_search_box',
				'type' 	=> 'button_set',
				'std' 	=> '-1',
				'options' => array(
					'-1' => esc_html__('Default','viettitan'),
					'1' => esc_html__('Show','viettitan'),
					'0' => esc_html__('Hide','viettitan')
				),
			),

			array (
				'name' 	=> esc_html__('Mobile Header Shopping Cart', 'viettitan'),
				'id' 	=> $prefix . 'mobile_header_shopping_cart',
				'type' 	=> 'button_set',
				'std' 	=> '-1',
				'options' => array(
					'-1' => esc_html__('Default','viettitan'),
					'1' => esc_html__('Show','viettitan'),
					'0' => esc_html__('Hide','viettitan')
				),
			),
		)
	);

	// HEADER CUSTOMIZE
	$meta_boxes[] = array(
		'id' => $prefix . 'page_header_customize_meta_box',
		'title' => esc_html__('Page Header Customize', 'viettitan'),
		'post_types' => array('post', 'page',  'portfolio','product'),
		'tab' => true,
		'fields' => array(
			array (
				'name' 	=> esc_html__('Header Customize Navigation', 'viettitan'),
				'id' 	=> $prefix . 'page_header_customize_section_1',
				'type' 	=> 'section',
				'std' 	=> '',
			),
			array(
				'name'  => esc_html__( 'Set header customize navigation?', 'viettitan' ),
				'id'    => $prefix . 'enable_header_customize_nav',
				'type'  => 'checkbox',
				'std'	=> 0,
			),
			array (
				'name' 	=> esc_html__('Header Customize Navigation', 'viettitan'),
				'id' 	=> $prefix . 'header_customize_nav',
				'type' 	=> 'sorter',
				'std' 	=> '',
				'desc'  => esc_html__('Select element for header customize navigation. Drag to change element order', 'viettitan'),
				'options' => array(
					'shopping-cart'     => esc_html__('Shopping Cart','viettitan'),
					'search-button'     => esc_html__('Search Button','viettitan'),
					'social-profile'    => esc_html__('Social Profile','viettitan'),
					'canvas-menu'       => esc_html__('Canvas Menu','viettitan'),
					'custom-text'       => esc_html__('Custom Text','viettitan'),
				),
				'required-field' => array($prefix . 'enable_header_customize_nav','=','1'),
			),
			array(
				'name' => esc_html__('Custom social profiles', 'viettitan'),
				'id' => $prefix . 'header_customize_nav_social_profile',
				'type'  => 'select_advanced',
				'placeholder' => esc_html__('Select social profiles','viettitan'),
				'std'	=> '',
				'multiple' => true,
				'options' => array(
					'twitter'  => esc_html__( 'Twitter', 'viettitan' ),
					'facebook'  => esc_html__( 'Facebook', 'viettitan' ),
					'dribbble'  => esc_html__( 'Dribbble', 'viettitan' ),
					'vimeo'  => esc_html__( 'Vimeo', 'viettitan' ),
					'tumblr'  => esc_html__( 'Tumblr', 'viettitan' ),
					'skype'  => esc_html__( 'Skype', 'viettitan' ),
					'linkedin'  => esc_html__( 'LinkedIn', 'viettitan' ),
					'googleplus'  => esc_html__( 'Google+', 'viettitan' ),
					'flickr'  => esc_html__( 'Flickr', 'viettitan' ),
					'youtube'  => esc_html__( 'YouTube', 'viettitan' ),
					'pinterest' => esc_html__( 'Pinterest', 'viettitan' ),
					'foursquare'  => esc_html__( 'Foursquare', 'viettitan' ),
					'instagram' => esc_html__( 'Instagram', 'viettitan' ),
					'github'  => esc_html__( 'GitHub', 'viettitan' ),
					'xing' => esc_html__( 'Xing', 'viettitan' ),
					'behance'  => esc_html__( 'Behance', 'viettitan' ),
					'deviantart'  => esc_html__( 'Deviantart', 'viettitan' ),
					'soundcloud'  => esc_html__( 'SoundCloud', 'viettitan' ),
					'yelp'  => esc_html__( 'Yelp', 'viettitan' ),
					'rss'  => esc_html__( 'RSS Feed', 'viettitan' ),
					'email'  => esc_html__( 'Email address', 'viettitan' ),
				),
				'required-field' => array($prefix . 'enable_header_customize_nav','=','1'),
			),
			array(
				'name'  => esc_html__( 'Custom text content', 'viettitan' ),
				'id'    => $prefix . 'header_customize_nav_text',
				'type'  => 'textarea',
				'std'	=> '',
				'required-field' => array($prefix . 'enable_header_customize_nav','=','1'),
			),

			array (
				'name' 	=> esc_html__('Header Customize Left', 'viettitan'),
				'id' 	=> $prefix . 'page_header_customize_section_2',
				'type' 	=> 'section',
				'std' 	=> '',
			),
			array(
				'name'  => esc_html__( 'Set header customize left?', 'viettitan' ),
				'id'    => $prefix . 'enable_header_customize_left',
				'type'  => 'checkbox',
				'std'	=> 0,
			),
			array (
				'name' 	=> esc_html__('Header Customize Left', 'viettitan'),
				'id' 	=> $prefix . 'header_customize_left',
				'type' 	=> 'sorter',
				'std' 	=> '',
				'desc'  => esc_html__('Select element for header customize left. Drag to change element order', 'viettitan'),
				'options' => array(
					'shopping-cart'     => esc_html__('Shopping Cart','viettitan'),
					'search-button'     => esc_html__('Search Button','viettitan'),
					'social-profile'    => esc_html__('Social Profile','viettitan'),
					'canvas-menu'       => esc_html__('Canvas Menu','viettitan'),
					'custom-text'       => esc_html__('Custom Text','viettitan'),
				),
				'required-field' => array($prefix . 'enable_header_customize_left','=','1'),
			),
			array(
				'name' => esc_html__('Custom social profiles left', 'viettitan'),
				'id' => $prefix . 'header_customize_left_social_profile',
				'type'  => 'select_advanced',
				'placeholder' => esc_html__('Select social profiles','viettitan'),
				'std'	=> '',
				'multiple' => true,
				'options' => array(
					'twitter'  => esc_html__( 'Twitter', 'viettitan' ),
					'facebook'  => esc_html__( 'Facebook', 'viettitan' ),
					'dribbble'  => esc_html__( 'Dribbble', 'viettitan' ),
					'vimeo'  => esc_html__( 'Vimeo', 'viettitan' ),
					'tumblr'  => esc_html__( 'Tumblr', 'viettitan' ),
					'skype'  => esc_html__( 'Skype', 'viettitan' ),
					'linkedin'  => esc_html__( 'LinkedIn', 'viettitan' ),
					'googleplus'  => esc_html__( 'Google+', 'viettitan' ),
					'flickr'  => esc_html__( 'Flickr', 'viettitan' ),
					'youtube'  => esc_html__( 'YouTube', 'viettitan' ),
					'pinterest' => esc_html__( 'Pinterest', 'viettitan' ),
					'foursquare'  => esc_html__( 'Foursquare', 'viettitan' ),
					'instagram' => esc_html__( 'Instagram', 'viettitan' ),
					'github'  => esc_html__( 'GitHub', 'viettitan' ),
					'xing' => esc_html__( 'Xing', 'viettitan' ),
					'behance'  => esc_html__( 'Behance', 'viettitan' ),
					'deviantart'  => esc_html__( 'Deviantart', 'viettitan' ),
					'soundcloud'  => esc_html__( 'SoundCloud', 'viettitan' ),
					'yelp'  => esc_html__( 'Yelp', 'viettitan' ),
					'rss'  => esc_html__( 'RSS Feed', 'viettitan' ),
					'email'  => esc_html__( 'Email address', 'viettitan' ),
				),
				'required-field' => array($prefix . 'enable_header_customize_left','=','1'),
			),
			array(
				'name'  => esc_html__( 'Custom text content left', 'viettitan' ),
				'id'    => $prefix . 'header_customize_left_text',
				'type'  => 'textarea',
				'std'	=> '',
				'required-field' => array($prefix . 'enable_header_customize_left','=','1'),
			),

			array (
				'name' 	=> esc_html__('Header Customize Right', 'viettitan'),
				'id' 	=> $prefix . 'page_header_customize_section_3',
				'type' 	=> 'section',
				'std' 	=> '',
			),
			array(
				'name'  => esc_html__( 'Set header customize right?', 'viettitan' ),
				'id'    => $prefix . 'enable_header_customize_right',
				'type'  => 'checkbox',
				'std'	=> 0,
			),
			array (
				'name' 	=> esc_html__('Header Customize Right', 'viettitan'),
				'id' 	=> $prefix . 'header_customize_right',
				'type' 	=> 'sorter',
				'std' 	=> '',
				'desc'  => esc_html__('Select element for header customize right. Drag to change element order', 'viettitan'),
				'options' => array(
					'shopping-cart'     => esc_html__('Shopping Cart','viettitan'),
					'search-button'     => esc_html__('Search Button','viettitan'),
					'social-profile'    => esc_html__('Social Profile','viettitan'),
					'canvas-menu'       => esc_html__('Canvas Menu','viettitan'),
					'custom-text'       => esc_html__('Custom Text','viettitan'),
				),
				'required-field' => array($prefix . 'enable_header_customize_right','=','1'),
			),
			array(
				'name' => esc_html__('Custom social profiles right', 'viettitan'),
				'id' => $prefix . 'header_customize_right_social_profile',
				'type'  => 'select_advanced',
				'placeholder' => esc_html__('Select social profiles','viettitan'),
				'std'	=> '',
				'multiple' => true,
				'options' => array(
					'twitter'  => esc_html__( 'Twitter', 'viettitan' ),
					'facebook'  => esc_html__( 'Facebook', 'viettitan' ),
					'dribbble'  => esc_html__( 'Dribbble', 'viettitan' ),
					'vimeo'  => esc_html__( 'Vimeo', 'viettitan' ),
					'tumblr'  => esc_html__( 'Tumblr', 'viettitan' ),
					'skype'  => esc_html__( 'Skype', 'viettitan' ),
					'linkedin'  => esc_html__( 'LinkedIn', 'viettitan' ),
					'googleplus'  => esc_html__( 'Google+', 'viettitan' ),
					'flickr'  => esc_html__( 'Flickr', 'viettitan' ),
					'youtube'  => esc_html__( 'YouTube', 'viettitan' ),
					'pinterest' => esc_html__( 'Pinterest', 'viettitan' ),
					'foursquare'  => esc_html__( 'Foursquare', 'viettitan' ),
					'instagram' => esc_html__( 'Instagram', 'viettitan' ),
					'github'  => esc_html__( 'GitHub', 'viettitan' ),
					'xing' => esc_html__( 'Xing', 'viettitan' ),
					'behance'  => esc_html__( 'Behance', 'viettitan' ),
					'deviantart'  => esc_html__( 'Deviantart', 'viettitan' ),
					'soundcloud'  => esc_html__( 'SoundCloud', 'viettitan' ),
					'yelp'  => esc_html__( 'Yelp', 'viettitan' ),
					'rss'  => esc_html__( 'RSS Feed', 'viettitan' ),
					'email'  => esc_html__( 'Email address', 'viettitan' ),
				),
				'required-field' => array($prefix . 'enable_header_customize_right','=','1'),
			),
			array(
				'name'  => esc_html__( 'Custom text content right', 'viettitan' ),
				'id'    => $prefix . 'header_customize_right_text',
				'type'  => 'textarea',
				'std'	=> '',
				'required-field' => array($prefix . 'enable_header_customize_right','=','1'),
			),
		)
	);

	// LOGO
	$meta_boxes[] = array(
		'id' => $prefix . 'page_logo_meta_box',
		'title' => esc_html__('Logo', 'viettitan'),
		'post_types' => array('post', 'page',  'portfolio','product'),
		'tab' => true,
		'fields' => array(
			array (
				'name' 	=> esc_html__('LOGO Desktop', 'viettitan'),
				'id' 	=> $prefix . 'page_logo_section_1',
				'type' 	=> 'section',
				'std' 	=> '',
			),
			array(
				'id'    => $prefix.  'logo',
				'name'  => esc_html__('Custom Logo', 'viettitan'),
				'desc'  => esc_html__('Upload custom logo in header.', 'viettitan'),
				'type'  => 'image_advanced',
				'max_file_uploads' => 1,
			),
			array(
				'id'    => $prefix.  'logo_retina',
				'name'  => esc_html__('Custom Logo Retina', 'viettitan'),
				'desc'  => esc_html__('Upload custom logo retina in header.', 'viettitan'),
				'type'  => 'image_advanced',
				'max_file_uploads' => 1,
			),
			array(
				'id'    => $prefix.  'logo_height',
				'name'  => esc_html__('Logo height', 'viettitan'),
				'desc'  => esc_html__('Logo height (px). Do not include units (empty to set default)', 'viettitan'),
				'type'  => 'text',
				'sdt'   => '',
			),
			array(
				'id'    => $prefix.  'logo_max_height',
				'name'  => esc_html__('Logo max height', 'viettitan'),
				'desc'  => esc_html__('Logo max height (px). Do not include units (empty to set default)', 'viettitan'),
				'type'  => 'text',
				'sdt'   => '',
			),
			array(
				'id'    => $prefix.  'logo_padding_top',
				'name'  => esc_html__('Logo padding top', 'viettitan'),
				'desc'  => esc_html__('Logo padding top (px). Do not include units (empty to set default)', 'viettitan'),
				'type'  => 'text',
				'sdt'   => '',
			),
			array(
				'id'    => $prefix.  'logo_padding_bottom',
				'name'  => esc_html__('Logo padding bottom', 'viettitan'),
				'desc'  => esc_html__('Logo padding bottom (px). Do not include units (empty to set default)', 'viettitan'),
				'type'  => 'text',
				'sdt'   => '',
			),

			array(
				'id'    => $prefix . 'sticky_logo',
				'name'  => esc_html__('Sticky Logo', 'viettitan'),
				'desc'  => esc_html__('Upload sticky logo in header (empty to default)', 'viettitan'),
				'type'  => 'image_advanced',
				'max_file_uploads' => 1,
			),
			array(
				'id'    => $prefix . 'sticky_logo_retina',
				'name'  => esc_html__('Sticky Logo Retina', 'viettitan'),
				'desc'  => esc_html__('Upload sticky logo retina in header (empty to default)', 'viettitan'),
				'type'  => 'image_advanced',
				'max_file_uploads' => 1,
			),

			array (
				'name' 	=> esc_html__('LOGO Mobile', 'viettitan'),
				'id' 	=> $prefix . 'page_logo_section_2',
				'type' 	=> 'section',
				'std' 	=> '',
			),
			array(
				'id'    => $prefix.  'mobile_logo',
				'name'  => esc_html__('Mobile Logo', 'viettitan'),
				'desc'  => esc_html__('Upload mobile logo in header.', 'viettitan'),
				'type'  => 'image_advanced',
				'max_file_uploads' => 1,
			),
			array(
				'id'    => $prefix.  'mobile_logo_retina',
				'name'  => esc_html__('Mobile Logo Retina', 'viettitan'),
				'desc'  => esc_html__('Upload mobile logo retina in header.', 'viettitan'),
				'type'  => 'image_advanced',
				'max_file_uploads' => 1,
			),
			array(
				'id'    => $prefix.  'mobile_logo_height',
				'name'  => esc_html__('Mobile Logo Height', 'viettitan'),
				'desc'  => esc_html__('Logo height (px). Do not include units (empty to set default)', 'viettitan'),
				'type'  => 'text',
				'sdt'   => '',
			),
			array(
				'id'    => $prefix.  'mobile_logo_max_height',
				'name'  => esc_html__('Mobile Logo Max Height', 'viettitan'),
				'desc'  => esc_html__('Logo max height (px). Do not include units (empty to set default)', 'viettitan'),
				'type'  => 'text',
				'sdt'   => '',
			),
			array(
				'id'    => $prefix.  'mobile_logo_padding',
				'name'  => esc_html__('Mobile Logo Padding', 'viettitan'),
				'desc'  => esc_html__('Logo padding top/bottom (px). Do not include units (empty to set default)', 'viettitan'),
				'type'  => 'text',
				'sdt'   => '',
			),
		)
	);

	// MENU
	$meta_boxes[] = array(
		'id' => $prefix . 'page_menu_meta_box',
		'title' => esc_html__('Menu', 'viettitan'),
		'post_types' => array('post', 'page',  'portfolio','product'),
		'tab' => true,
		'fields' => array(
			array(
				'name'  => esc_html__( 'Page menu', 'viettitan' ),
				'id'    => $prefix . 'page_menu',
				'type'  => 'select_advanced',
				'options' => $menu_list,
				'placeholder' => esc_html__('Select Menu','viettitan'),
				'std'	=> '',
				'multiple' => false,
				'desc' => esc_html__('Optionally you can choose to override the menu that is used on the page', 'viettitan'),
			),

			array(
				'name'  => esc_html__( 'Page Menu Left', 'viettitan' ),
				'id'    => $prefix . 'page_menu_left',
				'type'  => 'select_advanced',
				'options' => $menu_list,
				'placeholder' => esc_html__('Select Menu','viettitan'),
				'std'	=> '',
				'multiple' => false,
				'desc' => esc_html__('Optionally you can choose to override the menu left that is used on the page (apply "header-2" and "header-3")', 'viettitan'),
			),
			array(
				'name'  => esc_html__( 'Page Menu Right', 'viettitan' ),
				'id'    => $prefix . 'page_menu_right',
				'type'  => 'select_advanced',
				'options' => $menu_list,
				'placeholder' => esc_html__('Select Menu','viettitan'),
				'std'	=> '',
				'multiple' => false,
				'desc' => esc_html__('Optionally you can choose to override the menu right that is used on the page (apply "header-2" and "header-3")', 'viettitan'),
			),

			array(
				'name'  => esc_html__( 'Page menu mobile', 'viettitan' ),
				'id'    => $prefix . 'page_menu_mobile',
				'type'  => 'select_advanced',
				'options' => $menu_list,
				'placeholder' => esc_html__('Select Menu','viettitan'),
				'std'	=> '',
				'multiple' => false,
				'desc' => esc_html__('Optionally you can choose to override the menu mobile that is used on the page', 'viettitan'),
			),

			array(
				'name'  => esc_html__( 'Is One Page', 'viettitan' ),
				'id'    => $prefix . 'is_one_page',
				'type' 	=> 'checkbox',
				'std' 	=> '0',
				'desc' => esc_html__('Set page style is One Page', 'viettitan'),
			),

			array(
				'name' => esc_html__('Sub menu scheme', 'viettitan'),
				'id' => $prefix . 'menu_sub_scheme',
				'desc' => esc_html__("Choose submenu scheme", 'viettitan'),
				'type'  => 'button_set',
				'options'	=> array(
					'-1' => esc_html__('Default','viettitan'),
					'sub-menu-dark' => esc_html__('Dark','viettitan'),
					'sub-menu-light' => esc_html__('Light','viettitan'),
				),
				'std' => '-1',
			),
		)
	);


	// PAGE TITLE
	//--------------------------------------------------
	$meta_boxes[] = array(
		'id' => $prefix . 'page_title_meta_box',
		'title' => esc_html__('Page Title', 'viettitan'),
		'post_types' => array('post', 'page',  'portfolio','product'),
		'tab' => true,
		'fields' => array(
			array(
				'name'  => esc_html__('Show/Hide Page Title?', 'viettitan' ),
				'id'    => $prefix . 'show_page_title',
				'type'  => 'button_set',
				'std'	=> '-1',
				'options' => array(
					'-1'	=> esc_html__('Default','viettitan'),
					'1'	=> esc_html__('On','viettitan'),
					'0'	=> esc_html__('Off','viettitan'),
				)

			),
			// PAGE TITLE LINE 1
			array(
				'name' => esc_html__('Custom Page Title', 'viettitan'),
				'id' => $prefix . 'page_title_custom',
				'desc' => esc_html__("Enter a custom page title if you'd like.", 'viettitan'),
				'type'  => 'text',
				'std' => '',
                'required-field' => array($prefix . 'show_page_title','<>','0'),
			),

			array(
				'name'  => esc_html__( 'Custom Page Subtitle?', 'viettitan' ),
				'id'    => $prefix . 'enable_custom_page_subtitle',
				'type'  => 'checkbox',
				'std'	=> 0,
				'required-field' => array($prefix . 'show_page_title','<>','0'),
			),

			// PAGE TITLE LINE 2
			array(
				'name' => esc_html__('Custom Page Subtitle', 'viettitan'),
				'id' => $prefix . 'page_subtitle_custom',
				'desc' => esc_html__("Enter a custom page title if you'd like.", 'viettitan'),
				'type'  => 'text',
				'std' => '',
                'required-field' => array($prefix . 'show_page_title','<>','0'),
			),

			array(
				'name' => esc_html__('Text Align', 'viettitan'),
				'id' => $prefix . 'page_title_text_align',
				'desc' => esc_html__("Set Page Title Text Align", 'viettitan'),
				'type'  => 'button_set',
				'options'	=> array(
					'-1' => esc_html__('Default','viettitan'),
					'left' => esc_html__('Left','viettitan'),
					'center' => esc_html__('Center','viettitan'),
					'right' => esc_html__('Right','viettitan'),
				),
				'std' => '-1',
				'required-field' => array($prefix . 'show_page_title','<>','0'),
			),

			// PAGE TITLE Height
			array(
				'name' => esc_html__('Padding Top', 'viettitan'),
				'id' => $prefix . 'page_title_padding_top',
				'desc' => esc_html__("Enter a page title padding top value (not include unit).", 'viettitan'),
				'type'  => 'number',
				'std' => '',
				'required-field' => array($prefix . 'show_page_title','<>','0'),
			),

			array(
				'name' => esc_html__('Padding Bottom', 'viettitan'),
				'id' => $prefix . 'page_title_padding_bottom',
				'desc' => esc_html__("Enter a page title padding bottom value (not include unit).", 'viettitan'),
				'type'  => 'number',
				'std' => '',
				'required-field' => array($prefix . 'show_page_title','<>','0'),
			),

			array(
				'name' => esc_html__('Border Bottom', 'viettitan'),
				'id' => $prefix . 'page_title_border_bottom',
				'desc' => esc_html__("Enabling this option will display bottom border on Title Area", 'viettitan'),
				'type'  => 'button_set',
				'options'	=> array(
					'-1' => esc_html__('Default','viettitan'),
					'1' => esc_html__('Enable','viettitan'),
					'0' => esc_html__('Disable','viettitan'),
				),
				'std' => '-1',
				'required-field' => array($prefix . 'show_page_title','<>','0'),
			),

			array(
				'name' => esc_html__('Text Size', 'viettitan'),
				'id' => $prefix . 'page_title_text_size',
				'desc' => esc_html__("Choose a default Title size", 'viettitan'),
				'type'  => 'button_set',
				'options'	=> array(
					'-1' => esc_html__('Default','viettitan'),
					'md' => esc_html__('Medium','viettitan'),
					'lg' => esc_html__('Large','viettitan')
				),
				'std' => '-1',
				'required-field' => array($prefix . 'show_page_title','<>','0'),
			),




			// PAGE TITLE TEXT COLOR
			array(
				'name' => esc_html__('Text Color', 'viettitan'),
				'id' => $prefix . 'page_title_text_color',
				'desc' => esc_html__("Optionally set a text color for the page title.", 'viettitan'),
				'type'  => 'color',
				'std' => '',
                'required-field' => array($prefix . 'show_page_title','<>','0'),
			),

			// PAGE TITLE BACKGROUND COLOR

			array(
				'name'  => esc_html__( 'Custom Background Color?', 'viettitan' ),
				'id'    => $prefix . 'enable_custom_background_color',
				'type'  => 'checkbox',
				'std'	=> 0,
				'required-field' => array($prefix . 'show_page_title','<>','0'),
			),


			array(
				'name' => esc_html__('Background Color', 'viettitan'),
				'id' => $prefix . 'page_title_bg_color',
				'desc' => esc_html__("Optionally set a background color for the page title.", 'viettitan'),
				'type'  => 'color',
				'std' => '',
				'required-field' => array($prefix . 'show_page_title','<>','0'),
			),

			// Overlay Opacity Value
			array(
				'name'       => esc_html__( 'Background Color Opacity', 'viettitan' ),
				'id'         => $prefix .'page_title_bg_color_opacity',
				'desc'       => esc_html__( 'Set the opacity level of the overlay. This will lighten or darken the image depening on the color selected.', 'viettitan' ),
				'clone'      => false,
				'type'       => 'slider',
				'prefix'     => '',
				'js_options' => array(
					'min'  => 0,
					'max'  => 100,
					'step' => 1,
				),
				'required-field' => array($prefix . 'show_page_title','<>','0'),
			),



			array(
				'name'  => esc_html__( 'Custom Background Image?', 'viettitan' ),
				'id'    => $prefix . 'enable_custom_page_title_bg_image',
				'type'  => 'checkbox',
				'std'	=> 0,
				'required-field' => array($prefix . 'show_page_title','<>','0'),
			),

			// BACKGROUND IMAGE
			array(
				'id'    => $prefix.  'page_title_bg_image',
				'name'  => esc_html__('Background Image', 'viettitan'),
				'desc'  => esc_html__('Background Image for page title.', 'viettitan'),
				'type'  => 'image_advanced',
				'max_file_uploads' => 1,
				'required-field' => array($prefix . 'show_page_title','<>','0'),
			),

			// PAGE TITLE OVERLAY COLOR






			array(
				'name' => esc_html__('Page Title Parallax', 'viettitan'),
				'id' => $prefix . 'page_title_parallax',
				'desc' => esc_html__("Enable Page Title Parallax", 'viettitan'),
				'type'  => 'button_set',
				'options'	=> array(
					'-1' => esc_html__('Default','viettitan'),
					'1' => esc_html__('Enable','viettitan'),
					'0' => esc_html__('Disable','viettitan'),
				),
				'std' => '-1',
				'required-field' => array($prefix . 'show_page_title','<>','0'),
			),

			array(
				'name' => esc_html__('Parallax Position', 'viettitan'),
				'id' => $prefix . 'page_title_parallax_position',
				'desc' => '',
				'type'  => 'button_set',
				'options'	=> array(
					'-1' => esc_html__('Default','viettitan'),
					'top' => esc_html__('Top','viettitan'),
					'center' => esc_html__('Center','viettitan'),
					'bottom' => esc_html__('Bottom','viettitan'),
				),
				'std' => '-1',
				'required-field' => array($prefix . 'show_page_title','<>','0'),
			),



			// Breadcrumbs in Page Title
			array(
				'name' => esc_html__('Breadcrumbs', 'viettitan'),
				'id' => $prefix . 'breadcrumbs',
				'desc' => esc_html__("Show/Hide Breadcrumbs", 'viettitan'),
				'type'  => 'button_set',
				'options'	=> array(
					'-1' => esc_html__('Default','viettitan'),
					'1' => esc_html__('Enable','viettitan'),
					'0' => esc_html__('Disable','viettitan'),
				),
				'std' => '-1',
				'required-field' => array($prefix . 'show_page_title','<>','0'),
			),

			array(
				'name' => esc_html__('Breadcrumbs Styles', 'viettitan'),
				'id' => $prefix . 'breadcrumbs_style',
				'desc' => esc_html__("Set breadcrumbs styles", 'viettitan'),
				'type'  => 'button_set',
				'options'	=> array(
					'-1' => esc_html__('Default','viettitan'),
					'float' => esc_html__('Float','viettitan'),
					'normal' => esc_html__('Normal','viettitan'),
				),
				'std' => '-1',
				'required-field' => array($prefix . 'show_page_title','<>','0'),
			),

			array(
				'name' => esc_html__('Breadcrumbs Align', 'viettitan'),
				'id' => $prefix . 'breadcrumbs_align',
				'desc' => esc_html__("Set breadcrumbs align (apply with breadcrumbs style float)", 'viettitan'),
				'type'  => 'button_set',
				'options'	=> array(
					'-1' => esc_html__('Default','viettitan'),
					'left' => esc_html__('Left','viettitan'),
					'center' => esc_html__('Center','viettitan'),
					'right' => esc_html__('Right','viettitan'),
				),
				'std' => '-1',
				'required-field' => array($prefix . 'show_page_title','<>','0'),
			),

            array(
                'name'  => esc_html__( 'Remove Margin Bottom', 'viettitan' ),
                'id'    => $prefix . 'page_title_remove_margin_bottom',
                'type'  => 'checkbox',
                'std'	=> 0,
	            'required-field' => array($prefix . 'show_page_title','<>','0'),
            ),
		)
	);

	// PAGE FOOTER
	//--------------------------------------------------
	$meta_boxes[] = array(
		'id' => $prefix . 'page_footer_meta_box',
		'title' => esc_html__('Page Footer', 'viettitan'),
		'post_types' => array('post', 'page',  'portfolio','product'),
		'tab' => true,
		'fields' => array(
			array (
				'name' 	=> esc_html__('Footer Settings', 'viettitan'),
				'id' 	=> $prefix . 'page_footer_section_1',
				'type' 	=> 'section',
				'std' 	=> '',
			),
			array (
				'name' 	=> esc_html__('Show/Hide Footer', 'viettitan'),
				'id' 	=> $prefix . 'footer_show_hide',
				'type' 	=> 'button_set',
				'std' 	=> '-1',
				'options' => array(
					'-1' => esc_html__('Default','viettitan'),
					'1' => esc_html__('Show Footer','viettitan'),
					'0' => esc_html__('Hide Footer','viettitan')
				),
				'desc' => esc_html__('Show/hide footer', 'viettitan'),
			),
			array (
				'name' 	=> esc_html__('Wrapper Layout', 'viettitan'),
				'id' 	=> $prefix . 'footer_wrapper_layout',
				'type' 	=> 'button_set',
				'std' 	=> '-1',
				'options' => array(
					'-1'                => esc_html__('Default','viettitan'),
					'full'              => esc_html__('Full Width','viettitan'),
					'container-fluid'   => esc_html__('Container Fluid','viettitan'),
				),
				'desc' => esc_html__('Select Footer Wrapper Layout', 'viettitan'),
				'required-field' => array($prefix . 'footer_show_hide','<>','0'),
			),
			array (
				'name' 	=> esc_html__('Footer Container Layout', 'viettitan'),
				'id' 	=> $prefix . 'footer_container_layout',
				'type' 	=> 'button_set',
				'std' 	=> '-1',
				'options' => array(
					'-1'                => esc_html__('Default','viettitan'),
					'full'              => esc_html__('Full Width','viettitan'),
					'container-fluid'   => esc_html__('Container Fluid','viettitan'),
					'container'         => esc_html__('Container','viettitan'),
				),
				'desc' => esc_html__('Select Footer Wrapper Layout', 'viettitan'),
				'required-field' => array($prefix . 'footer_show_hide','<>','0'),
			),
			array (
				'name' 	=> esc_html__('Layout', 'viettitan'),
				'id' 	=> $prefix . 'footer_layout',
				'type' 	=> 'image_set',
				'allowClear' => true,
				'width' => '80px',
				'std' 	=> '',
				'options' => array(
					'footer-1' => VIETTITAN_THEME_URL.'/assets/images/theme-options/footer-layout-1.jpg',
					'footer-2' => VIETTITAN_THEME_URL.'/assets/images/theme-options/footer-layout-2.jpg',
					'footer-3' => VIETTITAN_THEME_URL.'/assets/images/theme-options/footer-layout-3.jpg',
					'footer-4' => VIETTITAN_THEME_URL.'/assets/images/theme-options/footer-layout-4.jpg',
					'footer-5' => VIETTITAN_THEME_URL.'/assets/images/theme-options/footer-layout-5.jpg',
					'footer-6' => VIETTITAN_THEME_URL.'/assets/images/theme-options/footer-layout-6.jpg',
					'footer-7' => VIETTITAN_THEME_URL.'/assets/images/theme-options/footer-layout-7.jpg',
					'footer-8' => VIETTITAN_THEME_URL.'/assets/images/theme-options/footer-layout-8.jpg',
					'footer-9' => VIETTITAN_THEME_URL.'/assets/images/theme-options/footer-layout-9.jpg',
				),
				'desc' => esc_html__('Select Footer Layout (Not set to default).', 'viettitan'),
				'required-field' => array($prefix . 'footer_show_hide','<>','0'),
			),
			array (
				'name' 	=> esc_html__('Sidebar 1', 'viettitan'),
				'id' 	=> $prefix . 'footer_sidebar_1',
				'type' 	=> 'sidebars',
				'placeholder' => esc_html__('Select Sidebar','viettitan'),
				'std' 	=> '',
				'required-field' => array($prefix . 'footer_layout','=',array('footer-1','footer-2','footer-3','footer-4','footer-5','footer-6','footer-7','footer-8','footer-9')),
			),

			array (
				'name' 	=> esc_html__('Sidebar 2', 'viettitan'),
				'id' 	=> $prefix . 'footer_sidebar_2',
				'type' 	=> 'sidebars',
				'placeholder' => esc_html__('Select Sidebar','viettitan'),
				'std' 	=> '',
				'required-field' => array($prefix . 'footer_layout','=',array('footer-1','footer-2','footer-3','footer-4','footer-5','footer-6','footer-7','footer-8')),
			),

			array (
				'name' 	=> esc_html__('Sidebar 3', 'viettitan'),
				'id' 	=> $prefix . 'footer_sidebar_3',
				'type' 	=> 'sidebars',
				'placeholder' => esc_html__('Select Sidebar','viettitan'),
				'std' 	=> '',
				'required-field' => array($prefix . 'footer_layout','=',array('footer-1','footer-2','footer-3','footer-5','footer-8')),
			),

			array (
				'name' 	=> esc_html__('Sidebar 4', 'viettitan'),
				'id' 	=> $prefix . 'footer_sidebar_4',
				'type' 	=> 'sidebars',
				'placeholder' => esc_html__('Select Sidebar','viettitan'),
				'std' 	=> '',
				'required-field' => array($prefix . 'footer_layout','=',array('footer-1')),
			),
			array(
				'id'    => $prefix.  'footer_padding_top',
				'name'  => esc_html__('Main Footer padding top', 'viettitan'),
				'desc'  => esc_html__('Main Footer padding top. Do not include units (empty to set default)', 'viettitan'),
				'type'  => 'text',
				'sdt'   => '',
				'required-field' => array($prefix . 'footer_show_hide','<>','0'),
			),

			array(
				'id'    => $prefix.  'footer_padding_bottom',
				'name'  => esc_html__('Main Footer padding bottom', 'viettitan'),
				'desc'  => esc_html__('Main Footer padding bottom. Do not include units (empty to set default)', 'viettitan'),
				'type'  => 'text',
				'sdt'   => '',
				'required-field' => array($prefix . 'footer_show_hide','<>','0'),
			),
			array(
				'id'    => $prefix.  'footer_bg_image',
				'name'  => esc_html__('Background Image', 'viettitan'),
				'desc'  => esc_html__('Set footer background image', 'viettitan'),
				'type'  => 'image_advanced',
				'max_file_uploads' => 1,
				'std' => '',
				'required-field' => array($prefix . 'footer_show_hide','<>','0'),
			),

			array (
				'name' 	=> esc_html__('Footer Scheme', 'viettitan'),
				'id' 	=> $prefix . 'footer_scheme',
				'type' 	=> 'button_set',
				'std' 	=> '-1',
				'options' => array(
					'-1' => esc_html__('Default','viettitan'),
					'dark-black'    => esc_html__('Dark - Black','viettitan'),
					'light-black'   => esc_html__('Light - Black','viettitan'),
					'light'         => esc_html__('Light','viettitan'),
					'dark'          => esc_html__('Dark','viettitan'),
					'custom'        => esc_html__('Custom','viettitan'),
				),
				'desc' => esc_html__('Select Footer Scheme', 'viettitan'),
				'required-field' => array($prefix . 'footer_show_hide','<>','0'),
			),
			array(
				'id' => $prefix . 'footer_bg_color',
				'name' => esc_html__('Background color', 'viettitan'),
				'desc' => esc_html__("Set footer background color.", 'viettitan'),
				'type'  => 'color',
				'std' => '',
				'required-field' => array($prefix . 'footer_scheme','=',array('custom')),
			),
			array(
				'id'         => $prefix .'footer_bg_color_opacity',
				'name'       => esc_html__( 'Background color opacity', 'viettitan' ),
				'desc'       => esc_html__( 'Set the opacity level of the footer background color', 'viettitan' ),
				'clone'      => false,
				'type'       => 'slider',
				'prefix'     => '',
				'std' => '100',
				'js_options' => array(
					'min'  => 0,
					'max'  => 100,
					'step' => 1,
				),
				'required-field' => array($prefix . 'footer_scheme','=',array('custom')),
			),

			array(
				'id' => $prefix . 'footer_main_overlay_color',
				'name' => esc_html__('Main footer overlay color', 'viettitan'),
				'desc' => esc_html__("Set main footer overlay color", 'viettitan'),
				'type'  => 'color',
				'std' => '',
				'required-field' => array($prefix . 'footer_scheme','=',array('custom')),
			),
			array(
				'id'         => $prefix .'footer_main_overlay_opacity',
				'name'       => esc_html__( 'Main footer overlay opacity', 'viettitan' ),
				'desc'       => esc_html__( 'Set the opacity level of the main footer overlay', 'viettitan' ),
				'clone'      => false,
				'type'       => 'slider',
				'prefix'     => '',
				'std' => '0',
				'js_options' => array(
					'min'  => 0,
					'max'  => 100,
					'step' => 1,
				),
				'required-field' => array($prefix . 'footer_scheme','=',array('custom')),
			),

			array(
				'id' => $prefix . 'footer_text_color',
				'name' => esc_html__('Text color', 'viettitan'),
				'desc' => esc_html__("Set footer text color.", 'viettitan'),
				'type'  => 'color',
				'std' => '',
				'required-field' => array($prefix . 'footer_scheme','=',array('custom')),
			),

			array(
				'id' => $prefix . 'footer_heading_text_color',
				'name' => esc_html__('Heading text color', 'viettitan'),
				'desc' => esc_html__("Set footer heading text color.", 'viettitan'),
				'type'  => 'color',
				'std' => '',
				'required-field' => array($prefix . 'footer_scheme','=',array('custom')),
			),

			array(
				'id' => $prefix . 'footer_above_bg_color',
				'name' => esc_html__('Footer Above Background Color', 'viettitan'),
				'desc' => esc_html__("Set Footer Above Background Color.", 'viettitan'),
				'type'  => 'color',
				'std' => '',
				'required-field' => array($prefix . 'footer_scheme','=',array('custom')),
			),
			array(
				'id'         => $prefix .'footer_above_bg_color_opacity',
				'name'       => esc_html__( 'Footer Above Background color opacity', 'viettitan' ),
				'desc'       => esc_html__( 'Set the opacity level of the footer above background color', 'viettitan' ),
				'clone'      => false,
				'type'       => 'slider',
				'prefix'     => '',
				'js_options' => array(
					'min'  => 0,
					'max'  => 100,
					'step' => 1,
				),
				'required-field' => array($prefix . 'footer_scheme','=',array('custom')),
			),
			array(
				'id' => $prefix . 'footer_above_text_color',
				'name' => esc_html__('Footer Above Text Color', 'viettitan'),
				'desc' => esc_html__("Set Footer Above Text Color.", 'viettitan'),
				'type'  => 'color',
				'std' => '',
				'required-field' => array($prefix . 'footer_scheme','=',array('custom')),
			),
			array(
				'id' => $prefix . 'bottom_bar_bg_color',
				'name' => esc_html__('Bottom Bar Background Color', 'viettitan'),
				'desc' => esc_html__("Set Bottom Bar Background Color.", 'viettitan'),
				'type'  => 'color',
				'std' => '',
				'required-field' => array($prefix . 'footer_scheme','=',array('custom')),
			),
			array(
				'id'         => $prefix .'bottom_bar_bg_color_opacity',
				'name'       => esc_html__( 'Bottom Bar Background color opacity', 'viettitan' ),
				'desc'       => esc_html__( 'Set the opacity level of the bottom bar background color', 'viettitan' ),
				'clone'      => false,
				'type'       => 'slider',
				'prefix'     => '',
				'js_options' => array(
					'min'  => 0,
					'max'  => 100,
					'step' => 1,
				),
				'required-field' => array($prefix . 'footer_scheme','=',array('custom')),
			),
			array(
				'id' => $prefix . 'bottom_bar_text_color',
				'name' => esc_html__('Bottom Bar Text Color', 'viettitan'),
				'desc' => esc_html__("Set Bottom Bar Text Color.", 'viettitan'),
				'type'  => 'color',
				'std' => '',
				'required-field' => array($prefix . 'footer_scheme','=',array('custom')),
			),

			array (
				'name' 	=> esc_html__('Footer Parallax', 'viettitan'),
				'id' 	=> $prefix . 'footer_parallax',
				'type' 	=> 'button_set',
				'std' 	=> '-1',
				'options' => array(
					'-1' => esc_html__('Default','viettitan'),
					'1' => 'On',
					'0' => 'Off'
				),
				'desc' => esc_html__('Enable Footer Parallax', 'viettitan'),
				'required-field' => array($prefix . 'footer_show_hide','<>','0'),
			),

			array (
				'name' 	=> esc_html__('Collapse footer on mobile device', 'viettitan'),
				'id' 	=> $prefix . 'collapse_footer',
				'type' 	=> 'button_set',
				'std' 	=> '-1',
				'options' => array(
					'-1' => esc_html__('Default','viettitan'),
					'1' => 'On',
					'0' => 'Off'
				),
				'desc' => esc_html__('Enable collapse footer', 'viettitan'),
				'required-field' => array($prefix . 'footer_show_hide','<>','0'),
			),

			//--------------------------------------------------------------------
			array (
				'name' 	=> esc_html__('Footer Above Settings', 'viettitan'),
				'id' 	=> $prefix . 'page_footer_section_2',
				'type' 	=> 'section',
				'std' 	=> '',
			),
			array (
				'name' 	=> esc_html__('Show/Hide Footer Above', 'viettitan'),
				'id' 	=> $prefix . 'footer_above_enable',
				'type' 	=> 'button_set',
				'std' 	=> '-1',
				'options' => array(
					'-1' => esc_html__('Default','viettitan'),
					'1' => esc_html__('Show Footer Above','viettitan'),
					'0' => esc_html__('Hide Footer Above','viettitan')
				),
				'desc' => esc_html__('Show/hide footer above', 'viettitan'),
			),
            array (
                'name' 	=> esc_html__('Footer Above Layout', 'viettitan'),
                'id' 	=> $prefix . 'footer_above_layout',
                'type' 	=> 'image_set',
                'allowClear' => true,
                'width' => '80px',
                'std' 	=> '',
                'options' => array(
                    'footer-above-1' => VIETTITAN_THEME_URL.'/assets/images/theme-options/bottom-bar-layout-4.jpg',
                    'footer-above-2' => VIETTITAN_THEME_URL.'/assets/images/theme-options/bottom-bar-layout-1.jpg',
                ),
                'desc' => esc_html__('Footer above layout.', 'viettitan'),
                'required-field' => array($prefix . 'footer_above_enable','<>','0'),
            ),

            array (
                'name' 	=> esc_html__('Footer Above Left Sidebar', 'viettitan'),
                'id' 	=> $prefix . 'footer_above_left_sidebar',
                'type' 	=> 'sidebars',
                'placeholder' => esc_html__('Select Sidebar','viettitan'),
                'std' 	=> '',
                'required-field' => array($prefix . 'footer_above_enable','<>','0'),
            ),

            array (
                'name' 	=> esc_html__('Footer Above Right Sidebar', 'viettitan'),
                'id' 	=> $prefix . 'footer_above_right_sidebar',
                'type' 	=> 'sidebars',
                'placeholder' => esc_html__('Select Sidebar','viettitan'),
                'std' 	=> '',
                'required-field' => array($prefix . 'footer_above_enable','<>','0'),
            ),
			array(
				'id'    => $prefix.  'footer_above_padding_top',
				'name'  => esc_html__('Footer above padding top', 'viettitan'),
				'desc'  => esc_html__('Footer above padding top. Do not include units (empty to set default)', 'viettitan'),
				'type'  => 'text',
				'sdt'   => '',
				'required-field' => array($prefix . 'footer_above_enable','<>','0'),
			),

			array(
				'id'    => $prefix.  'footer_above_padding_bottom',
				'name'  => esc_html__('Footer above padding bottom', 'viettitan'),
				'desc'  => esc_html__('Footer above padding bottom. Do not include units (empty to set default)', 'viettitan'),
				'type'  => 'text',
				'sdt'   => '',
				'required-field' => array($prefix . 'footer_above_enable','<>','0'),
			),

			//--------------------------------------------------------------------
			array (
				'name' 	=> esc_html__('Bottom Bar Settings', 'viettitan'),
				'id' 	=> $prefix . 'page_footer_section_3',
				'type' 	=> 'section',
				'std' 	=> '',
			),
			array (
				'name' 	=> esc_html__('Show/Hide Bottom Bar', 'viettitan'),
				'id' 	=> $prefix . 'bottom_bar',
				'type' 	=> 'button_set',
				'std' 	=> '-1',
				'options' => array(
					'-1' => 'Default',
					'1' => 'Show Bottom Bar',
					'0' => 'Hide Bottom Bar'
				),
				'desc' => esc_html__('Show Hide Bottom Bar.', 'viettitan'),
			),
			array (
				'name' 	=> esc_html__('Bottom Bar Layout', 'viettitan'),
				'id' 	=> $prefix . 'bottom_bar_layout',
				'type' 	=> 'image_set',
				'allowClear' => true,
				'width' => '80px',
				'std' 	=> '',
				'options' => array(
					'bottom-bar-1' => VIETTITAN_THEME_URL.'/assets/images/theme-options/bottom-bar-layout-1.jpg',
					'bottom-bar-2' => VIETTITAN_THEME_URL.'/assets/images/theme-options/bottom-bar-layout-2.jpg',
					'bottom-bar-3' => VIETTITAN_THEME_URL.'/assets/images/theme-options/bottom-bar-layout-3.jpg',
					'bottom-bar-4' => VIETTITAN_THEME_URL.'/assets/images/theme-options/bottom-bar-layout-4.jpg',
				),
				'desc' => esc_html__('Bottom bar layout.', 'viettitan'),
                'required-field' => array($prefix . 'bottom_bar','<>','0'),
			),

			array (
				'name' 	=> esc_html__('Bottom Bar Left Sidebar', 'viettitan'),
				'id' 	=> $prefix . 'bottom_bar_left_sidebar',
				'type' 	=> 'sidebars',
				'placeholder' => esc_html__('Select Sidebar','viettitan'),
				'std' 	=> '',
                'required-field' => array($prefix . 'bottom_bar','<>','0'),
			),

			array (
				'name' 	=> esc_html__('Bottom Bar Right Sidebar', 'viettitan'),
				'id' 	=> $prefix . 'bottom_bar_right_sidebar',
				'type' 	=> 'sidebars',
				'placeholder' => esc_html__('Select Sidebar','viettitan'),
				'std' 	=> '',
                'required-field' => array($prefix . 'bottom_bar','<>','0'),
			),
			array(
				'id'    => $prefix.  'bottom_bar_padding_top',
				'name'  => esc_html__('Bottom bar padding top', 'viettitan'),
				'desc'  => esc_html__('Bottom bar padding top. Do not include units (empty to set default)', 'viettitan'),
				'type'  => 'text',
				'sdt'   => '',
				'required-field' => array($prefix . 'bottom_bar','<>','0'),
			),

			array(
				'id'    => $prefix.  'bottom_bar_padding_bottom',
				'name'  => esc_html__('Bottom bar padding bottom', 'viettitan'),
				'desc'  => esc_html__('Bottom bar padding bottom. Do not include units (empty to set default)', 'viettitan'),
				'type'  => 'text',
				'sdt'   => '',
				'required-field' => array($prefix . 'bottom_bar','<>','0'),
			),

		)
	);

	// Make sure there's no errors when the plugin is deactivated or during upgrade
	if (class_exists('RW_Meta_Box')) {
		foreach ($meta_boxes as $meta_box) {
			new RW_Meta_Box($meta_box);
		}
	}
}

// Hook to 'admin_init' to make sure the meta box class is loaded before
// (in case using the meta box class in another plugin)
// This is also helpful for some conditionals like checking page template, categories, etc.
add_action('admin_init', 'viettitan_register_meta_boxes');
