<?php
/*---------------------------------------------------
/* THEME ADD ACTION
/*---------------------------------------------------*/
remove_action('viettitan_after_single_post_content','viettitan_share',15);

/*---------------------------------------------------
/* CUSTOM HEADER CSS
/*---------------------------------------------------*/
if (!function_exists('viettitan_custom_header_css')) {
	function viettitan_custom_header_css() {
		$page_id = '0';
		if (isset($_REQUEST['current_page_id'])) {
			$page_id = $_REQUEST['current_page_id'];
		}

		$css_variable = viettitan_custom_css_variable($page_id);

		if (!class_exists('Less_Parser')) {
			require_once VIETTITAN_THEME_DIR . 'viettitan-framework/less/Less.php';
		}
		$parser = new Less_Parser(array( 'compress'=>true ));

		$parser->parse($css_variable, VIETTITAN_THEME_URL);
		$parser->parseFile( VIETTITAN_THEME_DIR . 'assets/css/less/variable.less', VIETTITAN_THEME_URL );
		$parser->parseFile( VIETTITAN_THEME_DIR . 'assets/css/less/header-customize.less', VIETTITAN_THEME_URL );
		$parser->parseFile( VIETTITAN_THEME_DIR . 'assets/css/less/footer-customize.less', VIETTITAN_THEME_URL );

		$prefix = 'viettitan_';
		$enable_page_color = rwmb_meta($prefix . 'enable_page_color', array(), $page_id);
		if ($enable_page_color == '1') {
			$parser->parseFile( VIETTITAN_THEME_DIR . 'assets/css/less/color.less' );
		}

		$css = $parser->getCss();
		header("Content-type: text/css; charset: UTF-8");
		echo sprintf('%s', $css);
	}
	add_action('custom-page/header-custom-css', 'viettitan_custom_header_css');
}





