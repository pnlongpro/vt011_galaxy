<?php
if (!function_exists('viettitan_register_sidebar')) {
    function viettitan_register_sidebar() {
        register_sidebar( array(
            'name'          => esc_html__("Sidebar 1",'viettitan'),
            'id'            => 'sidebar-1',
            'description'   => esc_html__("Widget Area 1",'viettitan'),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h4 class="widget-title"><span>',
            'after_title'   => '</span></h4>',
        ) );
        register_sidebar( array(
            'name'          => esc_html__("Sidebar 2",'viettitan'),
            'id'            => 'sidebar-2',
            'description'   => esc_html__("Widget Area 2",'viettitan'),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h4 class="widget-title"><span>',
            'after_title'   => '</span></h4>',
        ) );

        register_sidebar( array(
            'name'          => esc_html__("Top Drawer",'viettitan'),
            'id'            => 'top_drawer_sidebar',
            'description'   => esc_html__("Top Drawer",'viettitan'),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h4 class="widget-title"><span>',
            'after_title'   => '</span></h4>',
        ));
	    register_sidebar( array(
		    'name'          => esc_html__("Top Bar Left",'viettitan'),
		    'id'            => 'top_bar_left',
		    'description'   => esc_html__("Top Bar Left",'viettitan'),
		    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		    'after_widget'  => '</aside>',
		    'before_title'  => '<h4 class="widget-title"><span>',
		    'after_title'   => '</span></h4>',
	    ) );

	    register_sidebar( array(
		    'name'          => esc_html__("Top Bar Right",'viettitan'),
		    'id'            => 'top_bar_right',
		    'description'   => esc_html__("Top Bar Right",'viettitan'),
		    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		    'after_widget'  => '</aside>',
		    'before_title'  => '<h4 class="widget-title"><span>',
		    'after_title'   => '</span></h4>',
	    ) );

        register_sidebar( array(
            'name'          => esc_html__("Footer Above Left",'viettitan'),
            'id'            => 'footer_above_left',
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h4 class="widget-title"><span>',
            'after_title'   => '</span></h4>',
        ) );

        register_sidebar( array(
            'name'          => esc_html__("Footer Above Right",'viettitan'),
            'id'            => 'footer_above_right',
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h4 class="widget-title"><span>',
            'after_title'   => '</span></h4>',
        ) );

        register_sidebar( array(
            'name'          => esc_html__("Footer 1",'viettitan'),
            'id'            => 'footer-1',
            'description'   => esc_html__("Footer 1",'viettitan'),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h4 class="widget-title"><span>',
            'after_title'   => '</span></h4>',
        ) );

        register_sidebar( array(
            'name'          => esc_html__("Footer 2",'viettitan'),
            'id'            => 'footer-2',
            'description'   => esc_html__("Footer 2",'viettitan'),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h4 class="widget-title"><span>',
            'after_title'   => '</span></h4>',
        ) );

        register_sidebar( array(
            'name'          => esc_html__("Footer 3",'viettitan'),
            'id'            => 'footer-3',
            'description'   => esc_html__("Footer 3",'viettitan'),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h4 class="widget-title"><span>',
            'after_title'   => '</span></h4>',
        ) );

        register_sidebar( array(
            'name'          => esc_html__("Footer 4",'viettitan'),
            'id'            => 'footer-4',
            'description'   => esc_html__("Footer 4",'viettitan'),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h4 class="widget-title"><span>',
            'after_title'   => '</span></h4>',
        ) );

        register_sidebar( array(
            'name'          => esc_html__("Bottom Bar Left",'viettitan'),
            'id'            => 'bottom_bar_left',
            'description'   => esc_html__("Bottom Bar Left",'viettitan'),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h4 class="widget-title"><span>',
            'after_title'   => '</span></h4>',
        ) );

        register_sidebar( array(
            'name'          => esc_html__("Bottom Bar Right",'viettitan'),
            'id'            => 'bottom_bar_right',
            'description'   => esc_html__("Bottom Bar Right",'viettitan'),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h4 class="widget-title"><span>',
            'after_title'   => '</span></h4>',
        ) );


        register_sidebar( array(
            'name'          => esc_html__("Woocommerce",'viettitan'),
            'id'            => 'woocommerce',
            'description'   => esc_html__("Woocommerce",'viettitan'),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h4 class="widget-title"><span>',
            'after_title'   => '</span></h4>',
        ) );

	    register_sidebar( array(
		    'name'          => esc_html__("Canvas Menu",'viettitan'),
		    'id'            => 'canvas-menu',
		    'description'   => esc_html__("Canvas Menu Widget Area",'viettitan'),
		    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		    'after_widget'  => '</aside>',
		    'before_title'  => '<h4 class="widget-title"><span>',
		    'after_title'   => '</span></h4>',
	    ) );

        $theme_mods = get_theme_mods();
        if(is_array($theme_mods) && array_key_exists('redux-widget-areas', $theme_mods)){
            $sidebar = $theme_mods['redux-widget-areas'];
            foreach ($sidebar as $name){
                register_sidebar( array(
                    'name'          => $name,
                    'id'            => str_replace(' ','',strtolower ($name)),
                    'description'   =>  $name,
                    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</aside>',
                    'before_title'  => '<h4 class="widget-title"><span>',
                    'after_title'   => '</span></h4>',
                ) );
            }
        }



    }
    add_action( 'widgets_init', 'viettitan_register_sidebar' );
}

if (!function_exists('viettitan_redux_custom_widget_area_filter')) {
    function viettitan_redux_custom_widget_area_filter($arg) {
        return array(
            'before_title'  => '<h4 class="widget-title">',
            'after_title'   => '</h4>',
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>'
        );
    }
    add_filter('redux_custom_widget_args','viettitan_redux_custom_widget_area_filter');
}

