<?php
if (!function_exists('viettitan_admin_enqueue_scripts')) {

	function viettitan_admin_enqueue_scripts() {
		// Enqueue Script
		wp_enqueue_script( 'viettitan-admin-app-js', VIETTITAN_THEME_URL . '/admin/assets/js/admin.app.js',array(), '1.0.0', true );
		$viettitan_options = &Viettitan_Global::get_options();
		$meta_boxes = &Viettitan_Global::get_meta_boxes();
		$meta_box_id = '';
		foreach ($meta_boxes as $box) {
			if (!isset($box['tab'])) {
				continue;
			}
			if (!empty($meta_box_id)) {
				$meta_box_id .= ',';
			}
			$meta_box_id .= '#' . $box['id'];
		}

		wp_localize_script( 'viettitan-admin-app-js' , 'meta_box_ids' , $meta_box_id);

		// Enqueue CSS
		wp_enqueue_style( 'viettitan-admin-meta-box-css', VIETTITAN_THEME_URL . '/admin/assets/css/meta-box.css', false, '1.0.0' );
        wp_enqueue_style( 'viettitan-admin-template-css', VIETTITAN_THEME_URL . '/admin/assets/css/template.css', false, '1.0.0' );
		wp_enqueue_style( 'viettitan-admin-redux-css', VIETTITAN_THEME_URL . '/admin/assets/css/redux-admin.css', false, '1.0.0' );

		/*font-awesome*/
		$url_font_awesome = VIETTITAN_THEME_URL . 'assets/plugins/fonts-awesome/css/font-awesome.min.css';
		if (isset($viettitan_options['cdn_font_awesome']) && !empty($viettitan_options['cdn_font_awesome'])) {
			$url_font_awesome = $viettitan_options['cdn_font_awesome'];
		}
		$min_suffix = (isset($viettitan_options['enable_minifile_css']) && $viettitan_options['enable_minifile_css'] == 1) ? '.min' :  '';
		wp_enqueue_style('viettitan_framework_font_awesome', $url_font_awesome, array());
		wp_enqueue_style('viettitan_framework_font_awesome_animation', VIETTITAN_THEME_URL . 'assets/plugins/fonts-awesome/css/font-awesome-animation.min.css', array());
		/*Viettitan FontIcon*/
		wp_enqueue_style('viettitan_framework_viettitan_icon', VIETTITAN_THEME_URL . 'assets/plugins/viettitan-icon/css/styles'.$min_suffix.'.css', array());

	}
	add_action( 'admin_enqueue_scripts', 'viettitan_admin_enqueue_scripts' );
}