<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 4/9/2016
 * Time: 12:41 PM
 */

//Customs Viettitan RSS Feed
if ( !class_exists( 'viettitanDashboardWidget' ) ) {
	class viettitanDashboardWidget {
		public function __construct() {
			add_action( 'wp_dashboard_setup', array( $this, 'add_viettitan_dashboard' ) );
		}

		public function add_viettitan_dashboard() {
			add_meta_box( 'viettitan_dashboard_widget', 'Viettitan Update', array( $this, 'viettitan_dashboard_widget' ), 'dashboard', 'side', 'high' );
		}

		public function viettitan_dashboard_widget() {
			echo '<div class="rss-widget">';
			wp_widget_rss_output( array(
				'url'          => 'http://viettitan.com/feed/',
				'title'        => 'VIETTITAN_NEWS',
				'items'        => 4,
				'show_summary' => 1,
				'show_author'  => 0,
				'show_date'    => 1
			) );
			echo '</div>';
		}
	}

	new viettitanDashboardWidget();
}

// remove unwanted dashboard widgets for relevant users
function wptutsplus_remove_dashboard_widgets() {
	$user = wp_get_current_user();
	if ( !$user->has_cap( 'manage_options' ) ) {
		remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
		remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
		remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
		remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
		remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );
		remove_meta_box( 'dashboard_activity', 'dashboard', 'side' );
	}
}

add_action( 'wp_dashboard_setup', 'wptutsplus_remove_dashboard_widgets' );

// Move the 'Right Now' dashboard widget to the right hand side
function wptutsplus_move_dashboard_widget() {
	$user = wp_get_current_user();
	if ( !$user->has_cap( 'manage_options' ) ) {
		global $wp_meta_boxes;
		$widget = $wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now'];
		unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now'] );
		$wp_meta_boxes['dashboard']['side']['core']['dashboard_right_now'] = $widget;
	}
}

add_action( 'wp_dashboard_setup', 'wptutsplus_move_dashboard_widget' );

remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );


function remove_dashboard_widgets() {
	global $wp_meta_boxes;

	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments'] );
	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_primary'] );
	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity'] );

}

add_action( 'wp_dashboard_setup', 'remove_dashboard_widgets' );

if ( !current_user_can( 'manage_options' ) ) {
	add_action( 'wp_dashboard_setup', 'remove_dashboard_widgets' );
}
