<?php
function viettitan_custom_wp_admin_style()
{
    $primary_color = '';
    if (isset($viettitan_options['primary_color']) && !empty($viettitan_options['primary_color'])) {
        $primary_color = $viettitan_options['primary_color'];
    }
    if (empty($primary_color)) {
        $primary_color = '#29B667';
    }
    $secondary_color = '';
    if (isset($viettitan_options['secondary_color']) && !empty($viettitan_options['secondary_color'])) {
        $secondary_color = $viettitan_options['secondary_color'];
    }
    if (empty($secondary_color)) {
        $secondary_color = '#00BFFF';
    }
    echo "<style>
    .vc_colored-dropdown .primary-color {
        background-color: {$primary_color} !important;
    }
    .vc_colored-dropdown .secondary-color {
        background-color: {$secondary_color} !important;
    }</style>";
}

add_action('admin_head', 'viettitan_custom_wp_admin_style');

add_action('vc_before_init', 'viettitan_vcSetAsTheme');
function viettitan_vcSetAsTheme()
{
    vc_set_as_theme();
}

function viettitan_number_settings_field($settings, $value)
{
    $param_name = isset($settings['param_name']) ? $settings['param_name'] : '';
    $type = isset($settings['type']) ? $settings['type'] : '';
    $min = isset($settings['min']) ? $settings['min'] : '';
    $max = isset($settings['max']) ? $settings['max'] : '';
    $suffix = isset($settings['suffix']) ? $settings['suffix'] : '';
    $class = isset($settings['class']) ? $settings['class'] : '';
    $output = '<input type="number" min="' . esc_attr($min) . '" max="' . esc_attr($max) . '" class="wpb_vc_param_value ' . esc_attr($param_name) . ' ' . esc_attr($type) . ' ' . esc_attr($class) . '" name="' . esc_attr($param_name) . '" value="' . esc_attr($value) . '" style="max-width:100px; margin-right: 10px;" />' . esc_attr($suffix);
    return $output;
}

function viettitan_icon_text_settings_field($settings, $value)
{
    return '<div class="vc-text-icon">'
    . '<input  name="' . $settings['param_name'] . '" style="width:80%;" class="wpb_vc_param_value wpb-textinput widefat input-icon ' . esc_attr($settings['param_name']) . ' ' . esc_attr($settings['type']) . '_field" type="text" value="' . esc_attr($value) . '"/>'
    . '<input title="' . esc_html__('Click to browse icon', 'viettitan') . '" style="width:20%; height:34px;" class="browse-icon button-secondary" type="button" value="' . esc_html__('Browse Icon', 'viettitan') . '" >'
    . '<span class="icon-preview"><i class="' . esc_attr($value) . '"></i></span>'
    . '</div>';
}

function viettitan_multi_select_settings_field_shortcode_param($settings, $value)
{
    $param_name = isset($settings['param_name']) ? $settings['param_name'] : '';
    $param_option = isset($settings['options']) ? $settings['options'] : '';
    $output = '<input type="hidden" name="' . $param_name . '" id="' . $param_name . '" class="wpb_vc_param_value ' . $param_name . '" value="' . $value . '"/>';
    $output .= '<select multiple id="' . $param_name . '_select2" name="' . $param_name . '_select2" class="multi-select">';
    if ($param_option != '' && is_array($param_option)) {
        foreach ($param_option as $text_val => $val) {
            if (is_numeric($text_val) && (is_string($val) || is_numeric($val))) {
                $text_val = $val;
            }
            $output .= '<option id="' . $val . '" value="' . $val . '">' . htmlspecialchars($text_val) . '</option>';
        }
    }

    $output .= '</select><input type="checkbox" id="' . $param_name . '_select_all" >' . esc_html__('Select All', 'viettitan');
    $output .= '<script type="text/javascript">
		        jQuery(document).ready(function($){

					$("#' . $param_name . '_select2").select2();

					var order = $("#' . $param_name . '").val();
					if (order != "") {
						order = order.split(",");
						var choices = [];
						for (var i = 0; i < order.length; i++) {
							var option = $("#' . $param_name . '_select2 option[value="+ order[i]  + "]");
							if (option.length > 0) {
							    choices[i] = {id:order[i], text:option[0].label, element: option};
							}
						}
						$("#' . $param_name . '_select2").select2("data", choices);
					}

			        $("#' . $param_name . '_select2").on("select2-selecting", function(e) {
			            var ids = $("#' . $param_name . '").val();
			            if (ids != "") {
			                ids +=",";
			            }
			            ids += e.val;
			            $("#' . $param_name . '").val(ids);
                    }).on("select2-removed", function(e) {
				          var ids = $("#' . $param_name . '").val();
				          var arr_ids = ids.split(",");
				          var newIds = "";
				          for(var i = 0 ; i < arr_ids.length; i++) {
				            if (arr_ids[i] != e.val){
				                if (newIds != "") {
			                        newIds +=",";
					            }
					            newIds += arr_ids[i];
				            }
				          }
				          $("#' . $param_name . '").val(newIds);
		             });

		            $("#' . $param_name . '_select_all").click(function(){
		                if($("#' . $param_name . '_select_all").is(":checked") ){
		                    $("#' . $param_name . '_select2 > option").prop("selected","selected");
		                    $("#' . $param_name . '_select2").trigger("change");
		                    var arr_ids =  $("#' . $param_name . '_select2").select2("val");
		                    var ids = "";
                            for (var i = 0; i < arr_ids.length; i++ ) {
                                if (ids != "") {
                                    ids +=",";
                                }
                                ids += arr_ids[i];
                            }
                            $("#' . $param_name . '").val(ids);

		                }else{
		                    $("#' . $param_name . '_select2 > option").removeAttr("selected");
		                    $("#' . $param_name . '_select2").trigger("change");
		                    $("#' . $param_name . '").val("");
		                }
		            });
		        });
		        </script>
		        <style>
		            .multi-select
		            {
		              width: 100%;
		            }
		            .select2-drop
		            {
		                z-index: 100000;
		            }
		        </style>';
    return $output;
}

function viettitan_tags_settings_field_shortcode_param($settings, $value)
{
    $param_name = isset($settings['param_name']) ? $settings['param_name'] : '';
    $output = '<input  name="' . $settings['param_name']
        . '" class="wpb_vc_param_value wpb-textinput '
        . $settings['param_name'] . ' ' . $settings['type']
        . '" type="hidden" value="' . $value . '"/>';
    $output .= '<input type="text" name="' . $param_name . '_tagsinput" id="' . $param_name . '_tagsinput" value="' . $value . '" data-role="tagsinput"/>';
    $output .= '<script type="text/javascript">
							jQuery(document).ready(function($){
								$("input[data-role=tagsinput], select[multiple][data-role=tagsinput]").tagsinput();

								$("#' . $param_name . '_tagsinput").on("itemAdded", function(event) {
		                             $("input[name=' . $param_name . ']").val($(this).val());
								});

								$("#' . $param_name . '_tagsinput").on("itemRemoved", function(event) {
		                             $("input[name=' . $param_name . ']").val($(this).val());
								});
							});
						</script>';
    return $output;
}

if (function_exists('vc_add_' . 'shortcode_param')) {
	call_user_func('vc_add_' . 'shortcode_param', 'number', 'viettitan_number_settings_field');
	call_user_func('vc_add_' . 'shortcode_param', 'icon_text', 'viettitan_icon_text_settings_field');
	call_user_func('vc_add_' . 'shortcode_param', 'multi-select', 'viettitan_multi_select_settings_field_shortcode_param');
	call_user_func('vc_add_' . 'shortcode_param', 'tags', 'viettitan_tags_settings_field_shortcode_param');
}
function viettitan_add_vc_param()
{
    $viettitan_icons = &Viettitan_Global::theme_font_icon();
	$viettitan_font_awesome = &Viettitan_Global::font_awesome();
    if (function_exists('vc_add_param')) {
        vc_add_param('vc_tta_accordion', array(
                'type' => 'dropdown',
                'param_name' => 'style',
                'value' => array(
                    esc_html__('Icon backround dark, active gray', 'viettitan') => 'accordion_style1',
                    esc_html__('Icon backround gray, active dark', 'viettitan') => 'accordion_style2',
                    esc_html__('Panel title transparent, active transparent', 'viettitan') => 'accordion_style3',
                    esc_html__('Panel title transparent, active dark', 'viettitan') => 'accordion_style4',
                    esc_html__('Classic', 'viettitan') => 'classic',
                    esc_html__('Modern', 'viettitan') => 'modern',
                    esc_html__('Flat', 'viettitan') => 'flat',
                    esc_html__('Outline', 'viettitan') => 'outline',
                ),
                'heading' => esc_html__('Style', 'viettitan'),
                'description' => esc_html__('Select accordion display style.', 'viettitan'),
                'weight' => 1,
            )
        );
        vc_add_param('vc_tta_tabs', array(
                'type' => 'dropdown',
                'param_name' => 'style',
                'value' => array(
                    esc_html__('Viettitan', 'viettitan') => 'tab_style1',
                    esc_html__('Classic', 'viettitan') => 'classic',
                    esc_html__('Modern', 'viettitan') => 'modern',
                    esc_html__('Flat', 'viettitan') => 'flat',
                    esc_html__('Outline', 'viettitan') => 'outline',
                ),
                'heading' => esc_html__('Style', 'viettitan'),
                'description' => esc_html__('Select tabs display style.', 'viettitan'),
                'weight' => 1,
            )
        );
        vc_add_param('vc_tta_tour', array(
                'type' => 'dropdown',
                'param_name' => 'style',
                'value' => array(
                    esc_html__('Viettitan', 'viettitan') => 'tour_style1',
                    esc_html__('Classic', 'viettitan') => 'classic',
                    esc_html__('Modern', 'viettitan') => 'modern',
                    esc_html__('Flat', 'viettitan') => 'flat',
                    esc_html__('Outline', 'viettitan') => 'outline',
                ),
                'heading' => esc_html__('Style', 'viettitan'),
                'description' => esc_html__('Select tabs display style.', 'viettitan'),
                'weight' => 1,
            )
        );
        vc_remove_param('vc_icon', 'type');
        vc_add_param('vc_icon', array(
                'type' => 'dropdown',
                'heading' => esc_html__('Icon library', 'viettitan'),
                'value' => array(
                    esc_html__('Viettitan Icon', 'viettitan') => 'viettitan',
                    esc_html__('Font Awesome', 'viettitan') => 'fontawesome',
                    esc_html__('Open Iconic', 'viettitan') => 'openiconic',
                    esc_html__('Typicons', 'viettitan') => 'typicons',
                    esc_html__('Entypo', 'viettitan') => 'entypo',
                    esc_html__('Linecons', 'viettitan') => 'linecons',
                ),
                'admin_label' => true,
                'weight' => 2,
                'param_name' => 'type',
                'description' => esc_html__('Select icon library.', 'viettitan'),
            )
        );
        vc_add_param('vc_icon',array(
                'type' => 'iconpicker',
                'heading' => esc_html__( 'Icon', 'viettitan' ),
                'param_name' => 'icon_fontawesome',
                'value' => 'fa fa-info-circle',
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'iconsPerPage' => 4000,
                    'source' => $viettitan_font_awesome,
                    // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'fontawesome',
                ),
                'description' => esc_html__( 'Select icon from library.', 'viettitan' ),
            )
        );
        vc_add_param('vc_icon', array(
                'type' => 'iconpicker',
                'heading' => esc_html__('Icon', 'viettitan'),
                'param_name' => 'icon_viettitan',
                'settings' => array(
                    'emptyIcon' => false, // default true, display an "EMPTY" icon?
                    'iconsPerPage' => 4000,
                    'type' => 'viettitan',
                    'source' => $viettitan_icons,
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'viettitan',
                ),
                'weight' => 1,
                'description' => esc_html__('Select icon from library.', 'viettitan'),
            )
        );

        $settings_vc_map = array(
            'category' => array(esc_html__('Content', 'viettitan'), esc_html__('Viettitan Shortcodes', 'viettitan'))
        );
        vc_map_update('vc_tta_tabs', $settings_vc_map);
        vc_map_update('vc_tta_tour', $settings_vc_map);
        vc_map_update('vc_tta_accordion', $settings_vc_map);
    }
}

viettitan_add_vc_param();

function viettitan_get_css_animation($css_animation)
{
    $output = '';
    if ($css_animation != '') {
        wp_enqueue_script('waypoints');
        $output = ' wpb_animate_when_almost_visible viettitan-css-animation ' . $css_animation;
    }
    return $output;
}

function viettitan_get_style_animation($duration, $delay)
{
    $styles = array();
    if ($duration != '0' && !empty($duration)) {
        $duration = (float)trim($duration, "\n\ts");
        $styles[] = "-webkit-animation-duration: {$duration}s";
        $styles[] = "-moz-animation-duration: {$duration}s";
        $styles[] = "-ms-animation-duration: {$duration}s";
        $styles[] = "-o-animation-duration: {$duration}s";
        $styles[] = "animation-duration: {$duration}s";
    }
    if ($delay != '0' && !empty($delay)) {
        $delay = (float)trim($delay, "\n\ts");
        $styles[] = "opacity: 0";
        $styles[] = "-webkit-animation-delay: {$delay}s";
        $styles[] = "-moz-animation-delay: {$delay}s";
        $styles[] = "-ms-animation-delay: {$delay}s";
        $styles[] = "-o-animation-delay: {$delay}s";
        $styles[] = "animation-delay: {$delay}s";
    }
    if (count($styles) > 1) {
        return 'style="' . implode(';', $styles) . '"';
    }
    return implode(';', $styles);
}

function  viettitan_convert_hex_to_rgba($hex, $opacity = 1)
{
    $hex = str_replace("#", "", $hex);
    if (strlen($hex) == 3) {
        $r = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
        $g = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
        $b = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
    } else {
        $r = hexdec(substr($hex, 0, 2));
        $g = hexdec(substr($hex, 2, 2));
        $b = hexdec(substr($hex, 4, 2));
    }
    $rgba = 'rgba(' . $r . ',' . $g . ',' . $b . ',' . $opacity . ')';
    return $rgba;
}

function viettitan_register_vc_map()
{
    $viettitan_icons = &Viettitan_Global::theme_font_icon();
	$viettitan_font_awesome = &Viettitan_Global::font_awesome();
    $pixel_icons = &Viettitan_Global::get_pixel_icons();
    $add_css_animation = array(
        'type' => 'dropdown',
        'heading' => esc_html__('CSS Animation', 'viettitan'),
        'param_name' => 'css_animation',
        'value' => array(esc_html__('No', 'viettitan') => '', esc_html__('Fade In', 'viettitan') => 'wpb_fadeIn', esc_html__('Fade Top to Bottom', 'viettitan') => 'wpb_fadeInDown', esc_html__('Fade Bottom to Top', 'viettitan') => 'wpb_fadeInUp', esc_html__('Fade Left to Right', 'viettitan') => 'wpb_fadeInLeft', esc_html__('Fade Right to Left', 'viettitan') => 'wpb_fadeInRight', esc_html__('Bounce In', 'viettitan') => 'wpb_bounceIn', esc_html__('Bounce Top to Bottom', 'viettitan') => 'wpb_bounceInDown', esc_html__('Bounce Bottom to Top', 'viettitan') => 'wpb_bounceInUp', esc_html__('Bounce Left to Right', 'viettitan') => 'wpb_bounceInLeft', esc_html__('Bounce Right to Left', 'viettitan') => 'wpb_bounceInRight', esc_html__('Zoom In', 'viettitan') => 'wpb_zoomIn', esc_html__('Flip Vertical', 'viettitan') => 'wpb_flipInX', esc_html__('Flip Horizontal', 'viettitan') => 'wpb_flipInY', esc_html__('Bounce', 'viettitan') => 'wpb_bounce', esc_html__('Flash', 'viettitan') => 'wpb_flash', esc_html__('Shake', 'viettitan') => 'wpb_shake', esc_html__('Pulse', 'viettitan') => 'wpb_pulse', esc_html__('Swing', 'viettitan') => 'wpb_swing', esc_html__('Rubber band', 'viettitan') => 'wpb_rubberBand', esc_html__('Wobble', 'viettitan') => 'wpb_wobble', esc_html__('Tada', 'viettitan') => 'wpb_tada'),
        'description' => esc_html__('Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'viettitan'),
        'group' => esc_html__('Animation Settings', 'viettitan')
    );

    $add_duration_animation = array(
        'type' => 'textfield',
        'heading' => esc_html__('Animation Duration', 'viettitan'),
        'param_name' => 'duration',
        'value' => '',
        'description' => esc_html__('Duration in seconds. You can use decimal points in the value. Use this field to specify the amount of time the animation plays. <em>The default value depends on the animation, leave blank to use the default.</em>', 'viettitan'),
        'dependency' => Array('element' => 'css_animation', 'value' => array('wpb_fadeIn', 'wpb_fadeInDown', 'wpb_fadeInUp', 'wpb_fadeInLeft', 'wpb_fadeInRight', 'wpb_bounceIn', 'wpb_bounceInDown', 'wpb_bounceInUp', 'wpb_bounceInLeft', 'wpb_bounceInRight', 'wpb_zoomIn', 'wpb_flipInX', 'wpb_flipInY', 'wpb_bounce', 'wpb_flash', 'wpb_shake', 'wpb_pulse', 'wpb_swing', 'wpb_rubberBand', 'wpb_wobble', 'wpb_tada')),
        'group' => esc_html__('Animation Settings', 'viettitan')
    );

    $add_delay_animation = array(
        'type' => 'textfield',
        'heading' => esc_html__('Animation Delay', 'viettitan'),
        'param_name' => 'delay',
        'value' => '',
        'description' => esc_html__('Delay in seconds. You can use decimal points in the value. Use this field to delay the animation for a few seconds, this is helpful if you want to chain different effects one after another above the fold.', 'viettitan'),
        'dependency' => Array('element' => 'css_animation', 'value' => array('wpb_fadeIn', 'wpb_fadeInDown', 'wpb_fadeInUp', 'wpb_fadeInLeft', 'wpb_fadeInRight', 'wpb_bounceIn', 'wpb_bounceInDown', 'wpb_bounceInUp', 'wpb_bounceInLeft', 'wpb_bounceInRight', 'wpb_zoomIn', 'wpb_flipInX', 'wpb_flipInY', 'wpb_bounce', 'wpb_flash', 'wpb_shake', 'wpb_pulse', 'wpb_swing', 'wpb_rubberBand', 'wpb_wobble', 'wpb_tada')),
        'group' => esc_html__('Animation Settings', 'viettitan')
    );
    $params_row = array(
        array(
            'type' => 'dropdown',
            'heading' => esc_html__('Layout', 'viettitan'),
            'param_name' => 'layout',
            'value' => array(
                esc_html__('Full Width (overflow hidden)', 'viettitan') => 'wide',
                esc_html__('Full Width (overflow visible)', 'viettitan') => 'fullwidth-visible',
                esc_html__('Container', 'viettitan') => 'boxed',
                esc_html__('Container Fluid', 'viettitan') => 'container-fluid',
            ),
        ),
        array(
            'type' => 'dropdown',
            'heading' => esc_html__('Columns gap', 'js_composer'),
            'param_name' => 'gap',
            'value' => array(
                '0px' => '0',
                '1px' => '1',
                '2px' => '2',
                '3px' => '3',
                '4px' => '4',
                '5px' => '5',
                '10px' => '10',
                '15px' => '15',
                '20px' => '20',
                '25px' => '25',
                '30px' => '30',
                '35px' => '35',
            ),
            'std' => '0',
            'description' => esc_html__('Select gap between columns in row.', 'js_composer'),
        ),
        array(
            'type' => 'checkbox',
            'heading' => esc_html__('Full height row?', 'js_composer'),
            'param_name' => 'full_height',
            'description' => esc_html__('If checked row will be set to full height.', 'js_composer'),
            'value' => array(esc_html__('Yes', 'js_composer') => 'yes'),
        ),
        array(
            'type' => 'dropdown',
            'heading' => esc_html__('Columns position', 'js_composer'),
            'param_name' => 'columns_placement',
            'value' => array(
                esc_html__('Middle', 'js_composer') => 'middle',
                esc_html__('Top', 'js_composer') => 'top',
                esc_html__('Bottom', 'js_composer') => 'bottom',
                esc_html__('Stretch', 'js_composer') => 'stretch',
            ),
            'description' => esc_html__('Select columns position within row.', 'js_composer'),
            'dependency' => array(
                'element' => 'full_height',
                'not_empty' => true,
            ),
        ),
        array(
            'type' => 'checkbox',
            'heading' => esc_html__('Equal height', 'js_composer'),
            'param_name' => 'equal_height',
            'description' => esc_html__('If checked columns will be set to equal height.', 'js_composer'),
            'value' => array(esc_html__('Yes', 'js_composer') => 'yes')
        ),
        array(
            'type' => 'dropdown',
            'heading' => esc_html__('Content position', 'js_composer'),
            'param_name' => 'content_placement',
            'value' => array(
                esc_html__('Default', 'js_composer') => '',
                esc_html__('Top', 'js_composer') => 'top',
                esc_html__('Middle', 'js_composer') => 'middle',
                esc_html__('Bottom', 'js_composer') => 'bottom',
            ),
            'description' => esc_html__('Select content position within columns.', 'js_composer'),
        ),
        array(
            'type' => 'checkbox',
            'heading' => esc_html__('Use video background?', 'js_composer'),
            'param_name' => 'video_bg',
            'description' => esc_html__('If checked, video will be used as row background.', 'js_composer'),
            'value' => array(esc_html__('Yes', 'js_composer') => 'yes'),
        ),
        array(
            'type' => 'textfield',
            'heading' => esc_html__('YouTube link', 'js_composer'),
            'param_name' => 'video_bg_url',
            'value' => 'https://www.youtube.com/watch?v=lMJXxhRFO1k',
            // default video url
            'description' => esc_html__('Add YouTube link.', 'js_composer'),
            'dependency' => array(
                'element' => 'video_bg',
                'not_empty' => true,
            ),
        ),
        array(
            'type' => 'dropdown',
            'heading' => esc_html__('Parallax', 'js_composer'),
            'param_name' => 'video_bg_parallax',
            'value' => array(
                esc_html__('None', 'js_composer') => '',
                esc_html__('Simple', 'js_composer') => 'content-moving',
                esc_html__('With fade', 'js_composer') => 'content-moving-fade',
            ),
            'description' => esc_html__('Add parallax type background for row.', 'js_composer'),
            'dependency' => array(
                'element' => 'video_bg',
                'not_empty' => true,
            ),
        ),
        array(
            'type' => 'dropdown',
            'heading' => esc_html__('Parallax', 'js_composer'),
            'param_name' => 'parallax',
            'value' => array(
                esc_html__('None', 'js_composer') => '',
                esc_html__('Simple', 'js_composer') => 'content-moving',
                esc_html__('With fade', 'js_composer') => 'content-moving-fade',
            ),
            'description' => esc_html__('Add parallax type background for row (Note: If no image is specified, parallax will use background image from Design Options).', 'js_composer'),
            'dependency' => array(
                'element' => 'video_bg',
                'is_empty' => true,
            ),
        ),
        array(
            'type' => 'attach_image',
            'heading' => esc_html__('Image', 'js_composer'),
            'param_name' => 'parallax_image',
            'value' => '',
            'description' => esc_html__('Select image from media library.', 'js_composer'),
            'dependency' => array(
                'element' => 'parallax',
                'not_empty' => true,
            ),
        ),
        array(
            'type' => 'textfield',
            'heading' => esc_html__('Parallax speed', 'viettitan'),
            'param_name' => 'parallax_speed',
            'value' => '1.5',
            'dependency' => Array('element' => 'parallax', 'value' => array('content-moving', 'content-moving-fade')),
        ),
        array(
            'type' => 'dropdown',
            'heading' => esc_html__('Show background overlay', 'viettitan'),
            'param_name' => 'overlay_set',
            'description' => esc_html__('Hide or Show overlay on background images.', 'viettitan'),
            'value' => array(
                esc_html__('Hide, please', 'viettitan') => 'hide_overlay',
                esc_html__('Show Overlay Color', 'viettitan') => 'show_overlay_color',
                esc_html__('Show Overlay Image', 'viettitan') => 'show_overlay_image',
            )
        ),
        array(
            'type' => 'attach_image',
            'heading' => esc_html__('Image Overlay:', 'viettitan'),
            'param_name' => 'overlay_image',
            'value' => '',
            'description' => esc_html__('Upload image overlay.', 'viettitan'),
            'dependency' => Array('element' => 'overlay_set', 'value' => array('show_overlay_image')),
        ),
        array(
            'type' => 'colorpicker',
            'heading' => esc_html__('Overlay color', 'viettitan'),
            'param_name' => 'overlay_color',
            'description' => esc_html__('Select color for background overlay.', 'viettitan'),
            'value' => '',
            'dependency' => Array('element' => 'overlay_set', 'value' => array('show_overlay_color')),
        ),
        array(
            'type' => 'number',
            'class' => '',
            'heading' => esc_html__('Overlay opacity', 'viettitan'),
            'param_name' => 'overlay_opacity',
            'value' => '50',
            'min' => '1',
            'max' => '100',
            'suffix' => '%',
            'description' => esc_html__('Select opacity for overlay.', 'viettitan'),
            'dependency' => Array('element' => 'overlay_set', 'value' => array('show_overlay_color', 'show_overlay_image')),
        ),
        array(
            'type' => 'el_id',
            'heading' => esc_html__('Row ID', 'js_composer'),
            'param_name' => 'el_id',
            'description' => sprintf(esc_html__('Enter row ID (Note: make sure it is unique and valid according to <a href="%s" target="_blank">w3c specification</a>).', 'js_composer'), 'http://www.w3schools.com/tags/att_global_id.asp'),
        ),
        array(
            'type' => 'textfield',
            'heading' => esc_html__('Extra class name', 'js_composer'),
            'param_name' => 'el_class',
            'description' => esc_html__('Style particular content element differently - add a class name and refer to it in custom CSS.', 'js_composer'),
        ),
        array(
            'type' => 'css_editor',
            'heading' => esc_html__('CSS box', 'js_composer'),
            'param_name' => 'css',
            'group' => esc_html__('Design Options', 'js_composer'),
        ),
        $add_css_animation,
        $add_duration_animation,
        $add_delay_animation,
    );
    vc_map(array(
        'name' => esc_html__('Row', 'viettitan'),
        'base' => 'vc_row',
        'is_container' => true,
        'icon' => 'icon-wpb-row',
        'show_settings_on_create' => false,
        'category' => esc_html__('Content', 'viettitan'),
        'description' => esc_html__('Place content elements inside the row', 'viettitan'),
        'params' => $params_row,
        'js_view' => 'VcRowView'
    ));
    vc_map(array(
        'name' => esc_html__('Row', 'viettitan'), //Inner Row
        'base' => 'vc_row_inner',
        'content_element' => false,
        'is_container' => true,
        'icon' => 'icon-wpb-row',
        'weight' => 1000,
        'show_settings_on_create' => false,
        'description' => esc_html__('Place content elements inside the row', 'viettitan'),
        'params' => $params_row,
        'js_view' => 'VcRowView'
    ));
    $params_icon = array(
        array(
            'type' => 'iconpicker',
            'heading' => esc_html__('Icon', 'viettitan'),
            'param_name' => 'i_icon_viettitan',
            'value' => 'icon-adjustments', // default value to backend editor admin_label
            'settings' => array(
                'emptyIcon' => false, // default true, display an "EMPTY" icon?
                'iconsPerPage' => 4000,
                'type' => 'viettitan',
                'source' => $viettitan_icons,
            ),
            'dependency' => array(
                'element' => 'i_type',
                'value' => 'viettitan',
            ),
            'description' => esc_html__('Select icon from library.', 'viettitan'),
        ),
        array(
            'type' => 'iconpicker',
            'heading' => esc_html__('Icon', 'viettitan'),
            'param_name' => 'i_icon_fontawesome',
            'value' => 'fa fa-adjust', // default value to backend editor admin_label
            'settings' => array(
                'emptyIcon' => false,
                // default true, display an "EMPTY" icon?
                'iconsPerPage' => 4000,
                'source' => $viettitan_font_awesome,
                // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
            ),
            'dependency' => array(
                'element' => 'i_type',
                'value' => 'fontawesome',
            ),
            'description' => esc_html__('Select icon from library.', 'viettitan'),
        ),
        array(
            'type' => 'iconpicker',
            'heading' => esc_html__('Icon', 'viettitan'),
            'param_name' => 'i_icon_openiconic',
            'value' => 'vc-oi vc-oi-dial', // default value to backend editor admin_label
            'settings' => array(
                'emptyIcon' => false, // default true, display an "EMPTY" icon?
                'type' => 'openiconic',
                'iconsPerPage' => 4000, // default 100, how many icons per/page to display
            ),
            'dependency' => array(
                'element' => 'i_type',
                'value' => 'openiconic',
            ),
            'description' => esc_html__('Select icon from library.', 'viettitan'),
        ),
        array(
            'type' => 'iconpicker',
            'heading' => esc_html__('Icon', 'viettitan'),
            'param_name' => 'i_icon_typicons',
            'value' => 'typcn typcn-adjust-brightness', // default value to backend editor admin_label
            'settings' => array(
                'emptyIcon' => false, // default true, display an "EMPTY" icon?
                'type' => 'typicons',
                'iconsPerPage' => 4000, // default 100, how many icons per/page to display
            ),
            'dependency' => array(
                'element' => 'i_type',
                'value' => 'typicons',
            ),
            'description' => esc_html__('Select icon from library.', 'viettitan'),
        ),
        array(
            'type' => 'iconpicker',
            'heading' => esc_html__('Icon', 'viettitan'),
            'param_name' => 'i_icon_entypo',
            'value' => 'entypo-icon entypo-icon-note', // default value to backend editor admin_label
            'settings' => array(
                'emptyIcon' => false, // default true, display an "EMPTY" icon?
                'type' => 'entypo',
                'iconsPerPage' => 4000, // default 100, how many icons per/page to display
            ),
            'dependency' => array(
                'element' => 'i_type',
                'value' => 'entypo',
            ),
        ),
        array(
            'type' => 'iconpicker',
            'heading' => esc_html__('Icon', 'viettitan'),
            'param_name' => 'i_icon_linecons',
            'value' => 'vc_li vc_li-heart', // default value to backend editor admin_label
            'settings' => array(
                'emptyIcon' => false, // default true, display an "EMPTY" icon?
                'type' => 'linecons',
                'iconsPerPage' => 4000, // default 100, how many icons per/page to display
            ),
            'dependency' => array(
                'element' => 'i_type',
                'value' => 'linecons',
            ),
            'description' => esc_html__('Select icon from library.', 'viettitan'),
        ),
    );
    $params_section = array_merge(
        array(
            array(
                'type' => 'textfield',
                'param_name' => 'title',
                'heading' => esc_html__('Title', 'viettitan'),
                'description' => esc_html__('Enter section title (Note: you can leave it empty).', 'viettitan'),
            ),
            array(
                'type' => 'el_id',
                'param_name' => 'tab_id',
                'settings' => array(
                    'auto_generate' => true,
                ),
                'heading' => esc_html__('Section ID', 'viettitan'),
                'description' => esc_html__('Enter section ID (Note: make sure it is unique and valid according to <a href="%s" target="_blank">w3c specification</a>).', 'viettitan'),
            ),
            array(
                'type' => 'checkbox',
                'param_name' => 'add_icon',
                'heading' => esc_html__('Add icon?', 'viettitan'),
                'description' => esc_html__('Add icon next to section title.', 'viettitan'),
            ),
            array(
                'type' => 'dropdown',
                'param_name' => 'i_position',
                'value' => array(
                    esc_html__('Before title', 'viettitan') => 'left',
                    esc_html__('After title', 'viettitan') => 'right',
                ),
                'dependency' => array(
                    'element' => 'add_icon',
                    'value' => 'true',
                ),
                'heading' => esc_html__('Icon position', 'viettitan'),
                'description' => esc_html__('Select icon position.', 'viettitan'),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__('Icon library', 'viettitan'),
                'value' => array(
                    esc_html__('Viettitan Icon', 'viettitan') => 'viettitan',
                    esc_html__('Font Awesome', 'viettitan') => 'fontawesome',
                    esc_html__('Open Iconic', 'viettitan') => 'openiconic',
                    esc_html__('Typicons', 'viettitan') => 'typicons',
                    esc_html__('Entypo', 'viettitan') => 'entypo',
                    esc_html__('Linecons', 'viettitan') => 'linecons',
                ),
                'admin_label' => true,
                'param_name' => 'i_type',
                'description' => esc_html__('Select icon library.', 'viettitan'),
                'dependency' => array(
                    'element' => 'add_icon',
                    'value' => 'true',
                ),
            ),

        ),
        $params_icon,
        array(
            array(
                'type' => 'textfield',
                'heading' => esc_html__('Extra class name', 'viettitan'),
                'param_name' => 'el_class',
                'description' => esc_html__('If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'viettitan')
            )
        )
    );
    vc_map(array(
        'name' => esc_html__('Section', 'viettitan'),
        'base' => 'vc_tta_section',
        'icon' => 'icon-wpb-ui-tta-section',
        'allowed_container_element' => 'vc_row',
        'is_container' => true,
        'show_settings_on_create' => false,
        'as_child' => array(
            'only' => 'vc_tta_tour,vc_tta_tabs,vc_tta_accordion',
        ),
        'category' => esc_html__('Content', 'viettitan'),
        'description' => esc_html__('Section for Tabs, Tours, Accordions.', 'viettitan'),
        'params' => $params_section,
        'js_view' => 'VcBackendTtaSectionView',
        'custom_markup' => '
		<div class="vc_tta-panel-heading">
		    <h4 class="vc_tta-panel-title vc_tta-controls-icon-position-left"><a href="javascript:;" data-vc-target="[data-model-id=\'{{ model_id }}\']" data-vc-accordion data-vc-container=".vc_tta-container"><span class="vc_tta-title-text">{{ section_title }}</span><i class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a></h4>
		</div>
		<div class="vc_tta-panel-body">
			{{ editor_controls }}
			<div class="{{ container-class }}">
			{{ content }}
			</div>
		</div>',
        'default_content' => '',
    ));
    /**
     * Pie chart
     */
    $params_piechart = array_merge(
        array(
            array(
                'type' => 'dropdown',
                'heading' => esc_html__('Layout Style', 'viettitan'),
                'param_name' => 'layout_style',
                'admin_label' => true,
                'value' => array(esc_html__('Normal', 'viettitan') => 'pie_text', esc_html__('Pie Icon', 'viettitan') => 'pie_icon'),
                'description' => esc_html__('Select Layout Style.', 'viettitan'),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__('Value', 'viettitan'),
                'param_name' => 'value',
                'description' => esc_html__('Enter value for graph (Note: choose range from 0 to 100).', 'viettitan'),
                'value' => '50',
                'admin_label' => true
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__('Label value', 'viettitan'),
                'param_name' => 'label_value',
                'description' => esc_html__('Enter label for pie chart (Note: leaving empty will set value from "Value" field).', 'viettitan'),
                'value' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__('Units', 'viettitan'),
                'param_name' => 'units',
                'description' => esc_html__('Enter measurement units (Example: %, px, points, etc. Note: graph value and units will be appended to graph title).', 'viettitan')
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__('Icon library', 'viettitan'),
                'value' => array(
                    esc_html__('[None]', 'viettitan') => '',
                    esc_html__('Viettitan Icon', 'viettitan') => 'viettitan',
                    esc_html__('Font Awesome', 'viettitan') => 'fontawesome',
                    esc_html__('Open Iconic', 'viettitan') => 'openiconic',
                    esc_html__('Typicons', 'viettitan') => 'typicons',
                    esc_html__('Entypo', 'viettitan') => 'entypo',
                    esc_html__('Linecons', 'viettitan') => 'linecons',
                    esc_html__('Image', 'viettitan') => 'image',
                ),
                'admin_label' => true,
                'param_name' => 'i_type',
                'description' => esc_html__('Select icon library.', 'viettitan'),
                'dependency' => Array('element' => 'layout_style', 'value' => array('pie_icon')),
            ),
        ),
        $params_icon,
        array(
            array(
                'type' => 'attach_image',
                'heading' => esc_html__('Upload Image Icon:', 'viettitan'),
                'param_name' => 'i_icon_image',
                'value' => '',
                'description' => esc_html__('Upload the custom image icon.', 'viettitan'),
                'dependency' => Array('element' => 'i_type', 'value' => array('image')),
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__('Text value / Icon color', 'viettitan'),
                'param_name' => 'value_color',
                'description' => esc_html__('Select value/icon color.', 'viettitan'),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__('Bar color', 'viettitan'),
                'param_name' => 'bar_color',
                'value' => array(esc_html__('Primary color', 'viettitan') => 'primary-color') + array(esc_html__('Secondary color', 'viettitan') => 'secondary-color') + getVcShared('colors-dashed') + array(esc_html__('Custom', 'viettitan') => 'custom'),
                'description' => esc_html__('Select pie chart color.', 'viettitan'),
                'param_holder_class' => 'vc_colored-dropdown',
                'std' => 'secondary-color'
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__('Custom color', 'viettitan'),
                'param_name' => 'bar_custom_color',
                'description' => esc_html__('Select custom bar color.', 'viettitan'),
                'dependency' => array(
                    'element' => 'bar_color',
                    'value' => array('custom')
                ),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__('Bar value color', 'viettitan'),
                'param_name' => 'color',
                'value' => array(esc_html__('Primary color', 'viettitan') => 'primary-color') + array(esc_html__('Secondary color', 'viettitan') => 'secondary-color') + getVcShared('colors-dashed') + array(esc_html__('Custom', 'viettitan') => 'custom'),
                'description' => esc_html__('Select pie chart color.', 'viettitan'),
                'param_holder_class' => 'vc_colored-dropdown',
                'std' => 'primary-color'
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__('Custom color', 'viettitan'),
                'param_name' => 'custom_color',
                'description' => esc_html__('Select custom bar value color.', 'viettitan'),
                'dependency' => array(
                    'element' => 'color',
                    'value' => array('custom')
                ),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__('Extra class name', 'viettitan'),
                'param_name' => 'el_class',
                'description' => esc_html__('Style particular content element differently - add a class name and refer to it in custom CSS.', 'viettitan')
            ),
            array(
                'type' => 'css_editor',
                'heading' => esc_html__('CSS box', 'viettitan'),
                'param_name' => 'css',
                'group' => esc_html__('Design Options', 'viettitan')
            ),
        )
    );
    vc_map(array(
        'name' => esc_html__('Pie Chart', 'viettitan'),
        'base' => 'vc_pie',
        'class' => '',
        'icon' => 'icon-wpb-vc_pie',
        'category' => array(esc_html__('Content', 'viettitan'), esc_html__('Viettitan Shortcodes', 'viettitan')),
        'description' => esc_html__('Animated pie chart', 'viettitan'),
        'params' => $params_piechart,
    ));

    $custom_colors = array(
        esc_html__('Informational', 'viettitan') => 'info',
        esc_html__('Warning', 'viettitan') => 'warning',
        esc_html__('Success', 'viettitan') => 'success',
        esc_html__('Error', 'viettitan') => "danger",
        esc_html__('Informational Classic', 'viettitan') => 'alert-info',
        esc_html__('Warning Classic', 'viettitan') => 'alert-warning',
        esc_html__('Success Classic', 'viettitan') => 'alert-success',
        esc_html__('Error Classic', 'viettitan') => "alert-danger",
        esc_html__('Viettitan Informational', 'viettitan') => "m-info",
        esc_html__('Viettitan Warning', 'viettitan') => "m-warning",
        esc_html__('Viettitan Success', 'viettitan') => "m-success",
        esc_html__('Viettitan Error', 'viettitan') => "m-danger",
        esc_html__('Viettitan Notice', 'viettitan') => "m-notice",
    );
    vc_map( array(
        'name' => esc_html__( 'Message Box', 'viettitan' ),
        'base' => 'vc_message',
        'icon' => 'icon-wpb-information-white',
        'category' => array(esc_html__('Content', 'viettitan'), esc_html__('Viettitan Shortcodes', 'viettitan')),
        'description' => esc_html__( 'Notification box', 'viettitan' ),
        'params' => array(
            array(
                'type' => 'params_preset',
                'heading' => esc_html__( 'Message Box Presets', 'viettitan' ),
                'param_name' => 'color',
                // due to backward compatibility, really it is message_box_type
                'value' => '',
                'options' => array(
                    array(
                        'label' => esc_html__( 'Custom', 'viettitan' ),
                        'value' => '',
                        'params' => array(),
                    ),
                    array(
                        'label' => esc_html__( 'Informational', 'viettitan' ),
                        'value' => 'info',
                        'params' => array(
                            'message_box_color' => 'info',
                            'icon_type' => 'fontawesome',
                            'icon_fontawesome' => 'fa fa-info-circle',
                        ),
                    ),
                    array(
                        'label' => esc_html__( 'Warning', 'viettitan' ),
                        'value' => 'warning',
                        'params' => array(
                            'message_box_color' => 'warning',
                            'icon_type' => 'fontawesome',
                            'icon_fontawesome' => 'fa fa-exclamation-triangle',
                        ),
                    ),
                    array(
                        'label' => esc_html__( 'Success', 'viettitan' ),
                        'value' => 'success',
                        'params' => array(
                            'message_box_color' => 'success',
                            'icon_type' => 'fontawesome',
                            'icon_fontawesome' => 'fa fa-check',
                        ),
                    ),
                    array(
                        'label' => esc_html__( 'Error', 'viettitan' ),
                        'value' => 'danger',
                        'params' => array(
                            'message_box_color' => 'danger',
                            'icon_type' => 'fontawesome',
                            'icon_fontawesome' => 'fa fa-times',
                        ),
                    ),
                    array(
                        'label' => esc_html__( 'Informational Classic', 'viettitan' ),
                        'value' => 'alert-info', // due to backward compatibility
                        'params' => array(
                            'message_box_color' => 'alert-info',
                            'icon_type' => 'pixelicons',
                            'icon_pixelicons' => 'vc_pixel_icon vc_pixel_icon-info',
                        ),
                    ),
                    array(
                        'label' => esc_html__( 'Warning Classic', 'viettitan' ),
                        'value' => 'alert-warning', // due to backward compatibility
                        'params' => array(
                            'message_box_color' => 'alert-warning',
                            'icon_type' => 'pixelicons',
                            'icon_pixelicons' => 'vc_pixel_icon vc_pixel_icon-alert',
                        ),
                    ),
                    array(
                        'label' => esc_html__( 'Success Classic', 'viettitan' ),
                        'value' => 'alert-success',
                        // due to backward compatibility
                        'params' => array(
                            'message_box_color' => 'alert-success',
                            'icon_type' => 'pixelicons',
                            'icon_pixelicons' => 'vc_pixel_icon vc_pixel_icon-tick',
                        ),
                    ),
                    array(
                        'label' => esc_html__( 'Error Classic', 'viettitan' ),
                        'value' => 'alert-danger',  // due to backward compatibility
                        'params' => array(
                            'message_box_color' => 'alert-danger',
                            'icon_type' => 'pixelicons',
                            'icon_pixelicons' => 'vc_pixel_icon vc_pixel_icon-explanation',
                        ),
                    ),
                ),
                'description' => esc_html__( 'Select predefined message box design or choose "Custom" for custom styling.', 'viettitan' ),
                'param_holder_class' => 'vc_message-type vc_colored-dropdown',
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Style', 'viettitan' ),
                'param_name' => 'message_box_style',
                'value' => getVcShared( 'message_box_styles' ),
                'description' => esc_html__( 'Select message box design style.', 'viettitan' ),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Shape', 'viettitan' ),
                'param_name' => 'style',
                // due to backward compatibility message_box_shape
                'std' => 'rounded',
                'value' => array(
                    esc_html__( 'Square', 'viettitan' ) => 'square',
                    esc_html__( 'Rounded', 'viettitan' ) => 'rounded',
                    esc_html__( 'Round', 'viettitan' ) => 'round',
                ),
                'description' => esc_html__( 'Select message box shape.', 'viettitan' ),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Color', 'viettitan' ),
                'param_name' => 'message_box_color',
                'value' => $custom_colors + getVcShared( 'colors' ),
                'description' => esc_html__( 'Select message box color.', 'viettitan' ),
                'param_holder_class' => 'vc_message-type vc_colored-dropdown',
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__('Icon library', 'viettitan'),
                'value' => array(
                    esc_html__('Viettitan Icon', 'viettitan') => 'viettitan',
                    esc_html__('Font Awesome', 'viettitan') => 'fontawesome',
                    esc_html__('Open Iconic', 'viettitan') => 'openiconic',
                    esc_html__('Typicons', 'viettitan') => 'typicons',
                    esc_html__('Entypo', 'viettitan') => 'entypo',
                    esc_html__('Linecons', 'viettitan') => 'linecons',
                    esc_html__('Pixel', 'viettitan') => 'pixelicons',
                ),
                'param_name' => 'icon_type',
                'description' => esc_html__('Select icon library.', 'viettitan'),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => esc_html__('Icon', 'viettitan'),
                'param_name' => 'icon_viettitan',
                'settings' => array(
                    'emptyIcon' => false, // default true, display an "EMPTY" icon?
                    'iconsPerPage' => 4000,
                    'type' => 'viettitan',
                    'source' => $viettitan_icons,
                ),
                'dependency' => array(
                    'element' => 'icon_type',
                    'value' => 'viettitan',
                ),
                'description' => esc_html__('Select icon from library.', 'viettitan'),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => esc_html__( 'Icon', 'viettitan' ),
                'param_name' => 'icon_fontawesome',
                'value' => 'fa fa-info-circle',
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'iconsPerPage' => 4000,
                    'source' => $viettitan_font_awesome,
                    // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'icon_type',
                    'value' => 'fontawesome',
                ),
                'description' => esc_html__( 'Select icon from library.', 'viettitan' ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => esc_html__( 'Icon', 'viettitan' ),
                'param_name' => 'icon_openiconic',
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'type' => 'openiconic',
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'icon_type',
                    'value' => 'openiconic',
                ),
                'description' => esc_html__( 'Select icon from library.', 'viettitan' ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => esc_html__( 'Icon', 'viettitan' ),
                'param_name' => 'icon_typicons',
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'type' => 'typicons',
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'icon_type',
                    'value' => 'typicons',
                ),
                'description' => esc_html__( 'Select icon from library.', 'viettitan' ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => esc_html__( 'Icon', 'viettitan' ),
                'param_name' => 'icon_entypo',
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'type' => 'entypo',
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'icon_type',
                    'value' => 'entypo',
                ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => esc_html__( 'Icon', 'viettitan' ),
                'param_name' => 'icon_linecons',
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'type' => 'linecons',
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'icon_type',
                    'value' => 'linecons',
                ),
                'description' => esc_html__( 'Select icon from library.', 'viettitan' ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => esc_html__( 'Icon', 'viettitan' ),
                'param_name' => 'icon_pixelicons',
                'settings' => array(
                    'emptyIcon' => false, // default true, display an "EMPTY" icon?
                    'type' => 'pixelicons',
                    'source' => $pixel_icons,
                ),
                'dependency' => array(
                    'element' => 'icon_type',
                    'value' => 'pixelicons',
                ),
                'description' => esc_html__( 'Select icon from library.', 'viettitan' ),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Icon Size', 'viettitan' ),
                'param_name' => 'icon_size', // due to backward compatibility message_box_shape
                'std' => 'small',
                'value' => array(
                    esc_html__( 'Small', 'viettitan' ) => 'icon_small',
                    esc_html__( 'Large', 'viettitan' ) => 'icon_large',
                ),
            ),
            array(
                'type' => 'textarea_html',
                'holder' => 'div',
                'class' => 'messagebox_text',
                'heading' => esc_html__( 'Message text', 'viettitan' ),
                'param_name' => 'content',
                'value' => esc_html__( '<p>I am message box. Click edit button to change this text.</p>', 'viettitan' ),
            ),
            $add_css_animation,
            $add_duration_animation,
            $add_delay_animation,
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Extra class name', 'viettitan' ),
                'param_name' => 'el_class',
                'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'viettitan' ),
            ),
            array(
                'type' => 'css_editor',
                'heading' => esc_html__( 'CSS box', 'viettitan' ),
                'param_name' => 'css',
                'group' => esc_html__( 'Design Options', 'viettitan' ),
            ),
        ),
        'js_view' => 'VcMessageView_Backend',
    ) );
    vc_map(array(
        'name' => esc_html__('Progress Bar', 'viettitan'),
        'base' => 'vc_progress_bar',
        'icon' => 'icon-wpb-graph',
        'category' => array(esc_html__('Content', 'viettitan'), esc_html__('Viettitan Shortcodes', 'viettitan')),
        'description' => esc_html__('Animated progress bar', 'viettitan'),
        'params' => array(
            array(
                'type' => 'dropdown',
                'heading' => esc_html__('Layout Style', 'viettitan'),
                'param_name' => 'layout_style',
                'admin_label' => true,
                'value' => array(esc_html__('Text left', 'viettitan') => 'style1', esc_html__('Text inner bar', 'viettitan') => 'style2', esc_html__('Text move', 'viettitan') => 'style3'),
                'description' => esc_html__('Select Layout Style.', 'viettitan')
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__('Widget title', 'viettitan'),
                'param_name' => 'title',
                'description' => esc_html__('Enter text used as widget title (Note: located above content element).', 'viettitan')
            ),
            array(
                'type' => 'param_group',
                'heading' => esc_html__('Values', 'viettitan'),
                'param_name' => 'values',
                'description' => esc_html__('Enter values for graph - value, title and color.', 'viettitan'),
                'value' => urlencode(json_encode(array(
                    array(
                        'label' => esc_html__('Development', 'viettitan'),
                        'value' => '90',
                    ),
                    array(
                        'label' => esc_html__('Design', 'viettitan'),
                        'value' => '80',
                    ),
                    array(
                        'label' => esc_html__('Marketing', 'viettitan'),
                        'value' => '70',
                    ),
                ))),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Label', 'viettitan'),
                        'param_name' => 'label',
                        'description' => esc_html__('Enter text used as title of bar.', 'viettitan'),
                        'admin_label' => true,
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Value', 'viettitan'),
                        'param_name' => 'value',
                        'description' => esc_html__('Enter value of bar.', 'viettitan'),
                        'admin_label' => true,
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__('Color', 'viettitan'),
                        'param_name' => 'color',
                        'value' => array(
                                esc_html__('Default', 'viettitan') => ''
                            ) + array(
                                esc_html__('Primary color', 'viettitan') => 'primary-color',
                                esc_html__('Secondary color', 'viettitan') => 'secondary-color',
                                esc_html__('Classic Grey', 'viettitan') => 'bar_grey',
                                esc_html__('Classic Blue', 'viettitan') => 'bar_blue',
                                esc_html__('Classic Turquoise', 'viettitan') => 'bar_turquoise',
                                esc_html__('Classic Green', 'viettitan') => 'bar_green',
                                esc_html__('Classic Orange', 'viettitan') => 'bar_orange',
                                esc_html__('Classic Red', 'viettitan') => 'bar_red',
                                esc_html__('Classic Black', 'viettitan') => 'bar_black',
                            ) + getVcShared('colors-dashed') + array(
                                esc_html__('Custom Color', 'viettitan') => 'custom'
                            ),
                        'description' => esc_html__('Select single bar background color.', 'viettitan'),
                        'admin_label' => true,
                        'param_holder_class' => 'vc_colored-dropdown'
                    ),
                    array(
                        'type' => 'colorpicker',
                        'heading' => esc_html__('Custom color', 'viettitan'),
                        'param_name' => 'customcolor',
                        'description' => esc_html__('Select custom single bar value background color.', 'viettitan'),
                        'dependency' => array(
                            'element' => 'color',
                            'value' => array('custom')
                        ),
                    ),
                    array(
                        'type' => 'colorpicker',
                        'heading' => esc_html__('Custom bar color', 'viettitan'),
                        'param_name' => 'custombarcolor',
                        'description' => esc_html__('Select custom single bar background color.', 'viettitan'),
                        'dependency' => array(
                            'element' => 'color',
                            'value' => array('custom')
                        ),
                    ),
                    array(
                        'type' => 'colorpicker',
                        'heading' => esc_html__('Custom label text color', 'viettitan'),
                        'param_name' => 'customtxtcolor',
                        'description' => esc_html__('Select custom single bar label text color.', 'viettitan'),
                        'dependency' => array(
                            'element' => 'color',
                            'value' => array('custom')
                        ),
                    ),
                    array(
                        'type' => 'colorpicker',
                        'heading' => esc_html__('Custom value text color', 'viettitan'),
                        'param_name' => 'customvaluetxtcolor',
                        'description' => esc_html__('Select custom single bar value text color.', 'viettitan'),
                        'dependency' => array(
                            'element' => 'color',
                            'value' => array('custom')
                        ),
                    ),
                ),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__('Units', 'viettitan'),
                'param_name' => 'units',
                'description' => esc_html__('Enter measurement units (Example: %, px, points, etc. Note: graph value and units will be appended to graph title).', 'viettitan')
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__('Color', 'viettitan'),
                'param_name' => 'bgcolor',
                'value' => array(
                        esc_html__('Primary color', 'viettitan') => 'primary-color',
                        esc_html__('Secondary color', 'viettitan') => 'secondary-color',
                        esc_html__('Classic Grey', 'viettitan') => 'bar_grey',
                        esc_html__('Classic Blue', 'viettitan') => 'bar_blue',
                        esc_html__('Classic Turquoise', 'viettitan') => 'bar_turquoise',
                        esc_html__('Classic Green', 'viettitan') => 'bar_green',
                        esc_html__('Classic Orange', 'viettitan') => 'bar_orange',
                        esc_html__('Classic Red', 'viettitan') => 'bar_red',
                        esc_html__('Classic Black', 'viettitan') => 'bar_black',
                    ) + getVcShared('colors-dashed') + array(
                        esc_html__('Custom Color', 'viettitan') => 'custom'
                    ),
                'std' => 'viettitan1',
                'description' => esc_html__('Select bar background color.', 'viettitan'),
                'admin_label' => true,
                'param_holder_class' => 'vc_colored-dropdown',
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__('Bar value custom background color', 'viettitan'),
                'param_name' => 'custombgcolor',
                'description' => esc_html__('Select custom background color for bars value.', 'viettitan'),
                'dependency' => array('element' => 'bgcolor', 'value' => array('custom'))
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__('Bar custom background color', 'viettitan'),
                'param_name' => 'custombgbarcolor',
                'description' => esc_html__('Select custom background color for bars.', 'viettitan'),
                'dependency' => array('element' => 'bgcolor', 'value' => array('custom'))
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__('Bar custom label text color', 'viettitan'),
                'param_name' => 'customtxtcolor',
                'description' => esc_html__('Select custom label text color for bars.', 'viettitan'),
                'dependency' => array('element' => 'bgcolor', 'value' => array('custom'))
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__('Bar custom value text color', 'viettitan'),
                'param_name' => 'customvaluetxtcolor',
                'description' => esc_html__('Select custom value text color for bars.', 'viettitan'),
                'dependency' => array('element' => 'bgcolor', 'value' => array('custom'))
            ),
            array(
                'type' => 'checkbox',
                'heading' => esc_html__('Options', 'viettitan'),
                'param_name' => 'options',
                'value' => array(
                    esc_html__('Add stripes', 'viettitan') => 'striped',
                    esc_html__('Add animation (Note: visible only with striped bar).', 'viettitan') => 'animated'
                )
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__('Extra class name', 'viettitan'),
                'param_name' => 'el_class',
                'description' => esc_html__('Style particular content element differently - add a class name and refer to it in custom CSS.', 'viettitan')
            ),
            array(
                'type' => 'css_editor',
                'heading' => esc_html__('CSS box', 'viettitan'),
                'param_name' => 'css',
                'group' => esc_html__('Design Options', 'viettitan')
            ),
        )
    ));
}

add_action('vc_after_init', 'viettitan_register_vc_map');