<?php
	$viettitan_options = &Viettitan_Global::get_options();
	$prefix = 'viettitan_';
	$header_responsive = isset($viettitan_options['mobile_header_responsive_breakpoint']) && !empty($viettitan_options['mobile_header_responsive_breakpoint'])
						 ? $viettitan_options['mobile_header_responsive_breakpoint'] : '991';

	$header_layout = rwmb_meta($prefix . 'header_layout');
	if (($header_layout === '') || ($header_layout == '-1')) {
		$header_layout = $viettitan_options['header_layout'];
	}
?>
<!DOCTYPE html>
<!-- Open Html -->
<html <?php language_attributes(); ?>>
	<!-- Open Head -->
	<head>
		<?php wp_head(); ?>
	</head>
	<!-- Close Head -->
	<body <?php body_class(); ?> data-responsive="<?php echo esc_attr($header_responsive)?>" data-header="<?php echo esc_attr($header_layout) ?>">
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5&appId=585298754954760";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

		<?php
			/**
			 * @hooked - viettitan_site_loading - 5
			 **/
			do_action('viettitan_before_page_wrapper');
		?>
		<!-- Open Wrapper -->
		<div id="wrapper">

		<?php
		/**
		 * @hooked - viettitan_before_page_wrapper_content - 10
		 * @hooked - viettitan_page_header - 15
		 **/
		do_action('viettitan_before_page_wrapper_content');
		?>

			<!-- Open Wrapper Content -->
			<div id="wrapper-content" class="clearfix">

			<?php
			/**
			 **/
			do_action('viettitan_main_wrapper_content_start');
			?>
