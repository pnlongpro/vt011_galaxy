<?php
/**
 * Product Loop End
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
$viettitan_woocommerce_loop = &Viettitan_Global::get_woocommerce_loop();
$archive_product_layout =  isset($viettitan_woocommerce_loop['layout']) ? $viettitan_woocommerce_loop['layout'] : '';
?>
<?php if ($archive_product_layout == 'slider') : ?>
	</div>
<?php endif; ?>
</div>
<?php viettitan_woocommerce_reset_loop(); ?>