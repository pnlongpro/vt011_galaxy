<?php
if (isset($_REQUEST['custom-page']) && !empty($_REQUEST['custom-page'])) {
	if (!Viettitan_Global::is_do_action_custom_page()) {
		do_action('custom-page/'.$_REQUEST['custom-page']);
	}
} else {
	get_header();
	viettitan_get_template( 'archive');
	get_footer();
}