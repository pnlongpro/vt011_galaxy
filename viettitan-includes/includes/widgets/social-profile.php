<?php
/**
 * Created by PhpStorm.
 * User: phuongth
 * Date: 3/26/15
 * Time: 5:24 PM
 */
class Viettitan_Social_Profile extends  Viettitan_Widget {
    public function __construct() {
        $this->widget_cssclass    = 'widget-social-profile';
        $this->widget_description =  esc_html__( "Social profile widget", 'viettitan' );
        $this->widget_id          = 'viettitan-social-profile';
        $this->widget_name        = esc_html__( 'Viettitan: Social Profile', 'viettitan' );
        $this->settings           = array(
            'title' => array(
		        'type' => 'text',
		        'std' => '',
		        'label' => esc_html__('Title','viettitan')
            ),
	        'layout'  => array(
                'type'    => 'select',
                'std'     => 's-default',
                'label'   => esc_html__( 'Layout', 'viettitan' ),
                'options' => array(
                    's-default'  => esc_html__( 'Default', 'viettitan' ),
                    's-rounded'  => esc_html__( 'Rounded', 'viettitan' ),
	                's-icon-text'  => esc_html__( 'Icon And Text', 'viettitan' ),
                )
            ),
	        'scheme'  => array(
		        'type'    => 'select',
		        'std'     => 's-primary',
		        'label'   => esc_html__( 'Scheme', 'viettitan' ),
		        'options' => array(
			        's-scheme-default' => esc_html__( 'Default', 'viettitan' ),
			        's-primary' => esc_html__( 'Primary', 'viettitan' ),
			        's-secondary' => esc_html__( 'Secondary', 'viettitan' ),
			        's-light' => esc_html__( 'Light (#FFF)', 'viettitan' ),
			        's-dark'  => esc_html__( 'Dark (#000)', 'viettitan' ),
			        's-gray'  => esc_html__( 'Gray (#777)', 'viettitan' ),
			        's-light-gray-1'  => esc_html__( 'Light Gray 1 (#CCC)', 'viettitan' ),
			        's-light-gray-2'  => esc_html__( 'Light Gray 2 (#BBB)', 'viettitan' ),
			        's-light-gray-3'  => esc_html__( 'Light Gray 3 (#BABABA)', 'viettitan' ),
			        's-dark-gray-1'  => esc_html__( 'Dark Gray 1 (#444)', 'viettitan' ),
			        's-dark-gray-2'  => esc_html__( 'Dark Gray 2 (#666)', 'viettitan' ),
			        's-dark-gray-3'  => esc_html__( 'Dark Gray 3 (#888)', 'viettitan' ),
		        )
	        ),
	        'size'  => array(
		        'type'    => 'select',
		        'std'     => 's-medium',
		        'label'   => esc_html__( 'Size', 'viettitan' ),
		        'options' => array(
			        's-md'  => esc_html__( 'Medium', 'viettitan' ),
			        's-lg'  => esc_html__( 'Large', 'viettitan' ),
		        )
	        ),

            'icons' => array(
                'type'  => 'multi-select',
                'label'   => esc_html__( 'Select social profiles', 'viettitan' ),
                'std'   => '',
	            'options' => array(
		            'twitter'  => esc_html__( 'Twitter', 'viettitan' ),
		            'facebook'  => esc_html__( 'Facebook', 'viettitan' ),
		            'dribbble'  => esc_html__( 'Dribbble', 'viettitan' ),
		            'vimeo'  => esc_html__( 'Vimeo', 'viettitan' ),
		            'tumblr'  => esc_html__( 'Tumblr', 'viettitan' ),
		            'skype'  => esc_html__( 'Skype', 'viettitan' ),
		            'linkedin'  => esc_html__( 'LinkedIn', 'viettitan' ),
		            'googleplus'  => esc_html__( 'Google+', 'viettitan' ),
		            'flickr'  => esc_html__( 'Flickr', 'viettitan' ),
		            'youtube'  => esc_html__( 'YouTube', 'viettitan' ),
		            'pinterest' => esc_html__( 'Pinterest', 'viettitan' ),
		            'foursquare'  => esc_html__( 'Foursquare', 'viettitan' ),
		            'instagram' => esc_html__( 'Instagram', 'viettitan' ),
		            'github'  => esc_html__( 'GitHub', 'viettitan' ),
		            'xing' => esc_html__( 'Xing', 'viettitan' ),
		            'behance'  => esc_html__( 'Behance', 'viettitan' ),
		            'deviantart'  => esc_html__( 'Deviantart', 'viettitan' ),
		            'soundcloud'  => esc_html__( 'SoundCloud', 'viettitan' ),
		            'yelp'  => esc_html__( 'Yelp', 'viettitan' ),
		            'rss'  => esc_html__( 'RSS Feed', 'viettitan' ),
		            'email'  => esc_html__( 'Email address', 'viettitan' ),
	            )
            )
        );
        if(function_exists('vc_map')){
            add_shortcode('viettitanframework_widget_social_profile', array($this, 'vc_widget'));
        }
        parent::__construct();
    }

    function widget( $args, $instance ) {
        extract( $args, EXTR_SKIP );
	    $title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : '';
        $layout         = ( ! empty( $instance['layout'] ) ) ? $instance['layout'] : 's-default';
	    $scheme        = ( ! empty( $instance['scheme'] ) ) ? $instance['scheme'] : 's-primary';
	    $size        = ( ! empty( $instance['size'] ) ) ? $instance['size'] : 's-md';
	    $icons        = ( ! empty( $instance['icons'] ) ) ? $instance['icons'] : '';

	    $class_wrap = 'social-profile '. $layout . ' ' . $scheme . ' ' .$size;

	    $social_icons = viettitan_get_social_icon($icons,$class_wrap );

	    echo wp_kses_post($args['before_widget']);
		if ($title) {
			echo wp_kses_post($args['before_title'] . $title . $args['after_title']);
		}
	    echo wp_kses_post( $social_icons );
	    echo wp_kses_post($args['after_widget']);
    }

    function vc_widget($atts){
        $attributes = vc_map_get_attributes( 'viettitanframework_widget_social_profile', $atts );
        $args = array();
        $args['widget_id'] = 'viettitan-social-profile';
        $attributes['icons'] = str_replace(',','||',$attributes['icons'] );
        the_widget('Viettitan_Social_Profile',$attributes,$args);
    }
}
if (!function_exists('viettitan_register_social_profile')) {
    function viettitan_register_social_profile() {
        register_widget('Viettitan_Social_Profile');

        if(function_exists('vc_map')){
            vc_map( array(
                'name' => esc_html__( 'Viettitan Social Profile','viettitan' ),
                'base' => 'viettitanframework_widget_social_profile',
                'icon' => 'fa fa-share-square-o',
                'category' => esc_html__( 'Viettitan Widgets', 'viettitan' ),
                'class' => 'wpb_vc_wp_widget',
                'weight' => - 50,
                'description' => esc_html__( 'Social icon for your site', 'viettitan' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__( 'Title', 'viettitan' ),
                        'param_name' => 'title'
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__( 'Layout', 'viettitan' ),
                        'param_name' => 'layout',
                        'value' => array(
                            esc_html__( 'Default', 'viettitan' ) => 's-default'  ,
                            esc_html__( 'Rounded', 'viettitan' ) => 's-rounded'  ,
                            esc_html__( 'Icon And Text', 'viettitan' ) => 's-icon-text'  ,
                        )
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__( 'Scheme', 'viettitan' ),
                        'param_name' => 'scheme',
                        'std' => 's-primary',
                        'value' => array(
                            esc_html__( 'Default', 'viettitan' ) => 's-scheme-default' ,
                            esc_html__( 'Primary', 'viettitan' ) => 's-primary' ,
                            esc_html__( 'Secondary', 'viettitan' ) => 's-secondary' ,
                            esc_html__( 'Light (#FFF)', 'viettitan' ) => 's-light' ,
                            esc_html__( 'Dark (#000)', 'viettitan' ) => 's-dark'  ,
                            esc_html__( 'Gray (#777)', 'viettitan' ) => 's-gray'  ,
                            esc_html__( 'Light Gray 1 (#CCC)', 'viettitan' ) => 's-light-gray-1'  ,
                            esc_html__( 'Light Gray 2 (#BBB)', 'viettitan' ) => 's-light-gray-2'  ,
                            esc_html__( 'Light Gray 3 (#BABABA)', 'viettitan' ) => 's-light-gray-3'  ,
                            esc_html__( 'Dark Gray 1 (#444)', 'viettitan' ) => 's-dark-gray-1'  ,
                            esc_html__( 'Dark Gray 2 (#666)', 'viettitan' ) => 's-dark-gray-2' ,
                            esc_html__( 'Dark Gray 3 (#888)', 'viettitan' ) => 's-dark-gray-3'  ,
                        )
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__( 'Size', 'viettitan' ),
                        'param_name' => 'size',
                        'std' => 's-md',
                        'value' => array(
                            esc_html__( 'Medium', 'viettitan' ) => 's-md'  ,
                            esc_html__( 'Large', 'viettitan' ) => 's-lg'
                        )
                    ),
                    array(
                        'type' => 'multi-select',
                        'heading' => esc_html__( 'Icons', 'viettitan' ),
                        'admin_label' => true,
                        'param_name' => 'icons',
                        'options' => array(
                            esc_html__( 'Twitter', 'viettitan' ) => 'twitter'  ,
                            esc_html__( 'Facebook', 'viettitan' ) => 'facebook'  ,
                            esc_html__( 'Dribbble', 'viettitan' ) => 'dribbble'  ,
                            esc_html__( 'Vimeo', 'viettitan' ) => 'vimeo'  ,
                            esc_html__( 'Tumblr', 'viettitan' ) => 'tumblr'  ,
                            esc_html__( 'Skype', 'viettitan' ) => 'skype'  ,
                            esc_html__( 'LinkedIn', 'viettitan' ) => 'linkedin' ,
                            esc_html__( 'Google+', 'viettitan' ) => 'googleplus' ,
                            esc_html__( 'Flickr', 'viettitan' ) => 'flickr'  ,
                            esc_html__( 'YouTube', 'viettitan' ) => 'youtube'  ,
                            esc_html__( 'Pinterest', 'viettitan' ) => 'pinterest' ,
                            esc_html__( 'Foursquare', 'viettitan' ) => 'foursquare'  ,
                            esc_html__( 'Instagram', 'viettitan' ) => 'instagram' ,
                            esc_html__( 'GitHub', 'viettitan' ) => 'github'  ,
                            esc_html__( 'Xing', 'viettitan' ) => 'xing' ,
                            esc_html__( 'Behance', 'viettitan' ) => 'behance'  ,
                            esc_html__( 'Deviantart', 'viettitan' ) => 'deviantart'  ,
                            esc_html__( 'SoundCloud', 'viettitan' ) => 'soundcloud'  ,
                            esc_html__( 'Yelp', 'viettitan' ) => 'yelp' ,
                            esc_html__( 'RSS Feed', 'viettitan' ) => 'rss'  ,
                            esc_html__( 'Email address', 'viettitan' ) => 'email'  ,
                        )
                    ),
                )
            ) );
        }
    }
    add_action('widgets_init', 'viettitan_register_social_profile', 1);
}