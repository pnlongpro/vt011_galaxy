<?php
/**
 * Created by PhpStorm.
 * User: phuongth
 * Date: 10/5/15
 * Time: 2:18 PM
 */
if (!defined('ABSPATH')) {
    exit;
}
if (!class_exists('viettitan_acf_widget_fields')) {
    class viettitan_acf_widget_fields
    {
        function __construct($widget, $value)
        {
            $this->widget = $widget;
            $this->id = $this->widget->settings['id'];
            $this->field = array_key_exists('fields', $this->widget->settings) ? $this->widget->settings['fields'] : '';
            $this->extra = array_key_exists('extra', $this->widget->settings) ? $this->widget->settings['extra'] : '';
            $this->value = $value;
            $this->enqueue();
        }

        public function render()
        {
            $data_section_wrap = uniqid();
            $fields = $this->field;
            $extras = $this->extra;
            $field_values = is_array($this->value) && array_key_exists('fields', $this->value) ? $this->value['fields'] : '';
            $extra_values = is_array($this->value) && array_key_exists('extra', $this->value) ? $this->value['extra'] : '';
            $x = 0;
            $plugin_path = VIETTITAN_INCLUDES_DIR .'includes/widgets/fields';
            $is_edit_mode = (isset($field_values) && is_array($field_values) && count($field_values) > 0) || (isset($extra_values) && is_array($extra_values) && count($extra_values) > 0);
            if ($is_edit_mode) {
                include($plugin_path . '/templates/edit.php');
            } else {
                include($plugin_path . '/templates/new.php');
            }

        }

        public function enqueue()
        {
	        $viettitan_options = &viettitan_get_options_config();
            $min_suffix = (isset($viettitan_options['enable_minifile_js']) && $viettitan_options['enable_minifile_js'] == 1) ? '.min' : '';
            wp_enqueue_style('viettitan-widget-rows-css', VIETTITAN_INCLUDES_URI . 'includes/widgets/fields/assets/style.css', array(), false);
            wp_enqueue_script('viettitan-widget-rows-js', VIETTITAN_INCLUDES_URI . 'includes/widgets/fields/assets/main'.$min_suffix.'.js', false, true);
        }
    }
}