<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 5/28/2015
 * Time: 5:44 PM
 */
if ( !class_exists( 'ViettitanFramework_Shortcodes' ) ) {
	class ViettitanFramework_Shortcodes {
		private static $instance;

		public static function init() {
			if ( !isset( self::$instance ) ) {
				self::$instance = new ViettitanFramework_Shortcodes;
				add_action( 'init', array( self::$instance, 'includes' ), 0 );
				add_action( 'init', array( self::$instance, 'register_vc_map' ), 10 );
			}

			return self::$instance;
		}

		public function includes() {
			if ( !class_exists( 'Vc_Manager' ) ) {
				return;
			}
			$viettitan_options = &viettitan_get_options_config();
			$cpt_disable       = isset( $viettitan_options['cpt-disable'] ) ? $viettitan_options['cpt-disable'] : array();
			include_once( VIETTITAN_INCLUDES_DIR . 'includes/shortcodes/slider-container/slider-container.php' );
			include_once( VIETTITAN_INCLUDES_DIR . 'includes/shortcodes/heading/heading.php' );
			include_once( VIETTITAN_INCLUDES_DIR . 'includes/shortcodes/button/button.php' );
			include_once( VIETTITAN_INCLUDES_DIR . 'includes/shortcodes/partner-carousel/partner-carousel.php' );
			include_once( VIETTITAN_INCLUDES_DIR . 'includes/shortcodes/post/post.php' );
			include_once( VIETTITAN_INCLUDES_DIR . 'includes/shortcodes/call-action/call-action.php' );
			include_once( VIETTITAN_INCLUDES_DIR . 'includes/shortcodes/testimonial/testimonial.php' );
			include_once( VIETTITAN_INCLUDES_DIR . 'includes/shortcodes/banner/banner.php' );
			include_once( VIETTITAN_INCLUDES_DIR . 'includes/shortcodes/social-icon/social-icon.php' );
			include_once( VIETTITAN_INCLUDES_DIR . 'includes/shortcodes/google-map/google-map.php' );
			if ( class_exists( 'WooCommerce' ) ) {
				include_once( VIETTITAN_INCLUDES_DIR . 'includes/shortcodes/product/product.php' );
				include_once( VIETTITAN_INCLUDES_DIR . 'includes/shortcodes/product/product-categories.php' );
				include_once( VIETTITAN_INCLUDES_DIR . 'includes/shortcodes/product/product-sidebar.php' );
			}

			include_once( VIETTITAN_INCLUDES_DIR . 'includes/shortcodes/blog/blog.php' );
		}

		public static function viettitan_get_css_animation( $css_animation ) {
			$output = '';
			if ( $css_animation != '' ) {
				wp_enqueue_script( 'waypoints' );
				$output = ' wpb_animate_when_almost_visible viettitan-css-animation ' . $css_animation;
			}

			return $output;
		}

		public static function viettitan_get_style_animation( $duration, $delay ) {
			$styles = array();
			if ( $duration != '0' && !empty( $duration ) ) {
				$duration = (float) trim( $duration, "\n\ts" );
				$styles[] = "-webkit-animation-duration: {$duration}s";
				$styles[] = "-moz-animation-duration: {$duration}s";
				$styles[] = "-ms-animation-duration: {$duration}s";
				$styles[] = "-o-animation-duration: {$duration}s";
				$styles[] = "animation-duration: {$duration}s";
			}
			if ( $delay != '0' && !empty( $delay ) ) {
				$delay    = (float) trim( $delay, "\n\ts" );
				$styles[] = "opacity: 0";
				$styles[] = "-webkit-animation-delay: {$delay}s";
				$styles[] = "-moz-animation-delay: {$delay}s";
				$styles[] = "-ms-animation-delay: {$delay}s";
				$styles[] = "-o-animation-delay: {$delay}s";
				$styles[] = "animation-delay: {$delay}s";
			}
			if ( count( $styles ) > 1 ) {
				return 'style="' . implode( ';', $styles ) . '"';
			}

			return implode( ';', $styles );
		}

		public static function substr( $str, $txt_len, $end_txt = '...' ) {
			if ( empty( $str ) ) {
				return '';
			}
			if ( strlen( $str ) <= $txt_len ) {
				return $str;
			}

			$i = $txt_len;
			while ( $str[$i] != ' ' ) {
				$i --;
				if ( $i == - 1 ) {
					break;
				}
			}
			while ( $str[$i] == ' ' ) {
				$i --;
				if ( $i == - 1 ) {
					break;
				}
			}

			return substr( $str, 0, $i + 1 ) . $end_txt;
		}

		public function register_vc_map() {
			$viettitan_icons        = &viettitan_get_theme_icon();
			$viettitan_font_awesome = &viettitan_get_font_awesome();
			$viettitan_options      = &viettitan_get_options_config();
			$cpt_disable            = isset( $viettitan_options['cpt-disable'] ) ? $viettitan_options['cpt-disable'] : array();

			if ( function_exists( 'vc_map' ) ) {
				$add_css_animation = array(
					'type'        => 'dropdown',
					'heading'     => esc_html__( 'CSS Animation', 'viettitan' ),
					'param_name'  => 'css_animation',
					'value'       => array( esc_html__( 'No', 'viettitan' ) => '', esc_html__( 'Fade In', 'viettitan' ) => 'wpb_fadeIn', esc_html__( 'Fade Top to Bottom', 'viettitan' ) => 'wpb_fadeInDown', esc_html__( 'Fade Bottom to Top', 'viettitan' ) => 'wpb_fadeInUp', esc_html__( 'Fade Left to Right', 'viettitan' ) => 'wpb_fadeInLeft', esc_html__( 'Fade Right to Left', 'viettitan' ) => 'wpb_fadeInRight', esc_html__( 'Bounce In', 'viettitan' ) => 'wpb_bounceIn', esc_html__( 'Bounce Top to Bottom', 'viettitan' ) => 'wpb_bounceInDown', esc_html__( 'Bounce Bottom to Top', 'viettitan' ) => 'wpb_bounceInUp', esc_html__( 'Bounce Left to Right', 'viettitan' ) => 'wpb_bounceInLeft', esc_html__( 'Bounce Right to Left', 'viettitan' ) => 'wpb_bounceInRight', esc_html__( 'Zoom In', 'viettitan' ) => 'wpb_zoomIn', esc_html__( 'Flip Vertical', 'viettitan' ) => 'wpb_flipInX', esc_html__( 'Flip Horizontal', 'viettitan' ) => 'wpb_flipInY', esc_html__( 'Bounce', 'viettitan' ) => 'wpb_bounce', esc_html__( 'Flash', 'viettitan' ) => 'wpb_flash', esc_html__( 'Shake', 'viettitan' ) => 'wpb_shake', esc_html__( 'Pulse', 'viettitan' ) => 'wpb_pulse', esc_html__( 'Swing', 'viettitan' ) => 'wpb_swing', esc_html__( 'Rubber band', 'viettitan' ) => 'wpb_rubberBand', esc_html__( 'Wobble', 'viettitan' ) => 'wpb_wobble', esc_html__( 'Tada', 'viettitan' ) => 'wpb_tada' ),
					'description' => esc_html__( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'viettitan' ),
					'group'       => esc_html__( 'Animation Settings', 'viettitan' )
				);

				$add_duration_animation = array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'Animation Duration', 'viettitan' ),
					'param_name'  => 'duration',
					'value'       => '',
					'description' => esc_html__( 'Duration in seconds. You can use decimal points in the value. Use this field to specify the amount of time the animation plays. <em>The default value depends on the animation, leave blank to use the default.</em>', 'viettitan' ),
					'dependency'  => Array( 'element' => 'css_animation', 'value' => array( 'wpb_fadeIn', 'wpb_fadeInDown', 'wpb_fadeInUp', 'wpb_fadeInLeft', 'wpb_fadeInRight', 'wpb_bounceIn', 'wpb_bounceInDown', 'wpb_bounceInUp', 'wpb_bounceInLeft', 'wpb_bounceInRight', 'wpb_zoomIn', 'wpb_flipInX', 'wpb_flipInY', 'wpb_bounce', 'wpb_flash', 'wpb_shake', 'wpb_pulse', 'wpb_swing', 'wpb_rubberBand', 'wpb_wobble', 'wpb_tada' ) ),
					'group'       => esc_html__( 'Animation Settings', 'viettitan' )
				);

				$add_delay_animation = array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'Animation Delay', 'viettitan' ),
					'param_name'  => 'delay',
					'value'       => '',
					'description' => esc_html__( 'Delay in seconds. You can use decimal points in the value. Use this field to delay the animation for a few seconds, this is helpful if you want to chain different effects one after another above the fold.', 'viettitan' ),
					'dependency'  => Array( 'element' => 'css_animation', 'value' => array( 'wpb_fadeIn', 'wpb_fadeInDown', 'wpb_fadeInUp', 'wpb_fadeInLeft', 'wpb_fadeInRight', 'wpb_bounceIn', 'wpb_bounceInDown', 'wpb_bounceInUp', 'wpb_bounceInLeft', 'wpb_bounceInRight', 'wpb_zoomIn', 'wpb_flipInX', 'wpb_flipInY', 'wpb_bounce', 'wpb_flash', 'wpb_shake', 'wpb_pulse', 'wpb_swing', 'wpb_rubberBand', 'wpb_wobble', 'wpb_tada' ) ),
					'group'       => esc_html__( 'Animation Settings', 'viettitan' )
				);

				$add_el_class     = array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'Extra class name', 'viettitan' ),
					'param_name'  => 'el_class',
					'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'viettitan' ),
				);
				$target_arr       = array(
					esc_html__( 'Same window', 'viettitan' ) => '_self',
					esc_html__( 'New window', 'viettitan' )  => '_blank'
				);
				$icon_type        = array(
					'type'        => 'dropdown',
					'heading'     => esc_html__( 'Icon library', 'viettitan' ),
					'value'       => array(
						esc_html__( '[None]', 'viettitan' )         => '',
						esc_html__( 'Viettitan Icon', 'viettitan' ) => 'viettitan',
						esc_html__( 'Font Awesome', 'viettitan' )   => 'fontawesome',
						esc_html__( 'Open Iconic', 'viettitan' )    => 'openiconic',
						esc_html__( 'Typicons', 'viettitan' )       => 'typicons',
						esc_html__( 'Entypo', 'viettitan' )         => 'entypo',
						esc_html__( 'Linecons', 'viettitan' )       => 'linecons',
						esc_html__( 'Image', 'viettitan' )          => 'image',
					),
					'param_name'  => 'icon_type',
					'description' => esc_html__( 'Select icon library.', 'viettitan' ),
				);
				$icon_font        = array(
					'type'        => 'dropdown',
					'heading'     => esc_html__( 'Icon library', 'viettitan' ),
					'value'       => array(
						esc_html__( '[None]', 'viettitan' )         => '',
						esc_html__( 'Viettitan Icon', 'viettitan' ) => 'viettitan',
						esc_html__( 'Font Awesome', 'viettitan' )   => 'fontawesome',
						esc_html__( 'Open Iconic', 'viettitan' )    => 'openiconic',
						esc_html__( 'Typicons', 'viettitan' )       => 'typicons',
						esc_html__( 'Entypo', 'viettitan' )         => 'entypo',
						esc_html__( 'Linecons', 'viettitan' )       => 'linecons',
					),
					'param_name'  => 'icon_type',
					'description' => esc_html__( 'Select icon library.', 'viettitan' ),
				);
				$icon_fontawesome = array(
					'type'        => 'iconpicker',
					'heading'     => esc_html__( 'Icon', 'viettitan' ),
					'param_name'  => 'icon_fontawesome',
					'value'       => 'fa fa-adjust', // default value to backend editor admin_label
					'settings'    => array(
						'emptyIcon'    => false,
						// default true, display an "EMPTY" icon?
						'iconsPerPage' => 4000,
						'source'       => $viettitan_font_awesome,
						// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
					),
					'dependency'  => array(
						'element' => 'icon_type',
						'value'   => 'fontawesome',
					),
					'description' => esc_html__( 'Select icon from library.', 'viettitan' ),
				);
				$icon_viettitan   = array(
					'type'        => 'iconpicker',
					'heading'     => esc_html__( 'Icon', 'viettitan' ),
					'param_name'  => 'icon_viettitan',
					'settings'    => array(
						'emptyIcon'    => false, // default true, display an "EMPTY" icon?
						'iconsPerPage' => 4000,
						'type'         => 'viettitan',
						'source'       => $viettitan_icons,
					),
					'dependency'  => array(
						'element' => 'icon_type',
						'value'   => 'viettitan',
					),
					'description' => esc_html__( 'Select icon from library.', 'viettitan' ),
				);
				$icon_openiconic  = array(
					'type'        => 'iconpicker',
					'heading'     => esc_html__( 'Icon', 'viettitan' ),
					'param_name'  => 'icon_openiconic',
					'value'       => 'vc-oi vc-oi-dial', // default value to backend editor admin_label
					'settings'    => array(
						'emptyIcon'    => false, // default true, display an "EMPTY" icon?
						'type'         => 'openiconic',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency'  => array(
						'element' => 'icon_type',
						'value'   => 'openiconic',
					),
					'description' => esc_html__( 'Select icon from library.', 'viettitan' ),
				);
				$icon_typicons    = array(
					'type'        => 'iconpicker',
					'heading'     => esc_html__( 'Icon', 'viettitan' ),
					'param_name'  => 'icon_typicons',
					'value'       => 'typcn typcn-adjust-brightness', // default value to backend editor admin_label
					'settings'    => array(
						'emptyIcon'    => false, // default true, display an "EMPTY" icon?
						'type'         => 'typicons',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency'  => array(
						'element' => 'icon_type',
						'value'   => 'typicons',
					),
					'description' => esc_html__( 'Select icon from library.', 'viettitan' ),
				);
				$icon_entypo      = array(
					'type'       => 'iconpicker',
					'heading'    => esc_html__( 'Icon', 'viettitan' ),
					'param_name' => 'icon_entypo',
					'value'      => 'entypo-icon entypo-icon-note', // default value to backend editor admin_label
					'settings'   => array(
						'emptyIcon'    => false, // default true, display an "EMPTY" icon?
						'type'         => 'entypo',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'icon_type',
						'value'   => 'entypo',
					),
				);
				$icon_linecons    = array(
					'type'        => 'iconpicker',
					'heading'     => esc_html__( 'Icon', 'viettitan' ),
					'param_name'  => 'icon_linecons',
					'value'       => 'vc_li vc_li-heart', // default value to backend editor admin_label
					'settings'    => array(
						'emptyIcon'    => false, // default true, display an "EMPTY" icon?
						'type'         => 'linecons',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency'  => array(
						'element' => 'icon_type',
						'value'   => 'linecons',
					),
					'description' => esc_html__( 'Select icon from library.', 'viettitan' ),
				);
				$icon_image       = array(
					'type'        => 'attach_image',
					'heading'     => esc_html__( 'Upload Image Icon:', 'viettitan' ),
					'param_name'  => 'icon_image',
					'value'       => '',
					'description' => esc_html__( 'Upload the custom image icon.', 'viettitan' ),
					'dependency'  => Array( 'element' => 'icon_type', 'value' => array( 'image' ) ),
				);
				vc_map( array(
					'name'                    => esc_html__( 'Slider Container', 'viettitan' ),
					'base'                    => 'viettitan_slider_container',
					'class'                   => '',
					'icon'                    => 'fa fa-ellipsis-h',
					'category'                => VIETTITAN_SHORTCODE_CATEGORY,
					'as_parent'               => array( 'except' => 'viettitan_slider_container' ),
					'content_element'         => true,
					'show_settings_on_create' => true,
					'params'                  => array(
						array(
							'type'             => 'checkbox',
							'heading'          => esc_html__( 'Loop', 'viettitan' ),
							'param_name'       => 'loop',
							'description'      => esc_html__( 'Inifnity loop.', 'viettitan' ),
							'value'            => array( esc_html__( 'Yes, please', 'viettitan' ) => 'yes' ),
							'std'              => 'yes',
							'edit_field_class' => 'vc_col-sm-6 vc_column'
						),
						array(
							'type'             => 'checkbox',
							'heading'          => esc_html__( 'Center', 'viettitan' ),
							'param_name'       => 'center',
							'description'      => esc_html__( 'Center item. Works well with even an odd number of items.', 'viettitan' ),
							'value'            => array( esc_html__( 'Yes, please', 'viettitan' ) => 'yes' ),
							'edit_field_class' => 'vc_col-sm-6 vc_column'
						),
						array(
							'type'             => 'checkbox',
							'heading'          => esc_html__( 'Navigation', 'viettitan' ),
							'param_name'       => 'nav',
							'description'      => esc_html__( 'Show navigation.', 'viettitan' ),
							'value'            => array( esc_html__( 'Yes, please', 'viettitan' ) => 'yes' ),
							'edit_field_class' => 'vc_col-sm-6 vc_column'
						),
						array(
							'type'             => 'checkbox',
							'heading'          => esc_html__( 'Pagination', 'viettitan' ),
							'param_name'       => 'dots',
							'description'      => esc_html__( 'Show pagination.', 'viettitan' ),
							'value'            => array( esc_html__( 'Yes, please', 'viettitan' ) => 'yes' ),
							'edit_field_class' => 'vc_col-sm-6 vc_column'
						),
						array(
							'type'             => 'checkbox',
							'heading'          => esc_html__( 'Autoplay', 'viettitan' ),
							'param_name'       => 'autoplay',
							'description'      => esc_html__( 'Autoplay.', 'viettitan' ),
							'value'            => array( esc_html__( 'Yes, please', 'viettitan' ) => 'yes' ),
							'std'              => 'yes',
							'edit_field_class' => 'vc_col-sm-6 vc_column'
						),
						array(
							'type'             => 'checkbox',
							'heading'          => esc_html__( 'Pause on hover', 'viettitan' ),
							'param_name'       => 'autoplayhoverpause',
							'description'      => esc_html__( 'Pause on mouse hover.', 'viettitan' ),
							'value'            => array( esc_html__( 'Yes, please', 'viettitan' ) => 'yes' ),
							'std'              => 'yes',
							'edit_field_class' => 'vc_col-sm-6 vc_column'
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Autoplay Timeout', 'viettitan' ),
							'param_name'  => 'autoplaytimeout',
							'description' => esc_html__( 'Autoplay interval timeout.', 'viettitan' ),
							'value'       => '',
							'std'         => 5000
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Items', 'viettitan' ),
							'param_name'  => 'items',
							'description' => esc_html__( 'This variable allows you to set the maximum amount of items displayed at a time with the widest browser width', 'viettitan' ),
							'value'       => '',
							'std'         => 4
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Items Desktop', 'viettitan' ),
							'param_name'  => 'itemsdesktop',
							'description' => esc_html__( 'Browser Width >= 1200', 'viettitan' ),
							'value'       => '',
							'std'         => '4'
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Items Desktop Small', 'viettitan' ),
							'param_name'  => 'itemsdesktopsmall',
							'description' => esc_html__( 'Browser Width >= 980', 'viettitan' ),
							'value'       => '',
							'std'         => '3'
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Items Tablet', 'viettitan' ),
							'param_name'  => 'itemstablet',
							'description' => esc_html__( 'Browser Width >= 768', 'viettitan' ),
							'value'       => '',
							'std'         => '2'
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Items Tablet Small', 'viettitan' ),
							'param_name'  => 'itemstabletsmall',
							'description' => esc_html__( 'Browser Width >= 600', 'viettitan' ),
							'value'       => '',
							'std'         => '2'
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Items Mobile', 'viettitan' ),
							'param_name'  => 'itemsmobile',
							'description' => esc_html__( 'Browser Width < 600', 'viettitan' ),
							'value'       => '',
							'std'         => '1'
						),
						$add_el_class,
						$add_css_animation,
						$add_duration_animation,
						$add_delay_animation
					),
					'js_view'                 => 'VcColumnView'
				) );

				//Button
				vc_map( array(
					'name'     => esc_html__( 'Button', 'viettitan' ),
					'base'     => 'viettitan_button',
					'class'    => '',
					'icon'     => 'fa fa-bold',
					'category' => VIETTITAN_SHORTCODE_CATEGORY,
					'params'   => array(
						array(
							'type'       => 'vc_link',
							'heading'    => esc_html__( 'Link (url)', 'viettitan' ),
							'param_name' => 'link',
							'value'      => '',
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Layout Style', 'viettitan' ),
							'param_name'  => 'layout_style',
							'admin_label' => true,
							'value'       => array(
								esc_html__( 'Border 1px', 'viettitan' )    => 'm-button-bordered',
								esc_html__( 'Border 2px', 'viettitan' )    => 'm-button-bordered-2',
								esc_html__( 'Background', 'viettitan' )    => 'm-button-bg',
								esc_html__( 'Background 3D', 'viettitan' ) => 'm-button-3d', ),
							'description' => esc_html__( 'Select Layout Style.', 'viettitan' )
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Size', 'viettitan' ),
							'param_name'  => 'size',
							'admin_label' => true,
							'value'       => array(
								esc_html__( 'Extra Small', 'viettitan' ) => 'm-button-xs',
								esc_html__( 'Small', 'viettitan' )       => 'm-button-sm',
								esc_html__( 'Medium', 'viettitan' )      => 'm-button-md',
								esc_html__( 'Large', 'viettitan' )       => 'm-button-lg',
								esc_html__( 'Extra Large', 'viettitan' ) => 'm-button-xlg' ),
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Color', 'viettitan' ),
							'param_name'  => 'button_color',
							'value'       => array(
								esc_html__( 'Primary', 'viettitan' )   => 'm-button-primary',
								esc_html__( 'Secondary', 'viettitan' ) => 'm-button-secondary',
								esc_html__( 'Gray', 'viettitan' )      => 'm-button-gray',
								esc_html__( 'Black', 'viettitan' )     => 'm-button-black',
								esc_html__( 'Light', 'viettitan' )     => 'm-button-light'
							),
							'description' => esc_html__( 'Select color for your element', 'viettitan' ),
						),
						array(
							'type'       => 'checkbox',
							'heading'    => esc_html__( 'Add icon?', 'viettitan-framework' ),
							'param_name' => 'add_icon',
							'value'      => array( esc_html__( 'Yes, please', 'viettitan' ) => 'yes' ),
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Icon library', 'viettitan' ),
							'value'       => array(
								esc_html__( '[None]', 'viettitan' )         => '',
								esc_html__( 'Viettitan Icon', 'viettitan' ) => 'viettitan',
								esc_html__( 'Font Awesome', 'viettitan' )   => 'fontawesome',
								esc_html__( 'Open Iconic', 'viettitan' )    => 'openiconic',
								esc_html__( 'Typicons', 'viettitan' )       => 'typicons',
								esc_html__( 'Entypo', 'viettitan' )         => 'entypo',
								esc_html__( 'Linecons', 'viettitan' )       => 'linecons',
								esc_html__( 'Image', 'viettitan' )          => 'image',
							),
							'param_name'  => 'icon_type',
							'description' => esc_html__( 'Select icon library.', 'viettitan' ),
							'dependency'  => Array( 'element' => 'add_icon', 'value' => 'yes' ),
						),
						$icon_viettitan,
						$icon_fontawesome,
						$icon_openiconic,
						$icon_typicons,
						$icon_entypo,
						$icon_linecons,
						$icon_image,
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Icon Alignment', 'viettitan' ),
							'description' => esc_html__( 'Select icon alignment.', 'viettitan' ),
							'param_name'  => 'i_align',
							'value'       => array(
								esc_html__( 'Left', 'viettitan' )  => 'i-left',
								esc_html__( 'Right', 'viettitan' ) => 'i-right',
							),
							'dependency'  => Array( 'element' => 'add_icon', 'value' => 'yes' ),
						),
						$add_el_class,
						$add_css_animation,
						$add_duration_animation,
						$add_delay_animation
					)
				) );

				//Call to action
				vc_map( array(
					'name'     => esc_html__( 'Call To Action', 'viettitan' ),
					'base'     => 'viettitan_call_action',
					'class'    => '',
					'icon'     => 'fa fa-play',
					'category' => VIETTITAN_SHORTCODE_CATEGORY,
					'params'   => array(
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Layout Style', 'viettitan' ),
							'param_name'  => 'layout_style',
							'admin_label' => true,
							'value'       => array(
								esc_html__( 'Only Text', 'viettitan' )      => 'style1',
								esc_html__( 'Text with Icon', 'viettitan' ) => 'style2' ),
							'description' => esc_html__( 'Select Layout Style.', 'viettitan' )
						),
						array(
							'type'       => 'textfield',
							'heading'    => esc_html__( 'Title', 'viettitan' ),
							'param_name' => 'title',
							'value'      => '',
						),
						array(
							'type'       => 'textarea',
							'heading'    => esc_html__( 'Description', 'viettitan' ),
							'param_name' => 'description',
							'value'      => '',
							'dependency' => array( 'element' => 'layout_style', 'value' => 'style2' )
						),
						array(
							'type'       => 'vc_link',
							'heading'    => esc_html__( 'Link (url)', 'viettitan' ),
							'param_name' => 'link',
							'value'      => '',
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Icon library', 'viettitan' ),
							'value'       => array(
								esc_html__( '[None]', 'viettitan' )         => '',
								esc_html__( 'Viettitan Icon', 'viettitan' ) => 'viettitan',
								esc_html__( 'Font Awesome', 'viettitan' )   => 'fontawesome',
								esc_html__( 'Open Iconic', 'viettitan' )    => 'openiconic',
								esc_html__( 'Typicons', 'viettitan' )       => 'typicons',
								esc_html__( 'Entypo', 'viettitan' )         => 'entypo',
								esc_html__( 'Linecons', 'viettitan' )       => 'linecons',
								esc_html__( 'Image', 'viettitan' )          => 'image',
							),
							'param_name'  => 'icon_type',
							'description' => esc_html__( 'Select icon library.', 'viettitan' ),
							'dependency'  => array( 'element' => 'layout_style', 'value' => 'style2' )
						),
						$icon_viettitan,
						$icon_fontawesome,
						$icon_openiconic,
						$icon_typicons,
						$icon_entypo,
						$icon_linecons,
						$icon_image,
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Background Color', 'viettitan' ),
							'param_name'  => 'color',
							'admin_label' => true,
							'value'       => array(
								esc_html__( 'Primary', 'viettitan' )     => 'p-color-bg',
								esc_html__( 'Secondary', 'viettitan' )   => 's-color-bg',
								esc_html__( 'Transparent', 'viettitan' ) => 'call-action-transparent' ),
							'description' => esc_html__( 'Select color for your element.', 'viettitan' )
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Height', 'viettitan' ),
							'param_name'  => 'height',
							'value'       => array(
								esc_html__( 'Tall', 'viettitan' )  => '',
								esc_html__( 'Short', 'viettitan' ) => 'short',
							),
							'description' => esc_html__( 'Select height for your element.', 'viettitan' )
						),
						$add_el_class,
						$add_css_animation,
						$add_duration_animation,
						$add_delay_animation
					)
				) );
				$category   = array();
				$categories = get_categories();
				if ( is_array( $categories ) ) {
					foreach ( $categories as $cat ) {
						$category[$cat->name] = $cat->slug;
					}
				}
				//Blog
				vc_map(
					array(
						'name'     => esc_html__( 'Blog', 'viettitan' ),
						'base'     => 'viettitan_blog',
						'icon'     => 'fa fa-file-text',
						'category' => VIETTITAN_SHORTCODE_CATEGORY,
						'params'   => array(

							array(
								'type'             => 'dropdown',
								'heading'          => esc_html__( 'Blog Style', 'viettitan' ),
								'param_name'       => 'type',
								'value'            => array(
									esc_html__( 'Larger Image', 'viettitan' ) => 'large-image',
									esc_html__( 'Medium Image', 'viettitan' ) => 'medium-image',
									esc_html__( 'Timeline', 'viettitan' )     => 'timeline',
									esc_html__( 'Masonry', 'viettitan' )      => 'masonry'
								),
								'std'              => 'large-image',
								'edit_field_class' => 'vc_col-sm-6 vc_column vc_column-with-padding',
							),

							array(
								"type"             => "dropdown",
								"heading"          => esc_html__( "Columns", 'viettitan' ),
								"param_name"       => "columns",
								"value"            => array(
									esc_html__( '2 columns', 'viettitan' ) => 2,
									esc_html__( '3 columns', 'viettitan' ) => 3,
									esc_html__( '4 columns', 'viettitan' ) => 4,
								),
								"description"      => esc_html__( "How much columns grid", 'viettitan' ),
								'dependency'       => array(
									'element' => 'type',
									'value'   => array( 'masonry' )
								),
								'std'              => 2,
								'edit_field_class' => 'vc_col-sm-6 vc_column',
							),


							array(
								'type'       => 'multi-select',
								'heading'    => esc_html__( 'Narrow Category', 'viettitan' ),
								'param_name' => 'category',
								'options'    => $category
							),

							array(
								"type"        => "textfield",
								"heading"     => esc_html__( "Total items", 'viettitan' ),
								"param_name"  => "max_items",
								"value"       => - 1,
								"description" => esc_html__( 'Set max limit for items or enter -1 to display all.', 'viettitan' )
							),

							array(
								'type'             => 'dropdown',
								'heading'          => esc_html__( 'Navigation Type', 'viettitan' ),
								'param_name'       => 'paging_style',
								'value'            => array(
									esc_html__( 'Show all', 'viettitan' )        => 'all',
									esc_html__( 'Default', 'viettitan' )         => 'default',
									esc_html__( 'Load More', 'viettitan' )       => 'load-more',
									esc_html__( 'Infinity Scroll', 'viettitan' ) => 'infinity-scroll',
								),
								'std'              => 'all',
								'edit_field_class' => 'vc_col-sm-6 vc_column',
								'dependency'       => array(
									'element' => 'max_items',
									'value'   => array( '-1' )
								),
							),


							array(
								"type"             => "textfield",
								"heading"          => esc_html__( "Posts per page", 'viettitan' ),
								"param_name"       => "posts_per_page",
								"value"            => get_option( 'posts_per_page' ),
								"description"      => esc_html__( 'Number of items to show per page', 'viettitan' ),
								'dependency'       => array(
									'element' => 'paging_style',
									'value'   => array( 'default', 'load-more', 'infinity-scroll' ),
								),
								'edit_field_class' => 'vc_col-sm-6 vc_column',
							),


							array(
								'type'       => 'checkbox',
								'heading'    => esc_html__( 'Has Sidebar', 'viettitan' ),
								'param_name' => 'has_sidebar',
								'std'        => '',
								'value'      => array( esc_html__( 'Yes, please', 'viettitan' ) => 'yes' )
							),


							// Data settings
							array(
								'type'               => 'dropdown',
								'heading'            => esc_html__( 'Order by', 'viettitan' ),
								'param_name'         => 'orderby',
								'value'              => array(
									esc_html__( 'Date', 'viettitan' )                  => 'date',
									esc_html__( 'Order by post ID', 'viettitan' )      => 'ID',
									esc_html__( 'Author', 'viettitan' )                => 'author',
									esc_html__( 'Title', 'viettitan' )                 => 'title',
									esc_html__( 'Last modified date', 'viettitan' )    => 'modified',
									esc_html__( 'Post/page parent ID', 'viettitan' )   => 'parent',
									esc_html__( 'Number of comments', 'viettitan' )    => 'comment_count',
									esc_html__( 'Menu order/Page Order', 'viettitan' ) => 'menu_order',
									esc_html__( 'Meta value', 'viettitan' )            => 'meta_value',
									esc_html__( 'Meta value number', 'viettitan' )     => 'meta_value_num',
									esc_html__( 'Random order', 'viettitan' )          => 'rand',
								),
								'description'        => esc_html__( 'Select order type. If "Meta value" or "Meta value Number" is chosen then meta key is required.', 'viettitan' ),
								'group'              => esc_html__( 'Data Settings', 'viettitan' ),
								'param_holder_class' => 'vc_grid-data-type-not-ids',
							),

							array(
								'type'               => 'dropdown',
								'heading'            => esc_html__( 'Sorting', 'viettitan' ),
								'param_name'         => 'order',
								'group'              => esc_html__( 'Data Settings', 'viettitan' ),
								'value'              => array(
									esc_html__( 'Descending', 'viettitan' ) => 'DESC',
									esc_html__( 'Ascending', 'viettitan' )  => 'ASC',
								),
								'param_holder_class' => 'vc_grid-data-type-not-ids',
								'description'        => esc_html__( 'Select sorting order.', 'viettitan' ),
							),

							array(
								'type'               => 'textfield',
								'heading'            => esc_html__( 'Meta key', 'viettitan' ),
								'param_name'         => 'meta_key',
								'description'        => esc_html__( 'Input meta key for grid ordering.', 'viettitan' ),
								'group'              => esc_html__( 'Data Settings', 'viettitan' ),
								'param_holder_class' => 'vc_grid-data-type-not-ids',
								'dependency'         => array(
									'element' => 'orderby',
									'value'   => array( 'meta_value', 'meta_value_num' ),
								),
							),

							$add_css_animation,
							$add_duration_animation,
							$add_delay_animation,
							$add_el_class
						)
					)
				);

				//Post
				vc_map( array(
					'name'        => esc_html__( 'Posts', 'viettitan' ),
					'base'        => 'viettitan_post',
					'icon'        => 'fa fa-file-text-o',
					'category'    => VIETTITAN_SHORTCODE_CATEGORY,
					'description' => esc_html__( 'Posts', 'viettitan' ),
					'params'      => array(
						array(
							'type'       => 'multi-select',
							'heading'    => esc_html__( 'Category', 'viettitan' ),
							'param_name' => 'category',
							'options'    => $category
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Display', 'viettitan' ),
							'param_name'  => 'display',
							'admin_label' => true,
							'value'       => array( esc_html__( 'Random', '' ) => 'random', esc_html__( 'Popular', 'viettitan' ) => 'popular', esc_html__( 'Recent', 'viettitan' ) => 'recent', esc_html__( 'Oldest', 'viettitan' ) => 'oldest' ),
							'std'         => 'recent',
							'description' => esc_html__( 'Select Orderby.', 'viettitan' )
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Layout Style', 'viettitan' ),
							'param_name'  => 'layout_style',
							'admin_label' => true,
							'value'       => array(
								esc_html__( 'Default', 'viettitan' )         => 'style1',
								esc_html__( 'Large and Small', 'viettitan' ) => 'style2' ),
							'description' => esc_html__( 'Select Layout Style.', 'viettitan' )
						),
						array(
							'type'       => 'textfield',
							'heading'    => esc_html__( 'Item Amount', 'viettitan' ),
							'param_name' => 'item_amount',
							'value'      => '6',
							'dependency' => Array( 'element' => 'layout_style', 'value' => 'style1' )
						),
						array(
							'type'       => 'textfield',
							'heading'    => esc_html__( 'Column', 'viettitan' ),
							'param_name' => 'column',
							'value'      => '3',
							'dependency' => Array( 'element' => 'layout_style', 'value' => 'style1' )
						),
						array(
							'type'       => 'checkbox',
							'heading'    => esc_html__( 'Slider Style', 'viettitan' ),
							'param_name' => 'is_slider',
							'value'      => array( esc_html__( 'Yes, please', 'viettitan' ) => 'yes' ),
							'dependency' => Array( 'element' => 'layout_style', 'value' => 'style1' )
						),
						array(
							'type'       => 'checkbox',
							'heading'    => esc_html__( 'Show pagination control', 'viettitan' ),
							'param_name' => 'dots',
							'value'      => array( esc_html__( 'Yes, please', 'viettitan' ) => 'yes' ),
							'dependency' => Array( 'element' => 'is_slider', 'value' => 'yes' )
						),
						array(
							'type'       => 'checkbox',
							'heading'    => esc_html__( 'Show navigation control', 'viettitan' ),
							'param_name' => 'nav',
							'value'      => array( esc_html__( 'Yes, please', 'viettitan' ) => 'yes' ),
							'dependency' => Array( 'element' => 'is_slider', 'value' => 'yes' )
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Autoplay Timeout', 'viettitan' ),
							'param_name'  => 'autoplaytimeout',
							'description' => esc_html__( 'Autoplay interval timeout.', 'viettitan' ),
							'value'       => '',
							'dependency'  => Array( 'element' => 'is_slider', 'value' => 'yes' ),
							'std'         => 5000
						),
						$add_el_class,
						$add_css_animation,
						$add_duration_animation,
						$add_delay_animation
					)
				) );

				//partner
				vc_map( array(
					'name'        => esc_html__( 'Partner Carousel', 'viettitan' ),
					'base'        => 'viettitan_partner_carousel',
					'icon'        => 'fa fa-user-plus',
					'category'    => VIETTITAN_SHORTCODE_CATEGORY,
					'description' => esc_html__( 'Animated carousel with images', 'viettitan' ),
					'params'      => array(
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Layout Style', 'viettitan' ),
							'param_name'  => 'layout_style',
							'admin_label' => true,
							'value'       => array(
								esc_html__( 'Default', 'viettitan' )    => 'style1',
								esc_html__( 'Background', 'viettitan' ) => 'style2',
								esc_html__( 'Border', 'viettitan' )     => 'style3' ),
							'description' => esc_html__( 'Select Layout Style.', 'viettitan' )
						),
						array(
							'type'        => 'attach_images',
							'heading'     => esc_html__( 'Images', 'viettitan' ),
							'param_name'  => 'images',
							'value'       => '',
							'description' => esc_html__( 'Select images from media library.', 'viettitan' )
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Image size', 'viettitan' ),
							'param_name'  => 'img_size',
							'description' => esc_html__( 'Enter image size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use "thumbnail" size.', 'viettitan' ),
							'std'         => 'full'
						),
						array(
							'type'       => 'dropdown',
							'heading'    => esc_html__( 'Image Opacity', 'viettitan' ),
							'param_name' => 'opacity',
							'value'      => array(
								esc_html__( '[none]', 'viettitan' ) => '',
								esc_html__( '10%', 'viettitan' )    => '10',
								esc_html__( '20%', 'viettitan' )    => '20',
								esc_html__( '30%', 'viettitan' )    => '30',
								esc_html__( '40%', 'viettitan' )    => '40',
								esc_html__( '50%', 'viettitan' )    => '50',
								esc_html__( '60%', 'viettitan' )    => '60',
								esc_html__( '70%', 'viettitan' )    => '70',
								esc_html__( '80%', 'viettitan' )    => '80',
								esc_html__( '90%', 'viettitan' )    => '90',
								esc_html__( '100%', 'viettitan' )   => '100'
							),
							'std'        => '80'
						),
						array(
							'type'        => 'exploded_textarea',
							'heading'     => esc_html__( 'Custom links', 'viettitan' ),
							'param_name'  => 'custom_links',
							'description' => esc_html__( 'Enter links for each slide here. Divide links with linebreaks (Enter) . ', 'viettitan' ),
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Custom link target', 'viettitan' ),
							'param_name'  => 'custom_links_target',
							'description' => esc_html__( 'Select where to open  custom links.', 'viettitan' ),
							'value'       => $target_arr
						),
						array(
							'type'             => 'checkbox',
							'heading'          => esc_html__( 'Loop', 'viettitan' ),
							'param_name'       => 'loop',
							'description'      => esc_html__( 'Inifnity loop.', 'viettitan' ),
							'value'            => array( esc_html__( 'Yes, please', 'viettitan' ) => 'yes' ),
							'std'              => 'yes',
							'edit_field_class' => 'vc_col-sm-6 vc_column'
						),
						array(
							'type'             => 'checkbox',
							'heading'          => esc_html__( 'Center', 'viettitan' ),
							'param_name'       => 'center',
							'description'      => esc_html__( 'Center item. Works well with even an odd number of items.', 'viettitan' ),
							'value'            => array( esc_html__( 'Yes, please', 'viettitan' ) => 'yes' ),
							'edit_field_class' => 'vc_col-sm-6 vc_column'
						),
						array(
							'type'             => 'checkbox',
							'heading'          => esc_html__( 'Navigation', 'viettitan' ),
							'param_name'       => 'nav',
							'description'      => esc_html__( 'Show navigation.', 'viettitan' ),
							'value'            => array( esc_html__( 'Yes, please', 'viettitan' ) => 'yes' ),
							'edit_field_class' => 'vc_col-sm-6 vc_column'
						),
						array(
							'type'             => 'checkbox',
							'heading'          => esc_html__( 'Pagination', 'viettitan' ),
							'param_name'       => 'dots',
							'description'      => esc_html__( 'Show pagination.', 'viettitan' ),
							'value'            => array( esc_html__( 'Yes, please', 'viettitan' ) => 'yes' ),
							'edit_field_class' => 'vc_col-sm-6 vc_column'
						),
						array(
							'type'             => 'checkbox',
							'heading'          => esc_html__( 'Autoplay', 'viettitan' ),
							'param_name'       => 'autoplay',
							'description'      => esc_html__( 'Autoplay.', 'viettitan' ),
							'value'            => array( esc_html__( 'Yes, please', 'viettitan' ) => 'yes' ),
							'std'              => 'yes',
							'edit_field_class' => 'vc_col-sm-6 vc_column'
						),
						array(
							'type'             => 'checkbox',
							'heading'          => esc_html__( 'Pause on hover', 'viettitan' ),
							'param_name'       => 'autoplayhoverpause',
							'description'      => esc_html__( 'Pause on mouse hover.', 'viettitan' ),
							'value'            => array( esc_html__( 'Yes, please', 'viettitan' ) => 'yes' ),
							'std'              => 'yes',
							'edit_field_class' => 'vc_col-sm-6 vc_column'
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Autoplay Timeout', 'viettitan' ),
							'param_name'  => 'autoplaytimeout',
							'description' => esc_html__( 'Autoplay interval timeout.', 'viettitan' ),
							'value'       => '',
							'std'         => 5000
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Items', 'viettitan' ),
							'param_name'  => 'items',
							'description' => esc_html__( 'This variable allows you to set the maximum amount of items displayed at a time with the widest browser width', 'viettitan' ),
							'value'       => '',
							'std'         => 6
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Items Desktop', 'viettitan' ),
							'param_name'  => 'itemsdesktop',
							'description' => esc_html__( 'Browser Width >= 1200', 'viettitan' ),
							'value'       => '',
							'std'         => '6'
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Items Desktop Small', 'viettitan' ),
							'param_name'  => 'itemsdesktopsmall',
							'description' => esc_html__( 'Browser Width >= 980', 'viettitan' ),
							'value'       => '',
							'std'         => '5'
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Items Tablet', 'viettitan' ),
							'param_name'  => 'itemstablet',
							'description' => esc_html__( 'Browser Width >= 768', 'viettitan' ),
							'value'       => '',
							'std'         => '3'
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Items Tablet Small', 'viettitan' ),
							'param_name'  => 'itemstabletsmall',
							'description' => esc_html__( 'Browser Width >= 600', 'viettitan' ),
							'value'       => '',
							'std'         => '2'
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Items Mobile', 'viettitan' ),
							'param_name'  => 'itemsmobile',
							'description' => esc_html__( 'Browser Width < 600', 'viettitan' ),
							'value'       => '',
							'std'         => '1'
						),
						$add_el_class,
						$add_css_animation,
						$add_duration_animation,
						$add_delay_animation
					)
				) );

				//heading
				vc_map( array(
					'name'     => esc_html__( 'Headings', 'viettitan' ),
					'base'     => 'viettitan_heading',
					'class'    => '',
					'icon'     => 'fa fa-header',
					'category' => VIETTITAN_SHORTCODE_CATEGORY,
					'params'   => array(
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Title', 'viettitan' ),
							'param_name'  => 'title',
							'value'       => '',
							'admin_label' => true
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Title Size', 'viettitan' ),
							'param_name'  => 'size',
							'value'       => array(
								esc_html__( 'Extra Small', 'viettitan' ) => 'size-xs',
								esc_html__( 'Small', 'viettitan' )       => 'size-sm',
								esc_html__( 'Medium', 'viettitan' )      => 'size-md',
								esc_html__( 'Large', 'viettitan' )       => 'size-lg',
								esc_html__( 'Extra Large', 'viettitan' ) => 'size-xlg' ),
							'description' => esc_html__( 'Select Size for your element.', 'viettitan' ),
							'admin_label' => true
						),
						array(
							'type'       => 'textfield',
							'heading'    => esc_html__( 'Sub Title', 'viettitan' ),
							'param_name' => 'sub_title',
							'value'      => '',
						),
						array(
							'type'       => 'textarea',
							'heading'    => esc_html__( 'Description', 'viettitan' ),
							'param_name' => 'description',
							'value'      => '',
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Line below', 'viettitan' ),
							'param_name'  => 'line_style',
							'value'       => array(
								esc_html__( 'None ', 'viettitan' )         => '',
								esc_html__( 'Double color ', 'viettitan' ) => 'line-double',
								esc_html__( 'Single color', 'viettitan' )  => 'line-single', ),
							'description' => esc_html__( 'Select line below style for your element.', 'viettitan' ),
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Text Align', 'viettitan' ),
							'param_name'  => 'text_align',
							'value'       => array(
								esc_html__( 'Center', 'viettitan' ) => 'text-center',
								esc_html__( 'Left', 'viettitan' )   => 'text-left',
								esc_html__( 'Right', 'viettitan' )  => 'text-right' ),
							'description' => esc_html__( 'Select text align.', 'viettitan' )
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Color Scheme', 'viettitan' ),
							'param_name'  => 'color_scheme',
							'value'       => array(
								esc_html__( 'Dark', 'viettitan' )  => 'color-dark',
								esc_html__( 'Light', 'viettitan' ) => 'color-light' ),
							'description' => esc_html__( 'Select Color Scheme.', 'viettitan' )
						),
						$add_el_class,
						$add_css_animation,
						$add_duration_animation,
						$add_delay_animation
					)
				) );

//Banner
				vc_map(
					array(
						'name'        => esc_html__( 'Banner', 'viettitan' ),
						'base'        => 'viettitan_banner',
						'icon'        => 'fa fa-file-image-o',
						'category'    => VIETTITAN_SHORTCODE_CATEGORY,
						'description' => esc_html__( 'Interactive banner', 'viettitan' ),
						'params'      => array(
							array(
								'type'        => 'dropdown',
								'heading'     => esc_html__( 'Layout Style', 'viettitan' ),
								'param_name'  => 'layout_style',
								'admin_label' => true,
								'value'       => array(
									esc_html__( 'Detail Light', 'viettitan' )      => 'style1',
									esc_html__( 'Simple Dark', 'viettitan' )       => 'style2',
									esc_html__( 'Background Icon', 'viettitan' )   => 'style3',
									esc_html__( 'Sale Off', 'viettitan-magatron' ) => 'style4'
								),
								'description' => esc_html__( 'Select Layout Style.', 'viettitan' )
							),
							array(
								'type'       => 'attach_image',
								'heading'    => esc_html__( 'Image Banner:', 'viettitan' ),
								'param_name' => 'image',
								'value'      => '',
								'dependency' => array(
									'element' => 'layout_style',
									'value'   => array( 'style1', 'style2', 'style4' ) )
							),
							array(
								'type'        => 'dropdown',
								'heading'     => esc_html__( 'Icon Color', 'viettitan' ),
								'param_name'  => 'icon_color',
								'value'       => array(
									esc_html__( 'Light', 'viettitan' ) => 'icon-light',
									esc_html__( 'Dark', 'viettitan' )  => 'icon-dark',
								),
								'description' => esc_html__( 'Select color for your element', 'viettitan' ),
								'dependency'  => array( 'element' => 'layout_style', 'value' => 'style3' )
							),
							array(
								'type'        => 'dropdown',
								'heading'     => esc_html__( 'Background Color', 'viettitan' ),
								'param_name'  => 'bg_color',
								'value'       => array(
									esc_html__( 'Light', 'viettitan' )     => 'banner-bg-light',
									esc_html__( 'Gray', 'viettitan' )      => 'banner-bg-gray',
									esc_html__( 'Primary', 'viettitan' )   => 'p-color-bg',
									esc_html__( 'Secondary', 'viettitan' ) => 's-color-bg',
									esc_html__( 'Dark', 'viettitan' )      => 'banner-bg-dark',
								),
								'description' => esc_html__( 'Select color for your element', 'viettitan' ),
								'dependency'  => array( 'element' => 'layout_style', 'value' => 'style3' )
							),
							array(
								'type'       => 'vc_link',
								'heading'    => esc_html__( 'Link (url)', 'viettitan' ),
								'param_name' => 'link',
								'value'      => '',
							),
							array(
								'type'       => 'checkbox',
								'heading'    => esc_html__( 'Add Button?', 'viettitan' ),
								'param_name' => 'add_button',
								'value'      => array( esc_html__( 'Yes, please', 'viettitan' ) => 'add-button' ),
								'dependency' => array( 'element' => 'layout_style', 'value' => array( 'style1', 'style3' ) ),
							),
							array(
								'type'        => 'dropdown',
								'heading'     => esc_html__( 'Icon library', 'viettitan' ),
								'value'       => array(
									esc_html__( '[None]', 'viettitan' )         => '',
									esc_html__( 'Viettitan Icon', 'viettitan' ) => 'viettitan',
									esc_html__( 'Font Awesome', 'viettitan' )   => 'fontawesome',
									esc_html__( 'Open Iconic', 'viettitan' )    => 'openiconic',
									esc_html__( 'Typicons', 'viettitan' )       => 'typicons',
									esc_html__( 'Entypo', 'viettitan' )         => 'entypo',
									esc_html__( 'Linecons', 'viettitan' )       => 'linecons',
									esc_html__( 'Image', 'viettitan' )          => 'image',
								),
								'param_name'  => 'icon_type',
								'description' => esc_html__( 'Select icon library.', 'viettitan' ),
								'dependency'  => array( 'element' => 'layout_style', 'value' => array( 'style3', 'style2' ) )
							),
							$icon_viettitan,
							$icon_fontawesome,
							$icon_openiconic,
							$icon_typicons,
							$icon_entypo,
							$icon_linecons,
							$icon_image,
							array(
								'type'       => 'textfield',
								'heading'    => esc_html__( 'Title', 'viettitan' ),
								'param_name' => 'title',
								'value'      => '',
							),
							array(
								'type'       => 'textfield',
								'heading'    => esc_html__( 'Sale Off', 'viettitan' ),
								'param_name' => 'sale_off',
								'value'      => '',
								'dependency' => array( 'element' => 'layout_style', 'value' => 'style4' )
							),
							array(
								'type'       => 'textfield',
								'heading'    => esc_html__( 'Sub Title', 'viettitan' ),
								'param_name' => 'sub_title',
								'value'      => '',
							),
							array(
								'type'       => 'textarea',
								'heading'    => esc_html__( 'Description', 'viettitan' ),
								'param_name' => 'description',
								'value'      => '',
								'dependency' => array( 'element' => 'layout_style', 'value' => array( 'style1', 'style3' ) )
							),
							array(
								'type'       => 'textfield',
								'heading'    => esc_html__( 'Banner Height', 'viettitan' ),
								'param_name' => 'height',
								'value'      => '',
							),

							$add_el_class,
							$add_css_animation,
							$add_duration_animation,
							$add_delay_animation
						)
					)
				);
				$product_cat = array();
				if ( class_exists( 'WooCommerce' ) ) {
					$args               = array(
						'number' => '',
					);
					$product_categories = get_terms( 'product_cat', $args );
					if ( is_array( $product_categories ) ) {
						foreach ( $product_categories as $cat ) {
							$product_cat[$cat->name] = $cat->slug;
						}
					}


					vc_map(
						array(
							'name'     => esc_html__( 'Product', 'viettitan' ),
							'base'     => 'viettitan_product',
							'icon'     => 'fa fa-shopping-cart',
							'category' => VIETTITAN_SHORTCODE_CATEGORY,
							'params'   => array(
								array(
									"type"        => "textfield",
									"heading"     => esc_html__( "Title", 'viettitan' ),
									"param_name"  => "title",
									"admin_label" => true,
									"value"       => ''
								),
								array(
									'type'        => 'dropdown',
									'heading'     => esc_html__( 'Feature', 'viettitan' ),
									'param_name'  => 'feature',
									"admin_label" => true,
									'value'       => array(
										esc_html__( 'All', 'viettitan' )           => 'all',
										esc_html__( 'Sale Off', 'viettitan' )      => 'sale',
										esc_html__( 'New In', 'viettitan' )        => 'new-in',
										esc_html__( 'Featured', 'viettitan' )      => 'featured',
										esc_html__( 'Top rated', 'viettitan' )     => 'top-rated',
										esc_html__( 'Recent review', 'viettitan' ) => 'recent-review',
										esc_html__( 'Best Selling', 'viettitan' )  => 'best-selling'
									)
								),
								array(
									'type'        => 'multi-select',
									'heading'     => esc_html__( 'Narrow Category', 'viettitan' ),
									'param_name'  => 'category',
									'options'     => $product_cat,
									"admin_label" => true,
								),
								array(
									"type"             => "textfield",
									"heading"          => esc_html__( "Total Items", 'viettitan' ),
									"param_name"       => "per_page",
									"admin_label"      => true,
									"value"            => '8',
									"description"      => esc_html__( 'How much total items to show', 'viettitan' ),
									'edit_field_class' => 'vc_col-sm-6 vc_column'
								),

								array(
									'type'             => 'dropdown',
									"heading"          => esc_html__( "Columns", 'viettitan' ),
									"param_name"       => "columns",
									"admin_label"      => true,
									'value'            => array(
										'2' => 2,
										'3' => 3,
										'4' => 4,
									),
									'std'              => 4,
									"description"      => esc_html__( "How much columns grid", 'viettitan' ),
									'edit_field_class' => 'vc_col-sm-6 vc_column'
								),

								array(
									'type'             => 'checkbox',
									'heading'          => esc_html__( 'Show Rating', 'viettitan' ),
									'param_name'       => 'rating',
									'std'              => 1,
									'value'            => array(
										esc_html__( 'Yes, please', 'viettitan' ) => 1
									),
									'edit_field_class' => 'vc_col-sm-6 vc_column'
								),
								array(
									'type'             => 'checkbox',
									'heading'          => esc_html__( 'Display Slider', 'viettitan' ),
									'param_name'       => 'slider',
									'std'              => '',
									'value'            => array(
										esc_html__( 'Yes, please', 'viettitan' ) => 'slider'
									),
									'edit_field_class' => 'vc_col-sm-6 vc_column'
								),

								array(
									'type'             => 'checkbox',
									'heading'          => esc_html__( 'Navigation', 'viettitan' ),
									'param_name'       => 'nav',
									'description'      => esc_html__( 'Show navigation.', 'viettitan' ),
									'value'            => array( esc_html__( 'Yes, please', 'viettitan' ) => 'yes' ),
									'dependency'       => array(
										'element' => 'slider',
										'value'   => array( 'slider' )
									),
									'edit_field_class' => 'vc_col-sm-6 vc_column'
								),
								array(
									'type'             => 'checkbox',
									'heading'          => esc_html__( 'Pagination', 'viettitan' ),
									'param_name'       => 'dots',
									'description'      => esc_html__( 'Show pagination.', 'viettitan' ),
									'value'            => array( esc_html__( 'Yes, please', 'viettitan' ) => 'yes' ),
									'dependency'       => array(
										'element' => 'slider',
										'value'   => array( 'slider' )
									),
									'edit_field_class' => 'vc_col-sm-6 vc_column'
								),


								array(
									'type'             => 'checkbox',
									'heading'          => esc_html__( 'AutoPlay', 'viettitan' ),
									'param_name'       => 'auto_play',
									'std'              => 0,
									'value'            => array(
										esc_html__( 'Yes, please', 'viettitan' ) => 1
									),
									'dependency'       => array(
										'element' => 'slider',
										'value'   => array( 'slider' )
									),
									'edit_field_class' => 'vc_col-sm-6 vc_column',
								),

								array(
									"type"             => "textfield",
									"heading"          => esc_html__( "AutoPlay Speed", 'viettitan' ),
									"param_name"       => "auto_play_speed",
									"value"            => 5000,
									"description"      => esc_html__( 'How much speed autoPlay (ms)', 'viettitan' ),
									'dependency'       => array(
										'element' => 'slider',
										'value'   => array( 'slider' )
									),
									'edit_field_class' => 'vc_col-sm-6 vc_column',
								),

								array(
									'type'             => 'dropdown',
									'heading'          => esc_html__( 'Order by', 'viettitan' ),
									'param_name'       => 'orderby',
									'value'            => array(
										esc_html__( 'Date', 'viettitan' )            => 'date',
										esc_html__( 'Price', 'viettitan-framework' ) => 'price',
										esc_html__( 'Random', 'viettitan' )          => 'rand',
										esc_html__( 'Sales', 'viettitan-framework' ) => 'sales'
									),
									'description'      => esc_html__( 'Select how to sort retrieved products.', 'viettitan' ),
									'dependency'       => array(
										'element' => 'feature',
										'value'   => array( 'all', 'sale', 'featured' )
									),
									'edit_field_class' => 'vc_col-sm-6 vc_column'
								),
								array(
									'type'             => 'dropdown',
									'heading'          => esc_html__( 'Order way', 'viettitan' ),
									'param_name'       => 'order',
									'value'            => array(
										esc_html__( 'Descending', 'viettitan' ) => 'DESC',
										esc_html__( 'Ascending', 'viettitan' )  => 'ASC'
									),
									'description'      => esc_html__( 'Designates the ascending or descending order.', 'viettitan' ),
									'dependency'       => array(
										'element' => 'feature',
										'value'   => array( 'all', 'sale', 'featured' )
									),
									'edit_field_class' => 'vc_col-sm-6 vc_column'
								),
								$add_el_class,
								$add_css_animation,
								$add_duration_animation,
								$add_delay_animation
							)
						)
					);

					vc_map( array(
						'name'     => esc_html__( 'Product Categories', 'viettitan' ),
						'base'     => 'viettitan_product_categories',
						'class'    => '',
						'icon'     => 'fa fa-cart-plus',
						'category' => VIETTITAN_SHORTCODE_CATEGORY,
						'params'   => array(
							array(
								"type"        => "textfield",
								"heading"     => esc_html__( "Title", 'viettitan' ),
								"param_name"  => "title",
								"admin_label" => true,
								"value"       => ''
							),
							array(
								'type'       => 'multi-select',
								'heading'    => esc_html__( 'Narrow Category', 'viettitan' ),
								'param_name' => 'category',
								'options'    => $product_cat
							),
							array(
								'type'        => 'dropdown',
								"heading"     => esc_html__( "Columns", 'viettitan' ),
								"param_name"  => "columns",
								"admin_label" => true,
								'value'       => array(
									'2' => 2,
									'3' => 3,
									'4' => 4,
								),
								'std'         => 4,
								"description" => esc_html__( "How much columns grid", 'viettitan' ),
							),

							array(
								'type'             => 'checkbox',
								'heading'          => esc_html__( 'Hide empty', 'viettitan' ),
								'param_name'       => 'hide_empty',
								'std'              => 0,
								'value'            => array(
									esc_html__( 'Yes, please', 'viettitan' ) => 1
								),
								'edit_field_class' => 'vc_col-sm-6 vc_column'
							),

							array(
								'type'             => 'checkbox',
								'heading'          => esc_html__( 'Display Slider', 'viettitan' ),
								'param_name'       => 'slider',
								'std'              => '',
								'value'            => array(
									esc_html__( 'Yes, please', 'viettitan' ) => 'slider'
								),
								"admin_label"      => true,
								'edit_field_class' => 'vc_col-sm-6 vc_column'
							),

							array(
								'type'             => 'checkbox',
								'heading'          => esc_html__( 'Navigation', 'viettitan' ),
								'param_name'       => 'nav',
								'description'      => esc_html__( 'Show navigation.', 'viettitan' ),
								'value'            => array( esc_html__( 'Yes, please', 'viettitan' ) => 'yes' ),
								'dependency'       => array(
									'element' => 'slider',
									'value'   => array( 'slider' )
								),
								'edit_field_class' => 'vc_col-sm-6 vc_column'
							),
							array(
								'type'             => 'checkbox',
								'heading'          => esc_html__( 'Pagination', 'viettitan' ),
								'param_name'       => 'dots',
								'description'      => esc_html__( 'Show pagination.', 'viettitan' ),
								'value'            => array( esc_html__( 'Yes, please', 'viettitan' ) => 'yes' ),
								'dependency'       => array(
									'element' => 'slider',
									'value'   => array( 'slider' )
								),
								'edit_field_class' => 'vc_col-sm-6 vc_column'
							),

							array(
								'type'             => 'checkbox',
								'heading'          => esc_html__( 'AutoPlay', 'viettitan' ),
								'param_name'       => 'auto_play',
								'std'              => 0,
								'value'            => array(
									esc_html__( 'Yes, please', 'viettitan' ) => 1
								),
								'dependency'       => array(
									'element' => 'slider',
									'value'   => array( 'slider' )
								),
								'edit_field_class' => 'vc_col-sm-6 vc_column',
							),
							array(
								"type"             => "textfield",
								"heading"          => esc_html__( "AutoPlay Speed", 'viettitan' ),
								"param_name"       => "auto_play_speed",
								"value"            => 5000,
								"description"      => esc_html__( 'How much speed autoPlay (ms)', 'viettitan' ),
								'dependency'       => array(
									'element' => 'slider',
									'value'   => array( 'slider' )
								),
								'edit_field_class' => 'vc_col-sm-6 vc_column',
							),

							array(
								'type'             => 'dropdown',
								'heading'          => esc_html__( 'Order by', 'viettitan' ),
								'param_name'       => 'orderby',
								'value'            => array(
									esc_html__( 'Name', 'viettitan' )  => 'name',
									esc_html__( 'Order', 'viettitan' ) => 'order'
								),
								'description'      => esc_html__( 'Select how to sort retrieved products.', 'viettitan' ),
								'edit_field_class' => 'vc_col-sm-6 vc_column'
							),
							array(
								'type'             => 'dropdown',
								'heading'          => esc_html__( 'Order way', 'viettitan' ),
								'param_name'       => 'order',
								'value'            => array(
									esc_html__( 'Descending', 'viettitan' ) => 'DESC',
									esc_html__( 'Ascending', 'viettitan' )  => 'ASC'
								),
								'description'      => esc_html__( 'Designates the ascending or descending orde.', 'viettitan' ),
								'edit_field_class' => 'vc_col-sm-6 vc_column'
							),
							$add_el_class,
							$add_css_animation,
							$add_duration_animation,
							$add_delay_animation
						)
					) );


					vc_map(
						array(
							'name'     => esc_html__( 'Product Sidebar', 'viettitan' ),
							'base'     => 'viettitan_product_sidebar',
							'icon'     => 'fa fa-shopping-cart',
							'category' => VIETTITAN_SHORTCODE_CATEGORY,
							'params'   => array(
								array(
									"type"        => "textfield",
									"heading"     => esc_html__( "Title", 'viettitan' ),
									"param_name"  => "title",
									"admin_label" => true,
									"value"       => ''
								),
								array(
									'type'        => 'dropdown',
									'heading'     => esc_html__( 'Feature', 'viettitan' ),
									'param_name'  => 'feature',
									"admin_label" => true,
									'value'       => array(
										esc_html__( 'All', 'viettitan' )           => 'all',
										esc_html__( 'Sale Off', 'viettitan' )      => 'sale',
										esc_html__( 'New In', 'viettitan' )        => 'new-in',
										esc_html__( 'Featured', 'viettitan' )      => 'featured',
										esc_html__( 'Top rated', 'viettitan' )     => 'top-rated',
										esc_html__( 'Recent review', 'viettitan' ) => 'recent-review',
										esc_html__( 'Best Selling', 'viettitan' )  => 'best-selling'
									)
								),
								array(
									'type'        => 'multi-select',
									'heading'     => esc_html__( 'Narrow Category', 'viettitan' ),
									'param_name'  => 'category',
									'options'     => $product_cat,
									"admin_label" => true,
								),
								array(
									"type"        => "textfield",
									"heading"     => esc_html__( "Total Items", 'viettitan' ),
									"param_name"  => "total_item",
									"admin_label" => true,
									"value"       => 8,
									"description" => esc_html__( 'How much total items to show', 'viettitan' )
								),


								array(
									'type'       => 'checkbox',
									'heading'    => esc_html__( 'Display Slider', 'viettitan' ),
									'param_name' => 'slider',
									'std'        => '',
									'value'      => array(
										esc_html__( 'Yes, please', 'viettitan' ) => 'slider'
									)
								),


								array(
									"type"        => "textfield",
									"heading"     => esc_html__( "Per Page", 'viettitan' ),
									"param_name"  => "per_page",
									"value"       => 4,
									"description" => esc_html__( 'How much items per page to show', 'viettitan' ),
									'dependency'  => array(
										'element' => 'slider',
										'value'   => array( 'slider' )
									),
								),

								array(
									'type'             => 'checkbox',
									'heading'          => esc_html__( 'AutoPlay', 'viettitan' ),
									'param_name'       => 'auto_play',
									'std'              => 0,
									'value'            => array(
										esc_html__( 'Yes, please', 'viettitan' ) => 1
									),
									'dependency'       => array(
										'element' => 'slider',
										'value'   => array( 'slider' )
									),
									'edit_field_class' => 'vc_col-sm-6 vc_column',
								),


								array(
									"type"             => "textfield",
									"heading"          => esc_html__( "AutoPlay Speed", 'viettitan' ),
									"param_name"       => "auto_play_speed",
									"value"            => 5000,
									"description"      => esc_html__( 'How much speed autoPlay (ms)', 'viettitan' ),
									'dependency'       => array(
										'element' => 'auto_play',
										'value'   => array( '1' )
									),
									'edit_field_class' => 'vc_col-sm-6 vc_column',
								),


								array(
									'type'             => 'dropdown',
									'heading'          => esc_html__( 'Order by', 'viettitan' ),
									'param_name'       => 'orderby',
									'value'            => array(
										esc_html__( 'Date', 'viettitan' )            => 'date',
										esc_html__( 'Price', 'viettitan-framework' ) => 'price',
										esc_html__( 'Random', 'viettitan' )          => 'rand',
										esc_html__( 'Sales', 'viettitan-framework' ) => 'sales'

									),
									'description'      => esc_html__( 'Select how to sort retrieved products.', 'viettitan' ),
									'dependency'       => array(
										'element' => 'feature',
										'value'   => array( 'all', 'sale', 'featured' )
									),
									'edit_field_class' => 'vc_col-sm-6 vc_column'
								),
								array(
									'type'             => 'dropdown',
									'heading'          => esc_html__( 'Order way', 'viettitan' ),
									'param_name'       => 'order',
									'value'            => array(
										esc_html__( 'Descending', 'viettitan' ) => 'DESC',
										esc_html__( 'Ascending', 'viettitan' )  => 'ASC'
									),
									'description'      => esc_html__( 'Designates the ascending or descending order.', 'viettitan' ),
									'dependency'       => array(
										'element' => 'feature',
										'value'   => array( 'all', 'sale', 'featured' )
									),
									'edit_field_class' => 'vc_col-sm-6 vc_column'
								),
								$add_el_class,
								$add_css_animation,
								$add_duration_animation,
								$add_delay_animation
							)
						)
					);
				}

				vc_map(
					array(
						'name'     => esc_html__( 'Viettitan Google Map', 'viettitan' ),
						'base'     => 'viettitan_google_map',
						'icon'     => 'fa fa-map-marker',
						'category' => VIETTITAN_SHORTCODE_CATEGORY,
						'params'   => array(
							array(
								'type'             => 'textfield',
								'heading'          => esc_html__( 'Location X', 'viettitan' ),
								'param_name'       => 'location_x',
								'admin_label'      => true,
								'edit_field_class' => 'vc_col-sm-6',
							),
							array(
								'type'             => 'textfield',
								'heading'          => esc_html__( 'Location Y', 'viettitan' ),
								'param_name'       => 'location_y',
								'admin_label'      => true,
								'edit_field_class' => 'vc_col-sm-6',
							),
							array(
								'type'             => 'textfield',
								'heading'          => esc_html__( 'Map height', 'viettitan' ),
								'param_name'       => 'map_height',
								'admin_label'      => true,
								'edit_field_class' => 'vc_col-sm-6',
								'std'              => '500px',
								'description'      => esc_html__( 'Set map height (px or %).', 'viettitan' )
							),
							array(
								'type'             => 'dropdown',
								'heading'          => esc_html__( 'Map style', 'viettitan' ),
								'param_name'       => 'map_style',
								'admin_label'      => true,
								'edit_field_class' => 'vc_col-sm-6',
								'std'              => 'gray_scale',
								'value'            => array(
									esc_html__( 'None', 'viettitan' )       => 'none',
									esc_html__( 'Gray Scale', 'viettitan' ) => 'gray_scale',
									esc_html__( 'Icy Blue', 'viettitan' )   => 'icy_blue',
									esc_html__( 'Mono Green', 'viettitan' ) => 'mono_green',
								)
							),
							array(
								'type'        => 'number',
								'heading'     => esc_html__( 'Map zoom', 'viettitan' ),
								'param_name'  => 'map_zoom',
								'admin_label' => true,
								'std'         => '11',
								'min'         => '1',
								'max'         => '16',
							),
							array(
								'type'        => 'dropdown',
								'heading'     => esc_html__( 'Layout Style', 'viettitan' ),
								'param_name'  => 'layout_style',
								'admin_label' => true,
								'value'       => array( esc_html__( 'Show Marker Icon', 'viettitan' ) => 'marker', esc_html__( 'Show Info Windows', 'viettitan' ) => 'infowindow' ),
								'description' => esc_html__( 'Select Layout Style.', 'viettitan' ),
							),
							array(
								'type'       => 'textfield',
								'heading'    => esc_html__( 'Marker title', 'viettitan' ),
								'param_name' => 'marker_title',
								'dependency' => array(
									'element' => 'layout_style',
									'value'   => 'marker',
								),
							),
							array(
								'type'        => 'attach_image',
								'heading'     => esc_html__( 'Marker Icon', 'viettitan' ),
								'param_name'  => 'marker_icon',
								'value'       => '',
								'description' => esc_html__( 'Select an image from media library.', 'viettitan' ),
								'dependency'  => array(
									'element' => 'layout_style',
									'value'   => 'marker',
								),
							),
							array(
								'type'        => 'textarea_raw_html',
								'heading'     => esc_html__( 'Info Windows HTML', 'viettitan' ),
								'param_name'  => 'info_html',
								'description' => esc_html__( 'Enter your HTML content.', 'viettitan' ),
								'dependency'  => array(
									'element' => 'layout_style',
									'value'   => 'infowindow',
								),
							),
							array(
								'type'       => 'textfield',
								'heading'    => esc_html__( 'Max Width', 'viettitan' ),
								'param_name' => 'info_max_width',
								'std'        => 210,
								'dependency' => array(
									'element' => 'layout_style',
									'value'   => 'infowindow',
								),
							),
							array(
								'type'       => 'colorpicker',
								'heading'    => esc_html__( 'Background color', 'viettitan' ),
								'param_name' => 'info_bg',
								'value'      => '',
								'dependency' => array(
									'element' => 'layout_style',
									'value'   => 'infowindow',
								),
							),
							array(
								'type'       => 'colorpicker',
								'heading'    => esc_html__( 'Color', 'viettitan' ),
								'param_name' => 'info_color',
								'value'      => '',
								'dependency' => array(
									'element' => 'layout_style',
									'value'   => 'infowindow',
								),
							),
							$add_el_class,
							$add_css_animation,
							$add_duration_animation,
							$add_delay_animation
						)
					)
				);


				vc_map( array(
					'name'     => esc_html__( 'Testimonials', 'viettitan' ),
					'base'     => 'viettitan_testimonial',
					'icon'     => 'fa fa-quote-left',
					'category' => VIETTITAN_SHORTCODE_CATEGORY,
					'params'   => array(
						array(
							'type'       => 'param_group',
							'heading'    => esc_html__( 'Testimonials', 'viettitan' ),
							'param_name' => 'values',
							'value'      => urlencode( json_encode( array(
								array(
									'label' => esc_html__( 'Author', 'viettitan' ),
									'value' => '',
								),
							) ) ),
							'params'     => array(
								array(
									'type'        => 'attach_image',
									'heading'     => esc_html__( 'Avatar:', 'viettitan' ),
									'param_name'  => 'avatar',
									'value'       => '',
									'description' => esc_html__( 'Choose the author picture.', 'viettitan' ),
								),
								array(
									'type'        => 'textfield',
									'heading'     => esc_html__( 'Author Info', 'viettitan' ),
									'param_name'  => 'author',
									'admin_label' => true,
									'description' => esc_html__( 'Enter Author information.', 'viettitan' )
								),
								array(
									'type'       => 'textarea',
									'heading'    => esc_html__( 'Quote from author', 'viettitan' ),
									'param_name' => 'quote',
									'value'      => ''
								),
								array(
									'type'       => 'dropdown',
									'heading'    => esc_html__( 'Rating', 'viettitan' ),
									'param_name' => 'rate',
									'value'      => array(
										'None' => 0,
										'1'    => 1,
										'2'    => 2,
										'3'    => 3,
										'4'    => 4,
										'5'    => 5,
										'6'    => 6,
										'7'    => 7,
										'8'    => 8,
										'9'    => 9,
										'10'   => 10,
									),
								),
							),
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Layout Style', 'viettitan' ),
							'param_name'  => 'layout_style',
							'admin_label' => true,
							'value'       => array( esc_html__( 'Default', 'viettitan' ) => 'style1', esc_html__( 'Text italic, Image top', 'viettitan' ) => 'style2', esc_html__( 'Image bottom, Icon top', 'viettitan' ) => 'style3', esc_html__( 'Icon bottom', 'viettitan' ) => 'style4', esc_html__( 'Image top, rate top', 'viettitan' ) => 'style5' ),
							'description' => esc_html__( 'Select Layout Style.', 'viettitan' )
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Color Scheme', 'viettitan' ),
							'param_name'  => 'color_scheme',
							'value'       => array(
								esc_html__( 'Dark', 'viettitan' )  => 'color-dark',
								esc_html__( 'Light', 'viettitan' ) => 'color-light' ),
							'std'         => 'dark',
							'description' => esc_html__( 'Select Color Scheme.', 'viettitan' )
						),
						array(
							'type'             => 'checkbox',
							'heading'          => esc_html__( 'Navigation', 'viettitan' ),
							'param_name'       => 'nav',
							'description'      => esc_html__( 'Show navigation.', 'viettitan' ),
							'value'            => array( esc_html__( 'Yes, please', 'viettitan' ) => 'yes' ),
							'edit_field_class' => 'vc_col-sm-6 vc_column'
						),
						array(
							'type'             => 'checkbox',
							'heading'          => esc_html__( 'Pagination', 'viettitan' ),
							'param_name'       => 'dots',
							'description'      => esc_html__( 'Show pagination.', 'viettitan' ),
							'value'            => array( esc_html__( 'Yes, please', 'viettitan' ) => 'yes' ),
							'edit_field_class' => 'vc_col-sm-6 vc_column'
						),
						array(
							'type'             => 'checkbox',
							'heading'          => esc_html__( 'Pause on hover', 'viettitan' ),
							'param_name'       => 'autoplayhoverpause',
							'description'      => esc_html__( 'Pause on mouse hover.', 'viettitan' ),
							'value'            => array( esc_html__( 'Yes, please', 'viettitan' ) => 'yes' ),
							'std'              => 'yes',
							'edit_field_class' => 'vc_col-sm-6 vc_column'
						),
						array(
							'type'             => 'textfield',
							'heading'          => esc_html__( 'Autoplay Timeout', 'viettitan' ),
							'param_name'       => 'autoplaytimeout',
							'description'      => esc_html__( 'Autoplay interval timeout.', 'viettitan' ),
							'value'            => '',
							'edit_field_class' => 'vc_col-sm-6 vc_column',
							'std'              => 5000
						),
						$add_el_class,
						$add_css_animation,
						$add_duration_animation,
						$add_delay_animation
					)
				) );
			}
		}

	}

	if ( !function_exists( 'init_viettitan_framework_shortcodes' ) ) {
		function init_viettitan_framework_shortcodes() {
			return ViettitanFramework_Shortcodes::init();
		}

		init_viettitan_framework_shortcodes();
	}
}