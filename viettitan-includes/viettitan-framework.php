<?php
/**
 *
 *    Plugin Name: Viettitan Framework
 *    Plugin URI: http://viettitan.net
 *    Description: The Viettitan Framework plugin.
 *    Version: 1.0
 *    Author: viettitan
 *    Author URI: http://viettitan.net
 *
 *    Text Domain: viettitan
 *    Domain Path: /languages/
 *
 * @package Viettitan Framework
 * @category Core
 * @author viettitan
 *
 **/
if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
//==============================================================================
// GET OPTIONS CONFIG
//==============================================================================
if (!function_exists('viettitan_get_options_config')) {
	function &viettitan_get_options_config() {
		if (class_exists('Viettitan_Global')) {
			return Viettitan_Global::get_options();
		}
		$val = array();
		return $val;
	}
}

//==============================================================================
// GET THEME ICON
//==============================================================================
if (!function_exists('viettitan_get_theme_icon')) {
	function &viettitan_get_theme_icon() {
		if (class_exists('Viettitan_Global')) {
			return Viettitan_Global::theme_font_icon();
		}
		$val = array();
		return $val;
	}
}

//==============================================================================
// GET FONTS AWESOME
//==============================================================================
if (!function_exists('viettitan_get_font_awesome')) {
	function &viettitan_get_font_awesome() {
		if (class_exists('Viettitan_Global')) {
			return Viettitan_Global::font_awesome();
		}
		$val = array();
		return $val;
	}
}
if (!class_exists('viettitanFrameWork')) {
	class viettitanFrameWork
	{

		protected $loader;

		protected $prefix;

		protected $version;


		public function __construct()
		{
			$this->version = '1.0.0';
			$this->prefix = 'viettitan-framework';
			$this->define_constants();
			$this->includes();
			$this->define_hook();
		}


		private function  define_constants()
		{
			if (!defined('VIETTITAN_INCLUDES_DIR')) {
				define('VIETTITAN_INCLUDES_DIR', get_template_directory() .'/viettitan-includes/');
			}
			if (!defined('VIETTITAN_INCLUDES_URI')) {
				define('VIETTITAN_INCLUDES_URI', get_template_directory_uri() .'/viettitan-includes/');
			}
			if (!defined('VIETTITAN_FRAMEWORK_SHORTCODES_DIR')) {
				define('VIETTITAN_FRAMEWORK_SHORTCODES_DIR', get_template_directory() .'/viettitan-includes/includes/shortcodes/');
			}
			if (!defined('VIETTITAN_FRAMEWORK_SHORTCODES_URI')) {
				define('VIETTITAN_FRAMEWORK_SHORTCODES_URI', get_template_directory_uri() .'/viettitan-includes/includes/shortcodes/');
			}
			if (!defined('VIETTITAN_FRAMEWORK_NAME')) {
				define('VIETTITAN_FRAMEWORK_NAME', 'viettitan-framework');
			}
			if (!defined('VIETTITAN_SHORTCODE_CATEGORY')) {
				define('VIETTITAN_SHORTCODE_CATEGORY', esc_html__('Viettitan Shortcodes', 'viettitan'));
			}
		}

		private function includes()
		{
			require_once ( VIETTITAN_INCLUDES_DIR . 'includes/class-viettitan-framework-loader.php');
			if (!class_exists('WPAlchemy_MetaBox')) {
				include_once(VIETTITAN_INCLUDES_DIR . 'includes/MetaBox.php');
			}
			require_once VIETTITAN_INCLUDES_DIR . 'admin/class-viettitan-framework-admin.php';
			$this->loader = new viettitanFramework_Loader();

			/*short-codes*/
			include_once VIETTITAN_INCLUDES_DIR . 'includes/shortcodes/shortcodes.php';

			/* widgets */
            include_once VIETTITAN_INCLUDES_DIR . 'includes/widgets/widgets.php';
		}

		public function load_plugin_textdomain()
		{
		}


		private function define_hook()
		{
			/*set locale*/
			$this->loader->add_action('plugins_loaded', $this, 'load_plugin_textdomain');

			/*admin*/
			$plugin_admin = new viettitanFramework_Admin($this->get_prefix(), $this->get_version());

			$this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles');
			$this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts');
		}

		public function get_version()
		{
			return $this->version;
		}

		public function get_prefix()
		{
			return $this->prefix;
		}

		public function run()
		{
			$this->loader->run();
		}
	}

	if (!function_exists('init_viettitan_framework')) {
		function init_viettitan_framework()
		{
			$viettitanFramework = new viettitanFrameWork();
			$viettitanFramework->run();
		}

		init_viettitan_framework();
	}
}
