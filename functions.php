<?php
define( 'VIETTITAN_HOME_URL', trailingslashit( home_url() ) );
define( 'VIETTITAN_THEME_DIR', trailingslashit( get_template_directory() ) );
define( 'VIETTITAN_THEME_URL', trailingslashit( get_template_directory_uri() ) );


if (!function_exists('viettitan_include_theme_options')) {
	function viettitan_include_theme_options() {
		if (!class_exists( 'ReduxFramework' )) {
			require_once( VIETTITAN_THEME_DIR . 'viettitan-framework/options/framework.php' );
		}
		require_once( VIETTITAN_THEME_DIR . 'viettitan-framework/option-extensions/loader.php' );
		require_once( VIETTITAN_THEME_DIR . 'includes/options-config.php' );
	}
	viettitan_include_theme_options();
}

if (!function_exists('viettitan_add_custom_mime_types')) {
    function viettitan_add_custom_mime_types($mimes) {
        return array_merge($mimes, array(
            'eot'  => 'application/vnd.ms-fontobject',
            'woff' => 'application/x-font-woff',
            'ttf'  => 'application/x-font-truetype',
            'svg'  => 'image/svg+xml',
        ));
    }
    add_filter('upload_mimes','viettitan_add_custom_mime_types');
}


if (!function_exists('viettitan_include_library')) {
	function viettitan_include_library() {
        require_once(VIETTITAN_THEME_DIR . 'viettitan-framework/viettitan-framework.php');
        require_once(VIETTITAN_THEME_DIR . 'viettitan-includes/viettitan-framework.php');
		require_once(VIETTITAN_THEME_DIR . 'includes/theme-setup.php');
		require_once(VIETTITAN_THEME_DIR . 'includes/sidebar.php');
		require_once(VIETTITAN_THEME_DIR . 'includes/meta-boxes.php');
		require_once(VIETTITAN_THEME_DIR . 'includes/admin-enqueue.php');
		require_once(VIETTITAN_THEME_DIR . 'includes/theme-functions.php');
		require_once(VIETTITAN_THEME_DIR . 'includes/theme-action.php');
		require_once(VIETTITAN_THEME_DIR . 'includes/theme-filter.php');
		require_once(VIETTITAN_THEME_DIR . 'includes/frontend-enqueue.php');
		require_once(VIETTITAN_THEME_DIR . 'includes/tax-meta.php');
		require_once(VIETTITAN_THEME_DIR . 'includes/custom-dasboard.php');
		require_once(VIETTITAN_THEME_DIR . 'includes/custom-wp-admin.php');
		if(class_exists('Vc_Manager')){
			require_once(VIETTITAN_THEME_DIR . 'includes/vc-functions.php');
		}
    }
	viettitan_include_library();
}
